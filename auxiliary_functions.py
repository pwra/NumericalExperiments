#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 21 11:18:25 2020

@author: Peter Winkel Rasmussen
"""

import time
import numpy as np
import cupy as cp
import cupyx.scipy.fft as ft
import _helper
import astra
from matplotlib.colors import hsv_to_rgb

mempool = cp.get_default_memory_pool()
pinned_mempool = cp.get_default_pinned_memory_pool()

def check_gpu_mem():
    # print(mempool.used_bytes()/10**9)
    # print(mempool.total_bytes()/10**9)
    # print(pinned_mempool.n_free_blocks()/10**9)
    mempool.free_all_blocks()
    # print(mempool.used_bytes()/10**9)
    # print(mempool.total_bytes()/10**9)
    # print(pinned_mempool.n_free_blocks()/10**9)


def forward_2d(vol_geom, proj_geom, data, gpu_idx=[0]):
    astra.set_gpu_index(gpu_idx)
    volume_id = astra.data2d.create('-vol', vol_geom, data)
    sinogram_id = astra.data2d.create('-sino', proj_geom, 0)
    alg = 'FP_CUDA'
    cfg = astra.astra_dict(alg)
    cfg['ProjectionDataId'] = sinogram_id
    cfg['VolumeDataId'] = volume_id
    alg_id = astra.algorithm.create(cfg)
    astra.algorithm.run(alg_id)
    astra.algorithm.delete(alg_id)
    
    sinogram = astra.data2d.get(sinogram_id)

    astra.data2d.delete(volume_id)
    astra.data2d.delete(sinogram_id)

    return sinogram

def forward_reco(vol_geom, proj_geom, data, gpu_idx=[0,1]):
    # print('1')
    # print(astra.get_gpu_info())
    # astra.set_gpu_index(gpu_idx)
    # print('2')
    volume_id = astra.data3d.create('-vol', vol_geom, data)
    sinogram_id = astra.data3d.create('-sino', proj_geom, 0)
    alg = 'FP3D_CUDA'
    cfg = astra.astra_dict(alg)
    cfg['ProjectionDataId'] = sinogram_id
    cfg['VolumeDataId'] = volume_id
    alg_id = astra.algorithm.create(cfg)
    astra.algorithm.run(alg_id)
    sinogram = astra.data3d.get(sinogram_id)

    astra.algorithm.delete(alg_id)
    astra.data3d.delete(volume_id)
    astra.data3d.delete(sinogram_id)

    return sinogram

def backward_sino(vol_geom, proj_geom, data, gpu_idx=[0,1]):
    astra.set_gpu_index(gpu_idx)
    sinogram_id = astra.data3d.create('-sino', proj_geom, data)
    volume_id = astra.data3d.create('-vol', vol_geom, 0)
    alg = 'BP3D_CUDA'
    cfg = astra.astra_dict(alg)
    cfg['ProjectionDataId'] = sinogram_id
    cfg['ReconstructionDataId'] = volume_id
    alg_id = astra.algorithm.create(cfg)
    astra.algorithm.run(alg_id)
    astra.algorithm.delete(alg_id)

    bp_data = astra.data3d.get(volume_id)

    astra.algorithm.delete(alg_id)
    astra.data3d.delete(volume_id)
    astra.data3d.delete(sinogram_id)

    return bp_data

def cpu_ncp(b, Ax):
    from scipy import fft
    m = b.shape[0]
    res_vec = b - Ax
    npad = ((0, fft.next_fast_len(m) - m))
    res_vec = np.pad(res_vec, npad, mode='constant', constant_values=0)
    res_vec_fft = fft.rfft(res_vec)
    q = res_vec_fft.shape[0]
    s_vec = np.abs(res_vec_fft)**2
    
    c = np.cumsum(s_vec[1:])/np.sum(s_vec[1:])
    c_white = np.arange(1, q)/q
        
    ncp = np.linalg.norm(c-c_white)
    
    return c, ncp

def cuda_ncp(b, Ax, gpu_idx):
    with cp.cuda.Device(gpu_idx):
        m = b.shape[0]
        res_vec = cp.asarray(b - Ax)
        npad = ((0, _helper.next_fast_len(m) - m))
        res_vec = cp.pad(res_vec, npad, mode='constant', constant_values=0)
        check_gpu_mem()

        res_vec_fft = ft.rfft(res_vec).astype(cp.float32)
        res_vec = None
        check_gpu_mem()
        
        q = res_vec_fft.shape[0]
        s_vec = cp.abs(res_vec_fft)**2
        res_vec_fft = None
        check_gpu_mem()
        
        #Note the zeroth element is excluded,
        #but it does not change the norm.
        c = (cp.cumsum(s_vec[1:])/cp.sum(s_vec[1:])).astype(cp.float32)
        s_vec = None
        check_gpu_mem()
        
        c_white = cp.arange(1, q)/q
        ncp = cp.asnumpy(cp.linalg.norm(c-c_white))
        c = cp.asnumpy(c)
        c_white = None
        mempool.free_all_blocks()
    
    return c, ncp

def cuda_norm(a, gpu_idx, order=2):
    check_point1 = time.time()
    with cp.cuda.Device(gpu_idx):
        check_point2 = time.time()
        a = cp.array(a) #Fairly slow for big arrays
        check_point3 = time.time()
        norm = cp.linalg.norm(a, ord=order) #Really fast
        check_point4 = time.time()
        norm = cp.asnumpy(norm)
        check_point5 = time.time()
        
        a = None
        mempool.free_all_blocks()
        
    return norm

def add_noise(data, intensity, use_GPU=False, seed=137, num_split=1):
    if intensity == np.inf:
        return data, 0
    else:
        if use_GPU:
            # Set seed
            cp.random.seed(seed)
            noisy_data = np.zeros_like(data)
            noisy_data = np.array_split(noisy_data, num_split)
            max_data = data.max()
            data = np.array_split(data, num_split)
            
            for iSplit in range(num_split):
                # Load data to GPU
                noisy_data_cp = cp.array(data[iSplit])                
                # Absorption to transmission
                noisy_data_cp /= -max_data
                noisy_data_cp = cp.exp(noisy_data_cp, out=noisy_data_cp)
                # To detector count
                noisy_data_cp *= intensity
                # Convert data to int32 used by cp.random.poisson
                noisy_data_cp = noisy_data_cp.astype(np.int32)
                # Release float copy
                mempool.free_all_blocks()
                # add poison noise
                noisy_data_cp = cp.random.poisson(noisy_data_cp, dtype=np.int32)
                # Release old noisy_data_cp
                mempool.free_all_blocks()
                # Back to float
                noisy_data_cp = noisy_data_cp.astype(np.float32)
                # Release int copy
                mempool.free_all_blocks()
                # Count to absoprtion
                noisy_data_cp /= intensity
                noisy_data_cp = cp.log(noisy_data_cp, out=noisy_data_cp)
                noisy_data_cp *= -max_data
                # Back to CPU memory
                noisy_data[iSplit] = cp.asnumpy(noisy_data_cp)
                mempool.free_all_blocks()
            noisy_data_cp = None
            mempool.free_all_blocks()
            noisy_data = np.concatenate(noisy_data)
            data = np.concatenate(data)
        else:
            # Set seed
            np.random.seed(seed)
            noisy_data = np.copy(data)
            # Find Max
            max_data = noisy_data.max()
            # Absorption to transmission
            noisy_data /= -max_data
            noisy_data = np.exp(noisy_data, out=noisy_data)
            # To detector count
            noisy_data *= intensity
            # Convert data to int32 used by cp.random.poisson
            noisy_data = noisy_data.astype(np.int32)
            # add poison noise
            noisy_data = np.random.poisson(noisy_data)
            # Back to float
            noisy_data = noisy_data.astype(np.float32)
            # Count to absoprtion
            noisy_data /= intensity
            noisy_data = np.log(noisy_data, out=noisy_data)
            noisy_data *= -max_data
        return noisy_data, noisy_data-data
    
def setup_reco_alg(reco_type, vol_geom, sinograms_id, prior, relax=1.0):
    
    if reco_type == 1:
        rec_id = astra.data3d.create('-vol', vol_geom)
        reco_alg = 'SIRT3D_CUDA'
        cfg = astra.astra_dict(reco_alg)
        cfg['ProjectionDataId'] = sinograms_id
        cfg['ReconstructionDataId'] = rec_id
        cfg['option'] = {}
        # cfg['option']['Relaxation'] = relax
        alg_id = astra.algorithm.create(cfg)
    
    if reco_type == 2:
        rec_id = astra.data3d.create('-vol', vol_geom)
        reco_alg = 'SIRT3D_CUDA'
        cfg = astra.astra_dict(reco_alg)
        cfg['ProjectionDataId'] = sinograms_id
        cfg['ReconstructionDataId'] = rec_id
        cfg['option'] = {}
        # cfg['option']['Relaxation'] = relax
        cfg['option']['MinConstraint'] = 0
        cfg['option']['MaxConstraint'] = 2.5
        alg_id = astra.algorithm.create(cfg)
        
    elif reco_type == 3:
        rec_id = astra.data3d.create('-vol', vol_geom, prior)
        reco_alg = 'SIRT3D_CUDA'
        cfg = astra.astra_dict(reco_alg)
        cfg['ProjectionDataId'] = sinograms_id
        cfg['ReconstructionDataId'] = rec_id
        cfg['option'] = {}
        # cfg['option']['Relaxation'] = relax
        cfg['option']['MinConstraint'] = 0
        cfg['option']['MaxConstraint'] = 2.5
        alg_id = astra.algorithm.create(cfg)
        
    elif reco_type == 4:
        rec_id = astra.data3d.link('-vol', vol_geom, prior)
        reco_alg = 'SIRT3D_CUDA'
        cfg = astra.astra_dict(reco_alg)
        cfg['ProjectionDataId'] = sinograms_id
        cfg['ReconstructionDataId'] = rec_id
        cfg['option'] = {}
        # cfg['option']['Relaxation'] = relax
        cfg['option']['MinConstraint'] = 0
        cfg['option']['MaxConstraint'] = 2.5
        alg_id = astra.algorithm.create(cfg)
    
    return alg_id, rec_id

def setup_reco_alg_plugin(reco_type, vol_geom, sinograms_id, proj_id, prior, relax=1.0):
    
    astra.plugin.register(astra.plugins.SIRTPlugin)

    if reco_type == 1:
        rec_id = astra.data3d.create('-vol', vol_geom)
        cfg = astra.astra_dict('SIRT-PLUGIN')
        cfg['ProjectorId'] = proj_id
        cfg['ProjectionDataId'] = sinograms_id
        cfg['ReconstructionDataId'] = rec_id
        cfg['option'] = {}
        cfg['option']['Relaxation'] = relax
        alg_id = astra.algorithm.create(cfg)
    
    if reco_type == 2:
        rec_id = astra.data3d.create('-vol', vol_geom)
        cfg = astra.astra_dict('SIRT-PLUGIN')
        cfg['ProjectionDataId'] = sinograms_id
        cfg['ReconstructionDataId'] = rec_id
        cfg['option'] = {}
        cfg['option']['Relaxation'] = relax
        cfg['option']['MinConstraint'] = 0
        cfg['option']['MaxConstraint'] = 2.5
        alg_id = astra.algorithm.create(cfg)
        
    elif reco_type == 3:
        rec_id = astra.data3d.create('-vol', vol_geom, prior)
        cfg = astra.astra_dict('SIRT-PLUGIN')
        cfg['ProjectionDataId'] = sinograms_id
        cfg['ReconstructionDataId'] = rec_id
        cfg['option'] = {}
        cfg['option']['Relaxation'] = relax
        cfg['option']['MinConstraint'] = 0
        cfg['option']['MaxConstraint'] = 2.5
        alg_id = astra.algorithm.create(cfg)
        
    elif reco_type == 4:
        rec_id = astra.data3d.link('-vol', vol_geom, prior)
        cfg = astra.astra_dict('SIRT-PLUGIN')
        cfg['ProjectionDataId'] = sinograms_id
        cfg['ReconstructionDataId'] = rec_id
        cfg['option'] = {}
        cfg['option']['Relaxation'] = relax
        cfg['option']['MinConstraint'] = 0
        cfg['option']['MaxConstraint'] = 2.5
        alg_id = astra.algorithm.create(cfg)
    
    return alg_id, rec_id

def reco_message_error(reco_type):
    if reco_type == 0:
        print('Reconstruction performed with FBP')
    elif reco_type == 1:
        print('Reconstruction performed with SIRT.')
    elif reco_type == 2:
        print('Reconstruction performed with SIRT and box constraints.')
    elif reco_type == 3:
        print('Reconstruction performed with SIRT, box constraints '+
              'and initialization.')
    elif reco_type == 4:
        print('Reconstruction performed with SIRT, box constraints'+
              ', initialization and prior constraints.')
    else:
        raise ValueError('reco_type chosen not supported')
        
def stopping_criteria_error_message(args):
    if args.use_NCP:
        print('Using NCP stopping criteria!')
    elif args.use_GT:
        print('Using GT stopping criteria!')
    elif args.num_itr > 0:
        print('Running with {} number of iterations!'.format(args.num_itr))
    else:
        raise ValueError('Stopping criteria not set!')
        
def hsv_colors(num_colors):
    H = np.linspace(0, 1, num_colors+1)
    V = np.ones_like(H)
    S = np.ones_like(H)
    HSV = np.dstack((H,S,V))
    RGB = hsv_to_rgb(HSV)[0][:-1]
    
    return RGB