#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 14 09:34:45 2020

@author: Peter Winkel Rasmussen
"""
import numpy as np

num_frames = 100
num_rows = 256
num_cols = 256
num_slice = 256
num_pixel = int(np.sqrt(2)*num_rows)

angle_shift_deg = 24.0
angle_shift = angle_shift_deg/180.0*np.pi

oil_attenuation = 1.0
water_attenuation = 1.7
rock_attenuation = 2.5

bin_width = 0.02