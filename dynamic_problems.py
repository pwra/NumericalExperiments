#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 27 10:30:01 2020

@author: Peter Winkel Rasmussen
"""
import os
os.chdir('/home/pwra/simulation/')
import numpy as np
# import cupy as cp
import astra
import auxiliary_functions as af
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.style as style
import matplotlib.patches as mpatches
from matplotlib.widgets import Slider
style.use('seaborn')
style.use('ggplot')
from cycler import cycler
from matplotlib.colors import hsv_to_rgb, to_rgba, LogNorm
# import matplotlib.animation as animation
from mpl_toolkits.axes_grid1 import make_axes_locatable
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
params = {'axes.grid': False, 'savefig.dpi': 200,
          'text.color': 'black',
          'axes.labelcolor': 'black',
          'xtick.color': 'black',
          'ytick.color': 'black',
          'lines.linewidth': 4,
          'patch.linewidth': 3,
          'lines.markersize': 10}
plt.rcParams.update(params)

SMALL_SIZE = 13+6
MEDIUM_SIZE = 15+6
BIGGER_SIZE = 17+6

plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=MEDIUM_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title
astra.set_gpu_index([0,1])

file_path_plots = '/home/pwra/simulation/paper_plots/'
file_path = '/home/pwra/simulation/output_files/'
# %%
simulation_full = np.load(file_path+'simulation_attenuation_blur.npy') #(t, slice, row, column)
# %%
simulation = simulation_full[:,170]
num_frame, num_row, num_col = simulation.shape
# %%
num_vol = 256
# num_pixel = int(np.pi/2*num_vol)
num_pixel = int(np.sqrt(2)*num_vol)
num_projection = 400
angles = np.linspace(0,np.pi,num_projection, endpoint=False)

vol_geom = astra.create_vol_geom(num_vol, num_vol) #(rows, columns, slices)
proj_geom = astra.create_proj_geom('parallel', 1, num_pixel, angles)
perfect_projections = af.forward_2d(vol_geom, proj_geom, simulation[0])

# %%
projections_movement = np.empty((num_projection, num_pixel), dtype=np.float32)
for iFrame in range(num_frame-1):
    sub_angle = angles[4*iFrame:4*(iFrame+1)]
    proj_geom = astra.create_proj_geom('parallel', 1, num_pixel, sub_angle)
    sub_projections = af.forward_2d(vol_geom, proj_geom, simulation[iFrame])
    projections_movement[4*iFrame:4*(iFrame+1), :] = sub_projections
# %%
vol_geom = astra.create_vol_geom(num_vol, num_vol) #(rows, columns, slices)
proj_geom = astra.create_proj_geom('parallel', 1, num_pixel, angles[::8])
missing_projections = af.forward_2d(vol_geom, proj_geom, simulation[0])
# %%
intensity = 2.155e3 # matches 5.00% noise
# intensity = 5.35e4 # matches 1.00% noise
# intensity = 8.6e5 # matches 0.25% noise
# intensity = np.inf

noisy_projections, noise = af.add_noise(perfect_projections, intensity, use_GPU=True, num_split=1)
intensity = 8.6e5 # matches 0.25% noise
perfect_projections, noise = af.add_noise(perfect_projections, intensity, use_GPU=True, num_split=1)
missing_projections, noise = af.add_noise(missing_projections, intensity, use_GPU=True, num_split=1)
projections_movement, noise = af.add_noise(projections_movement, intensity, use_GPU=True, num_split=1)

# %%
# angles = np.linspace(0.5*np.pi,1.5*np.pi,num_projection, endpoint=False)
proj_geom = astra.create_proj_geom('parallel', 1, num_pixel, angles);
vol_geom = astra.create_vol_geom(num_vol, num_vol) # (rows, columns)
sinograms_id = astra.data2d.create('-sino', proj_geom, perfect_projections)
    
rec_id = astra.data2d.create('-vol', vol_geom)
reco_alg = 'FBP_CUDA'
cfg = astra.astra_dict(reco_alg)
cfg['ProjectionDataId'] = sinograms_id
cfg['ReconstructionDataId'] = rec_id
alg_id = astra.algorithm.create(cfg)

## %% Reconstruction
astra.algorithm.run(alg_id)
reconstruction_perfect = astra.data2d.get(rec_id)

## Clean up.
astra.algorithm.delete(alg_id)
astra.data2d.delete(rec_id)
astra.data2d.delete(sinograms_id)
# %%
proj_geom = astra.create_proj_geom('parallel', 1, num_pixel, angles);
vol_geom = astra.create_vol_geom(num_vol, num_vol) # (rows, columns)
sinograms_id = astra.data2d.create('-sino', proj_geom, projections_movement)
    
rec_id = astra.data2d.create('-vol', vol_geom)
reco_alg = 'FBP_CUDA'
cfg = astra.astra_dict(reco_alg)
cfg['ProjectionDataId'] = sinograms_id
cfg['ReconstructionDataId'] = rec_id
alg_id = astra.algorithm.create(cfg)

## %% Reconstruction
astra.algorithm.run(alg_id)
reconstruction_movement = astra.data2d.get(rec_id)

## Clean up.
astra.algorithm.delete(alg_id)
astra.data2d.delete(rec_id)
astra.data2d.delete(sinograms_id)
# %%
proj_geom = astra.create_proj_geom('parallel', 1, num_pixel, angles);
vol_geom = astra.create_vol_geom(num_vol, num_vol) # (rows, columns)
sinograms_id = astra.data2d.create('-sino', proj_geom, noisy_projections)
    
rec_id = astra.data2d.create('-vol', vol_geom)
reco_alg = 'FBP_CUDA'
cfg = astra.astra_dict(reco_alg)
cfg['ProjectionDataId'] = sinograms_id
cfg['ReconstructionDataId'] = rec_id
alg_id = astra.algorithm.create(cfg)

## %% Reconstruction
astra.algorithm.run(alg_id)
reconstruction_noisy = astra.data2d.get(rec_id)

## Clean up.
astra.algorithm.delete(alg_id)
astra.data2d.delete(rec_id)
astra.data2d.delete(sinograms_id)
# %%
proj_geom = astra.create_proj_geom('parallel', 1, num_pixel, angles[::8]);
vol_geom = astra.create_vol_geom(num_vol, num_vol) # (rows, columns)
sinograms_id = astra.data2d.create('-sino', proj_geom, missing_projections)
    
rec_id = astra.data2d.create('-vol', vol_geom)
reco_alg = 'FBP_CUDA'
cfg = astra.astra_dict(reco_alg)
cfg['ProjectionDataId'] = sinograms_id
cfg['ReconstructionDataId'] = rec_id
alg_id = astra.algorithm.create(cfg)

## %% Reconstruction
astra.algorithm.run(alg_id)
reconstruction_missing = astra.data2d.get(rec_id)

## Clean up.
astra.algorithm.delete(alg_id)
astra.data2d.delete(rec_id)
astra.data2d.delete(sinograms_id)
# %%
fig, ax = plt.subplots(2,2,squeeze=False, figsize=(10,10))
ax[0,0].imshow(reconstruction_perfect, cmap='gray', vmin=0, vmax=2.5)
ax[0,1].imshow(reconstruction_noisy, cmap='gray', vmin=0, vmax=2.5)
ax[1,0].imshow(reconstruction_missing, cmap='gray', vmin=0, vmax=2.5)
ax[1,1].imshow(reconstruction_movement, cmap='gray', vmin=0, vmax=2.5)

titles = np.array([[['Ideal data'],['Low exposure']],[['Few projections'],['Slow acquisition rate']]])[:,:,0]
plot_names = np.array([[['a'],['b']],[['c'],['d']]])[:,:,0]
for iRow in range(ax.shape[0]):
    for iCol in range(ax.shape[1]):
        ax[iRow,iCol].set_xticks([])
        ax[iRow,iCol].set_yticks([])
        ax[iRow,iCol].set_title(titles[iRow,iCol])
        ax[iRow,iCol].text(0.975, 0.975, plot_names[iRow,iCol],
                         horizontalalignment='right',
                         verticalalignment='top',
                         fontweight='bold',
                         fontsize=BIGGER_SIZE,
                         color='white',
                         transform=ax[iRow,iCol].transAxes)
fig.tight_layout(rect=[0,0,1,1],w_pad=1.5, h_pad=0.5)
fig.savefig(file_path_plots+'dynamic_problems.pdf')