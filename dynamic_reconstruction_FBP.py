#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 30 09:57:57 2020

@author: Peter Winkel Rasmussen
"""
import os
os.chdir('/home/pwra/simulation/')
import argparse
import constants as const
import numpy as np
import astra

# %%
def main(args):
    # %%
    scale = 4
    proj_used_idx = np.arange(0, args.num_proj,
                              args.num_proj//args.num_proj_used, dtype='int')
    astra.set_gpu_index(args.gpu)
    noise = args.noise
    num_itr = None
    
    file_path = '/home/pwra/simulation/output_files/'
    file_name = 'simulation_projections_Noise{}_NumProj{}.npy'.format(noise, args.num_proj)
    projections = np.load(file_path+file_name)
    
    angle_shift = int(np.round(const.angle_shift/(np.pi/args.num_proj)))
    
    projections = np.roll(projections, shift=angle_shift, axis=2)
    projections[:,:,:angle_shift,:] = np.flip(projections[:,:,:angle_shift,:], axis=-1)
    
    num_frame, num_slice, args.num_proj, num_pixel = projections.shape
    num_col = 256*scale
    num_row = 256*scale
    # %%
    angles = np.linspace(0, np.pi, args.num_proj, endpoint=False)
    
    proj_geom = astra.create_proj_geom('parallel', 1, num_pixel, angles[proj_used_idx]);
    vol_geom = astra.create_vol_geom(num_row, num_col) # (rows, columns)
    # residual = np.zeros((num_frame, num_itr), dtype=np.float32)
    
    reconstruction = np.zeros((num_frame, num_slice, num_row, num_col), dtype=np.float32)
    
    print_step = 0.0
    for iFrame in range(num_frame):
        if np.isclose(iFrame/num_frame, print_step):
            print('Reconstructing time step {} out of {} steps'.format(iFrame, num_frame))
            print_step += 0.1
        for iSlice in range(num_slice):
            sinograms_id = astra.data2d.create('-sino', proj_geom, projections[iFrame][iSlice, proj_used_idx, :])
                
            rec_id = astra.data2d.create('-vol', vol_geom)
            reco_alg = 'FBP_CUDA'
            cfg = astra.astra_dict(reco_alg)
            cfg['ProjectionDataId'] = sinograms_id
            cfg['ReconstructionDataId'] = rec_id
            alg_id = astra.algorithm.create(cfg)
        
            ## %% Reconstruction
            astra.algorithm.run(alg_id)
            reconstruction[iFrame, iSlice] = astra.data2d.get(rec_id)
            
            ## Clean up.
            astra.algorithm.delete(alg_id)
            astra.data2d.delete(rec_id)
            astra.data2d.delete(sinograms_id)
    # %%
    file_path = '/home/pwra/simulation/output_files/noise{}/num_proj_used{}/reco_type{}/'.format(noise, args.num_proj_used, 0)
    file_name = 'dynamic_reconstruction_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}.npy'.format(noise, args.num_proj_used, 0, num_itr)
    np.save(file_path+file_name, np.flip(reconstruction, axis=(2)))
# %%    
if __name__ == '__main__':
    # Construct the argument parser
    parser = argparse.ArgumentParser()
    
    # Add the arguments to the parser
    parser.add_argument("-num_proj", "--num_proj", required=False,
                        nargs='?', const=1, default=int(1440), type=int, help="first operand")
    parser.add_argument("-num_proj_used", "--num_proj_used", required=False,
                        nargs='?', const=1, default=int(1440), type=int, help="second operand")
    parser.add_argument("-noise", "--noise", required=False,
                        nargs='?', const=1, default='0.25', type=str, help="third operand")
    parser.add_argument("-gpu", "--gpu", required=False,
                        nargs='?', default=[0,1],
                        type=lambda s: [item for item in s.split(',')], help="fourth operand")
    args = parser.parse_args()
    # %%
    main(args)