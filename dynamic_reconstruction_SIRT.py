#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 10 14:39:53 2020

@author: Peter Winkel Rasmussen
"""
import os
os.chdir('/home/pwra/simulation/')
import time
import argparse
from distutils import util
import numpy as np
# import cupy as cp
import astra
# from scipy import stats
from skimage.draw import circle
import dynamic_reconstruction_FBP
import auxiliary_functions as af
import constants as const
import matplotlib.pyplot as plt
# mempool = cp.get_default_memory_pool()    
# %%
def main(args):
    # %%
    scale = 1
    num_proj = args.num_proj
    proj_used_idx = np.arange(0, num_proj, num_proj//args.num_proj_used, dtype='int')
    astra.set_gpu_index(args.gpu)
    # print(astra.astra.get_gpu_info())
    noise = args.noise
    reco_type = args.reco_type
    use_NCP = args.use_NCP
    num_itr = args.num_itr
    
    file_path = '/home/pwra/simulation/output_files/'
    file_name = 'frozen_simulation_projections_Noise{}_NumProj{}.npy'.format(noise, num_proj)
    projections = np.load(file_path+file_name)
    
    angle_shift = int(np.round(const.angle_shift/(np.pi/args.num_proj)))
    projections = np.roll(projections, shift=angle_shift, axis=2)
    projections[:,:,:angle_shift,:] = np.flip(projections[:,:,:angle_shift,:], axis=-1)
    
    num_frame, num_slice, num_proj, num_pixel = projections.shape
    num_col = const.num_cols*scale
    num_row = const.num_rows*scale
    
    # if args.reco_type in (3, 4):
    file_name = 'static_reconstruction_Noise{}_NumProj{}_RecoType{}_NumItr{}.npy'.format('0.25', 720, 2, 'NCP')
    reconstruction_static = np.load(file_path+file_name)
    
    if args.use_GT:
        file_name = 'simulation_attenuation_blur.npy'
        x_GT_full = np.load(file_path+file_name)
        if args.reverse_time:
            x_GT_full = np.flip(x_GT_full, axis=0)
        
    time_flip = ''
    if args.reverse_time:
        projections = np.flip(projections, axis=0)
        reconstruction_static = np.flip(reconstruction_static, axis=0)
        time_flip = '_Flip'
    # %%
    if reco_type == 4:
        rr, cc = circle(num_row//2-1, num_col//2-1, 124*scale)
        mask_inner = np.zeros((num_row, num_col), dtype='bool')
        mask_inner[rr, cc] = True
        mask_inner_rock = np.tile(mask_inner, (num_slice, 1, 1))
        
        static_mask = reconstruction_static[0] >= 2.4
        
        dynamic_mask = np.logical_and(reconstruction_static[0] < 1.83, reconstruction_static[0] > 0.0)
        dynamic_mask = np.logical_and(dynamic_mask, mask_inner_rock)
    # %%
    # # mask = dynamic_mask[0]
    # # normal = rec_tmp[ncp_idx,0]
    # # rock = rec_tmp[ncp_idx,0]*static_mask[0]
    # # fluid = rec_tmp[ncp_idx,0]*dynamic_mask[0]
    
    # rock_static = reconstruction_static[0,0]*static_mask[0]
    # fluid_static = reconstruction_static[0,0]*dynamic_mask[0]
    
    # # rock_GT = x_GT_full[0,0]*static_mask[0]
    # # fluid_GT = x_GT_full[0,0]*dynamic_mask[0]
    # # below = (rec_tmp[ncp_idx,0] < 1.0).astype('uint8')#np.logical_and(dynamic_mask[0], rec_tmp[ncp_idx,0] < 1.0).astype('uint8')
    # # above = np.logical_and(dynamic_mask[0], rec_tmp[ncp_idx,0] > 1.6).astype('uint8')
    
    # fig, ax = plt.subplots(1,3,squeeze=False,figsize=(12,8))
    # # ax[0,0].imshow(fluid)
    # # ax[0,1].imshow(normal)
    # # ax[0,2].imshow(rock)
    # ax[0,0].imshow(fluid_static)
    # # ax[1,1].imshow(above)
    # ax[0,1].imshow(reconstruction_static[0,0])
    # ax[0,2].imshow(rock_static)
    # fig.tight_layout()
    # %%
    angles = np.linspace(0, np.pi, num_proj, endpoint=False)
    
    proj_geom = astra.create_proj_geom('parallel3d', 1, 1, num_slice, num_pixel, angles[proj_used_idx]);
    vol_geom = astra.create_vol_geom(num_row, num_col, num_slice) # (rows, columns, slices)
    reconstruction = np.zeros((num_frame, num_slice, num_row, num_col), dtype=np.float32)
    print_step = 0.0
    iterations_used = np.zeros((num_frame), dtype=np.uint16)
    # res_norm = []
    
    rec = np.copy(reconstruction_static[0])
    for iFrame in range(num_frame):
        if np.isclose(iFrame/num_frame, print_step):
            print('{}. Reconstructing time step {} out of {} steps'.format(time.asctime(), iFrame, num_frame))
            print_step += 0.1
        sinograms = projections[iFrame][:, proj_used_idx, :]
        sinograms_id = astra.data3d.create('-sino', proj_geom, sinograms)
        
        alg_id, rec_id = af.setup_reco_alg(reco_type, vol_geom, sinograms_id, rec)
    
        ## %% Reconstruction
        if use_NCP:
            b = sinograms.ravel()
            # mempool.free_all_blocks()
            rec_tmp = np.zeros((5, num_slice, num_row, num_col), dtype=np.float32)
            ncp_tmp = np.zeros((5), dtype=np.float32)
            iItr = 0
            ncp_idx = 0
            idx_min = 0
            
            first_min = True
            itr_beyond = 0
            max_itr_beyond = 10
            num_minima = 0
            max_min = 2
            last_min = 1e6
            
            while iItr < 1000:
                # print(iItr)
                # 166 ms
                astra.algorithm.run(alg_id, 1)
                rec_tmp[ncp_idx] = astra.data3d.get(rec_id)
                
                if reco_type == 4:
                    rec[static_mask] = const.rock_attenuation
                    rec[np.logical_and(dynamic_mask, rec > const.water_attenuation)] = const.water_attenuation
                    rec[np.logical_and(dynamic_mask, rec < const.oil_attenuation)] = const.oil_attenuation
                    rec_tmp[ncp_idx] = rec
                
                # 230-270 ms
                Ax = af.forward_reco(vol_geom, proj_geom, rec_tmp[ncp_idx], gpu_idx=args.gpu).ravel()
                astra.set_gpu_index(args.gpu)
                
                # 63 ms
                c, ncp_tmp[ncp_idx] = af.cuda_ncp(b, Ax, args.gpu[0])

                # print(ncp_tmp, iItr, itr_beyond, max_itr_beyond)
                
                # if np.sum(ncp_tmp[idx_min] < ncp_tmp) == 4 and iItr >= 4:
                #     break
            
                if iItr >= 4:
                    if np.sum(ncp_tmp[idx_min] < ncp_tmp) == 4:
                        # print('\tNCP increasing!')
                        if first_min:
                            num_minima += 1
                            first_min = False
                            reconstruction[iFrame] = rec_tmp[idx_min]
                            iterations_used[iFrame] = iItr-3
                            last_min = ncp_tmp[idx_min]
                        itr_beyond += 1
                        if max_itr_beyond < itr_beyond:
                            break
                            
                    # if np.sum(ncp_tmp[idx_min] > ncp_tmp) == 4:
                    #     print('\tNCP decreasing!')

                    if ncp_tmp.min() == ncp_tmp[2] and ncp_tmp[2] < last_min:
                        # print('\tNCP True minima!')
                        last_min = ncp_tmp[2]
                        reconstruction[iFrame] = rec_tmp[2]
                        iterations_used[iFrame] = iItr-2
                        num_minima += 1
                        if max_min <= num_minima:
                            break
                        if first_min:
                            max_itr_beyond = max(max_itr_beyond, iItr)
                            first_min = False
                
                iItr += 1
                if ncp_idx < 4:
                    ncp_idx += 1
                else:
                    # 9.44 mu s
                    ncp_tmp = np.roll(ncp_tmp, -1)
                    # 122 msa
                    rec_tmp = np.roll(rec_tmp, -1, axis=0)
            # iterations_used[iFrame] = iItr-3
            # reconstruction[iFrame] = rec_tmp[idx_min]
        elif args.use_GT:
            raise ValueError('Option is currently deprecated')
        elif num_itr > 0:
            for iItr in range(num_itr):
                astra.algorithm.run(alg_id, 1)
                
                if reco_type == 4:
                    rec[static_mask] = const.rock_attenuation
                    rec[np.logical_and(dynamic_mask, rec > const.water_attenuation)] = const.water_attenuation
                    rec[np.logical_and(dynamic_mask, rec < const.oil_attenuation)] = const.oil_attenuation
        rec = np.copy(reconstruction[iFrame])
        
        ## Clean up.
        astra.algorithm.delete(alg_id)
        astra.data3d.delete(rec_id)
        astra.data3d.delete(sinograms_id)
    # %%
    # tmp1 = rec_tmp[idx_min,0]; tmp2 = rec[0]; tmp3 = reconstruction[9,0]; tmp4 = rec_tmp[2,0]; tmp5 = rec_tmp[ncp_idx,0]; tmp6 = reconstruction[8,0]
    # %%
    file_path = '/home/pwra/simulation/output_files/noise{}/num_proj_used{}/reco_type{}/'.format(noise, args.num_proj_used, reco_type)
    if use_NCP:
        file_name = 'frozen_dynamic_reconstruction_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}{}.npy'.format(noise, args.num_proj_used, reco_type, 'NCP', time_flip)
        np.save(file_path+file_name, reconstruction)
        
        file_name = 'frozen_dynamic_reconstruction_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}_iterations_used{}.npy'.format(noise, args.num_proj_used, reco_type, 'NCP', time_flip)
        np.save(file_path+file_name, iterations_used)
    elif args.use_GT:
        file_name = 'dynamic_reconstruction_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}.npy'.format(noise, args.num_proj_used, reco_type, 'GT')
        np.save(file_path+file_name, reconstruction)
        
        file_name = 'dynamic_reconstruction_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}_iterations_used.npy'.format(noise, args.num_proj_used, reco_type, 'GT')
        np.save(file_path+file_name, iterations_used)
    else:
        file_name = 'dynamic_reconstruction_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}.npy'.format(noise, args.num_proj_used, reco_type, num_itr)
        np.save(file_path+file_name, reconstruction)
# %%
if __name__ == '__main__':
    # Construct the argument parser
    parser = argparse.ArgumentParser()
    
    # Add the arguments to the parser
    parser.add_argument("-num_proj", "--num_proj", required=False,
                        default=360, type=int,
                        help="Number of projections in simulation.")
    
    parser.add_argument("-num_proj_used", "--num_proj_used", required=False,
                        default=360, type=int,
                        help="Number of projections used for reconstruction.")
    
    parser.add_argument("-noise", "--noise", required=False,
                        default='0.25', type=str, help="Noise level in simulation.")
    
    parser.add_argument("-gpu", "--gpu", required=False,
                        default=[0,1],
                        type=lambda s: [int(item) for item in s.split(',')],
                        help="Selects if GPU 0, 1 or both is used.")
    
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument("-use_NCP", "--use_NCP",
                        default=False, type=lambda s: bool(util.strtobool(s)))
    group.add_argument("-use_GT", "--use_GT",
                        default=False, type=lambda s: bool(util.strtobool(s)))
    group.add_argument("-num_itr", "--num_itr",
                        default=0, type=int)
    
    parser.add_argument("-reco_type", "--reco_type", required=False,
                        default=2, type=int, help="Select reconstruction algorithm.")
    
    parser.add_argument("-rvrs_time", "--reverse_time", required=False,
                        default=False, type=bool, help="Reverses time axis of simulation.")
    
    args = parser.parse_args() #Remember to comment this in if running from terminal
    # args = parser.parse_args(['-use_NCP', 'True'])
    # print(args)
    # %%
    af.reco_message_error(args.reco_type)
    af.stopping_criteria_error_message(args)
    if args.reco_type == 0:
        dynamic_reconstruction_FBP.main(args)
    else:
        main(args)
    
