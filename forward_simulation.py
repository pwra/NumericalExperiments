#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  4 09:54:27 2020

@author: Peter Winkel Rasmussen
"""
import os
os.chdir('/home/pwra/simulation/')
import sys
sys.path.append("/home/pwra/TomographicReconstruction/")
import numpy as np
# import cupy as cp
import astra
import auxiliary_functions as af
import constants as const
import matplotlib.pyplot as plt
import matplotlib.style as style
from matplotlib.widgets import Slider
style.use('seaborn')
style.use('ggplot')
# import matplotlib.animation as animation
from mpl_toolkits.axes_grid1 import make_axes_locatable
params = {'axes.grid': False, 'savefig.dpi': 200,
          'text.color': 'black'}
plt.rcParams.update(params)
astra.set_gpu_index([0,1])

file_path = '/home/pwra/simulation/output_files/'
# %%
simulation = np.load(file_path+'simulation_attenuation_blur.npy') #(t, slice, row, column)
# %%
# import sharpness_functions as sf
# simulation = sf.downscale_data(simulation[:10], 0.5, num_split=4)
num_frame, num_slice, num_row, num_col = simulation.shape
# %%
num_vol = 256
# num_pixel = int(np.pi/2*num_vol) #Number of projections needed for a "good" reconstruction.
num_pixel = int(np.sqrt(2)*num_vol)#const.num_pixel
num_projection = 360
angles = np.linspace(0, np.pi, num_projection, endpoint=False)

vol_geom = astra.create_vol_geom(num_vol, num_vol, num_slice) #(rows, columns, slices)
proj_geom = astra.create_proj_geom('parallel3d', 1, 1, num_slice, num_pixel, angles);
projections = np.empty((num_frame, num_slice, num_projection, num_pixel), dtype=np.float32)

for iFrame in range(num_frame):
    projection_id, projections[iFrame, :, :, :] = astra.create_sino3d_gpu(simulation[iFrame, :, :, :], proj_geom, vol_geom)
# %%
if False:
    projections[1:] = projections[0]
# %%
# intensity = 2.1225e3 # matches 5.00% noise
# intensity = 5.295e4 # matches 1.00% noise
intensity = 8.47125e5 # matches 0.25% noise
# intensity = np.inf

noisy_projections, noise = af.add_noise(projections, intensity, use_GPU=True, num_split=10)
noise_sd = np.std(noise)
test = noise.reshape(num_frame,num_slice*num_projection*num_pixel)
noise_level = np.linalg.norm(noise.ravel())/np.linalg.norm(projections.ravel())*100
print(intensity, noise_level)

file_name = 'simulation_projections_Noise{:.2f}_NumProj{}.npy'.format(noise_level, num_projection)
print(file_name)
# %%
np.save(file_path+'frozen_'+file_name, noisy_projections)
# %%
test_bin, test_edge = np.histogram(test[0], bins=200, range=(-25,25), density=True)
test_center = (test_edge[1:] + test_edge[:-1])/2

sigma = np.std(test[0])
mu = 0

fig, ax = plt.subplots(1,1,squeeze=False)
ax[0,0].plot(test_center, test_bin)
ax[0,0].plot(test_center, 1/(sigma * np.sqrt(2 * np.pi)) *
         np.exp( - (test_center - mu)**2 / (2 * sigma**2) ),
         linewidth=2, linestyle='--')
ax[0,0].set_yscale('log')
# ax[0,0].set_ylim(1,test_bin.max()*1.1)
fig.tight_layout(rect=[0,0,1,1])
# %%
def update_data(val):
    iSlice = int(sslice.val)
    iFrame = int(sframe.val)
    im.set_data(noise[iFrame, iSlice, :, :])
    # arr_min = np.nanmin(noise[iFrame, iSlice, :, :])
    # arr_max = np.nanmax(noise[iFrame, iSlice, :, :])
    # im.set_clim(arr_min, arr_max)

fig = plt.figure(figsize=(9,9))
ax = fig.add_subplot(111)
im = ax.imshow(noise[0,0,:,:], cmap='coolwarm')

# ax.set_title('Slice number = %i' % (0))
axcolor = 'lightgoldenrodyellow'

ax_slice = plt.axes([0.2, 0.02, 0.6, 0.02], facecolor=axcolor)
sslice = Slider(ax_slice, 'Slice', 0, num_row-1, valinit=0, valstep=1)

ax_frame = plt.axes([0.2, 0.05, 0.6, 0.02], facecolor=axcolor)
sframe = Slider(ax_frame, 'Frame', 0, num_frame-1, valinit=0, valstep=1)
im.set_clim(-3*noise_sd, 3*noise_sd)

divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.1)
plt.colorbar(im, cax=cax)

sslice.on_changed(update_data)
sframe.on_changed(update_data)

plt.tight_layout(rect=[0, 0.05, 1, 1])
plt.show()
# %%
def update_data(val):
    iProj = int(sproj.val)
    iFrame = int(sframe.val)
    im.set_data(noisy_projections[iFrame, :, iProj, :])
    arr_min = np.nanmin(noisy_projections[iFrame, :, iProj, :])
    arr_max = np.nanmax(noisy_projections[iFrame, :, iProj, :])
    im.set_clim(arr_min, arr_max)

fig = plt.figure(figsize=(9,6))
ax = fig.add_subplot(111)
im = ax.imshow(noisy_projections[0,:,0,:], cmap='viridis')

# ax.set_title('Slice number = %i' % (0))
axcolor = 'lightgoldenrodyellow'

ax_proj = plt.axes([0.2, 0.02, 0.6, 0.02], facecolor=axcolor)
sproj = Slider(ax_proj, 'Proj', 0, num_projection-1, valinit=0, valstep=1)

ax_frame = plt.axes([0.2, 0.05, 0.6, 0.02], facecolor=axcolor)
sframe = Slider(ax_frame, 'Frame', 0, num_frame-1, valinit=0, valstep=1)

divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.1)
plt.colorbar(im, cax=cax)

sproj.on_changed(update_data)
sframe.on_changed(update_data)

plt.tight_layout(rect=[0, 0.05, 1, 1])
plt.show()
# # %%
# figure_path = '/home/pwra/simulation/'
# fig = plt.figure(figsize=(9,9))
# ax = fig.add_subplot(111)
# im = ax.imshow(noisy_projections[0, :, 0, :], cmap='viridis')
# ax.set_aspect('auto')

# divider = make_axes_locatable(ax)
# cax = divider.append_axes("right", size="5%", pad=0.1)
# plt.colorbar(im, cax=cax)
# plt.tight_layout(rect=[0, 0, 1, 1])
# # fig.savefig(figure_path+'radiograph.pdf')
# # %%
# fig = plt.figure(figsize=(9,9))
# ax = fig.add_subplot(111)
# im = ax.imshow(noisy_projections[0, 0, :, :], cmap='viridis')
# ax.set_aspect('auto')

# divider = make_axes_locatable(ax)
# cax = divider.append_axes("right", size="5%", pad=0.1)
# plt.colorbar(im, cax=cax)
# plt.tight_layout(rect=[0, 0, 1, 1])
# # fig.savefig(figure_path+'sinogram.pdf')
# # %%
# fig = plt.figure(figsize=(9,9))
# ax = fig.add_subplot(111)
# im = ax.imshow(noise[0, :, 0, :], cmap='viridis')
# ax.set_aspect('auto')

# divider = make_axes_locatable(ax)
# cax = divider.append_axes("right", size="5%", pad=0.1)
# plt.colorbar(im, cax=cax)
# plt.tight_layout(rect=[0, 0, 1, 1])
# fig.savefig(figure_path+'sinogram.pdf')