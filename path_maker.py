#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr  2 11:01:10 2020

@author: Peter Winkel Rasmussen
"""
import os

noise_list = [0.0, 0.25, 1.1, 5.0]
num_proj_used_list = [20, 45, 90, 180, 360]

for iNoise, noise in enumerate(noise_list):
    noise_path = '/home/pwra/simulation/output_files/noise{}'.format(noise)
    os.mkdir(noise_path)
    for iProj_used, num_proj_used in enumerate(num_proj_used_list):
        proj_path = '{}/num_proj_used{}'.format(noise_path, num_proj_used)
        os.mkdir(proj_path)
        for iReco in range(4):
            reco_path = '{}/reco_type{}'.format(proj_path, iReco)
            os.mkdir(reco_path)