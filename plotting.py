#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 10 13:55:58 2020

@author: Peter Winkel Rasmussen
"""
# import os
# import sys
import numpy as np
# import cupy as cp
import argparse
from skimage.draw import circle
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.style as style
from matplotlib.widgets import Slider
style.use('seaborn')
style.use('ggplot')
from cycler import cycler
from matplotlib.colors import hsv_to_rgb, to_rgba
# import matplotlib.animation as animation
from mpl_toolkits.axes_grid1 import make_axes_locatable
params = {'axes.grid': False, 'savefig.dpi': 200,
          'text.color': 'black'}
plt.rcParams.update(params)

SMALL_SIZE = 13
MEDIUM_SIZE = 15
BIGGER_SIZE = 17

plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title


# file_path = '/home/pwra/simulation/output_files/'
file_path_plots = '/home/pwra/simulation/plots/'
# ground_truth = np.load(file_path+'simulation_attenuation.npy')
# num_frame, num_slice, num_row, num_col = ground_truth.shape

H = np.linspace(0, 1, 11)
V = np.ones_like(H)
S = np.ones_like(H)
HSV = np.dstack((H,S,V))
seed = 137
np.random.seed(seed)
RGB = hsv_to_rgb(HSV)[0][:-1]
# np.random.shuffle(RGB)
# %
plt.close('all')
default_cycler = (
                    cycler(color=RGB)
#                    cycler(color=['aqua', 'black', 'blue', 'brown',
#                                'chartreuse', 'chocolate', 'coral', 'crimson',
#                                'darkblue', 'darkgreen', 'fuchsia', 'goldenrod',
#                                'green', 'grey', 'indigo', 'lime', 
#                                'magenta', 'maroon', 'navy', 'olive'])
                    + cycler(linestyle=['-', '--', '-', '--', '-',
                                    '--', '-', '--', '-', '--',
                                    # '-', '--', '-', '--', '-',
                                    # '--','-', '--', '-', '--',
                                    ])
                                    # '-.', ':', '-.', ':',
                                    # '-.', ':', '-.', ':',
                                    # '-', ':', '-', ':'])
#                  + cycler(marker=['.', '.', '.', '.',  
#                                 '.', '.', '.', '.', 
#                                 '.', '.', '.', '.', 
#                                 '.', '.', '.', '.',])
                 )
plt.rc('axes', prop_cycle=default_cycler)
# %%
def main(args):
    # %%
    noise = args['noise']
    num_proj_used = args['num_proj_used']
    reco_type = args['reco_type']
    if reco_type == 0:
        num_itr_list = [None]
    elif any(reco_type == np.array([1,2,3])):
        num_itr_list = ['None', 1, 5, 10, 15, 20, 50, 100, 500, 'NCP', 'None']
    else:
        raise ValueError('Reco type does not exist!\n'
                         'Select 0,1,2,3 instead of {}'.format(reco_type))
    
    num_bins = 400
    bin_values = np.zeros((len(num_itr_list), num_bins), dtype=np.float32)
    min_edge = -2.5
    max_edge = 2.5
    bin_edges = np.linspace(min_edge, max_edge, num_bins+1, endpoint=True, dtype=np.float32)
    # tmp_edge = cp.array(bin_edges)
    bin_centers = (bin_edges[:-1]+bin_edges[1:])/2
    # %%
    file_path = '/home/pwra/simulation/output_files/noise{}/num_proj_used{}/reco_type{}/'.format(noise, num_proj_used, reco_type)
    file_name = 'res_l2_norm_Noise{}_NumProjUsed{}_RecoType{}.npy'.format(noise, num_proj_used, reco_type)
    res_l2_norm = np.load(file_path+file_name)
    
    file_name = 'res_l1_norm_Noise{}_NumProjUsed{}_RecoType{}.npy'.format(noise, num_proj_used, reco_type)
    res_l1_norm = np.load(file_path+file_name)
    
    file_name = 'res_binned_Noise{}_NumProjUsed{}_RecoType{}.npy'.format(noise, num_proj_used, reco_type)
    bin_values = np.load(file_path+file_name)
    
    if any(reco_type == np.array([1,2,3])):
        file_path = '/home/pwra/simulation/output_files/noise{}/num_proj_used{}/reco_type{}/'.format(noise, num_proj_used, 0)
        file_name = 'res_l2_norm_Noise{}_NumProjUsed{}_RecoType{}.npy'.format(noise, num_proj_used, 0)
        # res_l2_norm = np.insert(res_l2_norm, [-1], np.load(file_path+file_name), axis=0)
        res_l2_norm = np.append(res_l2_norm, np.load(file_path+file_name), axis=0)
        
        file_name = 'res_l1_norm_Noise{}_NumProjUsed{}_RecoType{}.npy'.format(noise, num_proj_used, 0)
        # res_l1_norm = np.insert(res_l1_norm, [-1], np.load(file_path+file_name), axis=0)
        res_l1_norm = np.append(res_l1_norm, np.load(file_path+file_name), axis=0)
        
        file_name = 'res_binned_Noise{}_NumProjUsed{}_RecoType{}.npy'.format(noise, num_proj_used, 0)
        bin_values = np.append(bin_values, np.load(file_path+file_name), axis=0)
        # bin_values = np.insert(bin_values, [-1], np.load(file_path+file_name), axis=0)

    file_path = '/home/pwra/simulation/output_files/noise{}/num_proj_used{}/reco_type{}/'.format(0.23, 720, 0)
    file_name = 'res_l2_norm_Noise{}_NumProjUsed{}_RecoType{}.npy'.format(0.23, 720, 0)
    res_l2_norm = np.insert(res_l2_norm, [0], np.load(file_path+file_name), axis=0)
    
    file_name = 'res_l1_norm_Noise{}_NumProjUsed{}_RecoType{}.npy'.format(0.23, 720, 0)
    res_l1_norm = np.insert(res_l1_norm, [0], np.load(file_path+file_name), axis=0)
    
    file_name = 'res_binned_Noise{}_NumProjUsed{}_RecoType{}.npy'.format(0.23, 720, 0)
    bin_values = np.insert(bin_values, [0], np.load(file_path+file_name), axis=0)
    # %%
    reco_means = np.zeros((bin_values.shape[0], 1), dtype=np.float32)
    for i in range(bin_values.shape[0]):
        reco_means[i] = np.average(bin_centers[None, :], weights=bin_values[i], axis=1)
    reco_var = np.average((bin_centers - reco_means)**2, weights=bin_values, axis=1)
    reco_sd = np.sqrt(reco_var)
    
    overall_means = np.mean(res_l2_norm, axis=1)
    min_idx = np.argsort(overall_means[1:])[0]    
    # %%
    if args['compare_recos']:
        num_itr_list_noFBP = [1, 5, 10, 15, 20, 50, 100, 500]
        file_path = '/home/pwra/simulation/output_files/noise{}/'.format(noise)
        f = open(file_path+'best_performance.txt', 'a')
        file_path = '/home/pwra/simulation/output_files/noise{}/num_proj_used{}/reco_type{}/'.format(noise, num_proj_used, reco_type)
        file_name = 'dynamic_reconstruction_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}.npy\n'.format(noise, num_proj_used, reco_type, num_itr_list_noFBP[min_idx])
        f.write(file_path+file_name)
        f.close()
    # %%
    fig, ax = plt.subplots(1,1, squeeze=False, figsize=(9,8))
    for iData in range(res_l2_norm.shape[0]):
        if iData == 0:
            label_name = 'FBP 720\n0.25% noise.'
        elif iData == res_l2_norm.shape[0] - 1:
            label_name = 'FBP {}\n{}% noise.'.format(num_proj_used, noise)
        else:
            label_name = 'NumItr {}'.format(num_itr_list[iData])
        ax[0,0].plot(res_l2_norm[iData], label=label_name)
    ax[0,0].set_ylim(100, 3500)
    ax[0,0].set_xlim(0, 100)
    ax[0,0].set_yscale('log')
    ax[0,0].set_yticks([100, 200, 300, 400, 500, 600, 700, 800, 900, 1000,
                        2000, 3000])
    ax[0,0].get_yaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
    box = ax[0,0].get_position()
    ax[0,0].set_position([box.x0, box.y0, box.width * 0.8, box.height])
    ax[0,0].legend(loc='center left', bbox_to_anchor=(1, 0.5))
    # ax[0,0].legend(loc='upper right', bbox_to_anchor=(0.5, 0.4, 0.5, 0.5))
    ax[0,0].grid(True, which="both", ls="-")
    ax[0,0].set_xlabel('Time step in simulation')
    ax[0,0].set_ylabel('|reconstruction - ground truth|')
    fig.suptitle('Noise = {}%. Number of projections used {}. Reconstruction type {}.'.format(noise, num_proj_used, reco_type))
    fig.tight_layout(rect=[0, 0, 0.85, 0.94])
    file_name = 'TimeStepResidual_l2_norm_Noise{}_NumProjUsed{}_RecoType{}.pdf'.format(noise, num_proj_used, reco_type)
    fig.savefig(file_path_plots+file_name)
    # %%
    fig, ax = plt.subplots(1,1, squeeze=False, figsize=(9,8))
    for iData in range(res_l1_norm.shape[0]):
        if iData == 0:
            label_name = 'FBP 720\n0.25% noise.'
        elif iData == res_l1_norm.shape[0] - 1:
            label_name = 'FBP {}\n{}% noise.'.format(num_proj_used, noise)
        else:
            label_name = 'NumItr {}'.format(num_itr_list[iData])
        ax[0,0].plot(res_l1_norm[iData], label=label_name)
    ax[0,0].set_ylim(2e5, 7.5e6)
    ax[0,0].set_xlim(0, 100)
    ax[0,0].set_yscale('log')
    # ax[0,0].set_yticks([2e5, 3e5, 4e5, 5e5, 6e5, 7e5, 8e5, 9e5, 1e6,
    #                     2e6, 3e6, 4e6, 5e6, 6e6, 7e6, 8e6])
    ax[0,0].set_yticks([2e5, 4e5, 6e5, 8e5, 1e6,
                        2e6, 4e6, 6e6, 8e6])
    ax[0,0].get_yaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
    box = ax[0,0].get_position()
    ax[0,0].set_position([box.x0, box.y0, box.width * 0.8, box.height])
    ax[0,0].legend(loc='center left', bbox_to_anchor=(1, 0.5))
    # ax[0,0].legend(loc='upper right', bbox_to_anchor=(0.5, 0.4, 0.5, 0.5))
    ax[0,0].grid(True, which="both", ls="-")
    ax[0,0].set_xlabel('Time step in simulation')
    ax[0,0].set_ylabel('|reconstruction - ground truth|')
    fig.suptitle('Noise = {}%. Number of projections used {}. Reconstruction type {}.'.format(noise, num_proj_used, reco_type))
    fig.tight_layout(rect=[0, 0, 0.85, 0.94])
    file_name = 'TimeStepResidual_l1_norm_Noise{}_NumProjUsed{}_RecoType{}.pdf'.format(noise, num_proj_used, reco_type)
    fig.savefig(file_path_plots+file_name)
    # %%
    fig, ax = plt.subplots(5,2, squeeze=False, figsize=(12,17))
    
    label_name = 'FBP {}\n{}% noise.'.format(num_proj_used, noise)
    ax[0,0].fill_between(bin_centers, 0, bin_values[-1, :],
                                        step='mid', facecolor=to_rgba(RGB[-1], 0.5), edgecolor='k',
                                        linewidth=1.0, label=label_name)
    # ax[0,0].set_yscale('log')
    # ax[0,0].grid()
    
    for iRow in range(ax.shape[0]):
        for iCol in range(ax.shape[1]):
            iData = iCol + iRow*ax.shape[1]
            if iData == 0:
                ax[iRow, iCol].set_title('FBP')
                label_name = 'FBP 720\n0.25% noise.'
            else:
                ax[iRow, iCol].set_title('Number of iterations {}.'.format(num_itr_list[iData]))     
                label_name = 'NumItr {}'.format(num_itr_list[iData])
            ax[iRow, iCol].fill_between(bin_centers, 0, bin_values[iData, :],
                                        step='mid', facecolor=to_rgba(RGB[iData], 0.5), edgecolor='k',
                                        linewidth=1.0, label=label_name)
            ax[iRow, iCol].set_yscale('log')
            ax[iRow, iCol].grid()
            
    ax[0,0].legend(loc='best')
    fig.suptitle('Noise = {}%. Number of projections used {}. Reconstruction type {}.'.format(noise, num_proj_used, reco_type))
    fig.tight_layout(rect=[0, 0, 1, 0.94])
    file_name = 'ResidualDistribution_Noise{}_NumProjUsed{}_RecoType{}.pdf'.format(noise, num_proj_used, reco_type)
    fig.savefig(file_path_plots+file_name)
    # %%
    # transparent = to_rgba(np.array([0,0,0]), alpha=0)
    # FBP_idx = np.argwhere(np.argsort(reco_sd) == 0)[0,0]
    # sd_sort = np.flip(np.argsort(reco_sd[1:])+1)
    # sd_sort = np.insert(sd_sort, FBP_idx-1, [0,0])
    
    # largest_sd_idx = np.argsort(reco_sd)[-1]
    # x_min = np.nonzero(bin_values[largest_sd_idx, :] > 0)[0][0]
    # x_max = np.nonzero(bin_values[largest_sd_idx, :] > 0)[0][-1]
    # # %%
    
    # fig, ax = plt.subplots(1,2, squeeze=False, figsize=(16,8))
    # hist_color = np.array([RGB[0], RGB[5], RGB[1], RGB[6], RGB[2],
    #                        RGB[3], RGB[7], RGB[4], RGB[8], RGB[9]])
    # fill_colors = [to_rgba(color, alpha=1) for iRGB, color in enumerate(hist_color)]
    # for iRow in range(ax.shape[1]):
    #     for iCol in range(sd_sort.shape[0]//2):
    #         count = iCol + iRow*sd_sort.shape[0]//2
    #         print(RGB[count])
    #         # print(iRow, sd_sort[count])
    #         label_name = 'NumItr {}'.format(num_itr_list[sd_sort[count]])
    #         ax[0,iRow].fill_between(bin_centers, 0, bin_values[sd_sort[count], :],
    #                                     step='mid', facecolor=fill_colors[count], edgecolor='k',
    #                                     linewidth=1.0, label=label_name)
    #     ax[0,iRow].legend(loc='best')
    #     ax[0,iRow].set_yscale('log')
    #     ax[0,iRow].set_ylim(0.1, 1e9)
    #     ax[0,iRow].grid()
    #     ax[0,iRow].set_xlim(bin_edges[x_min-10], bin_edges[x_max+10])
    # for iRow in range(2):
    #     for iCol in range(sd_sort.shape[0]//2):
    #         count = iCol + iRow*sd_sort.shape[0]//2
    #         # print(iRow, sd_sort[count])
    #         ax[0,iRow].fill_between(bin_centers, 0, bin_values[sd_sort[count], :],
    #                                     step='mid', facecolor=transparent, edgecolor='k',
    #                                     linewidth=1.0)
    # fig.suptitle('Noise = {}%. Number of projections used {}. Reconstruction type {}.'.format(noise, num_proj_used, reco_type))
    # fig.tight_layout(rect=[0, 0, 1, 0.94])
    # file_name = 'ResidualDistribution2_Noise{}_NumProjUsed{}_RecoType{}.pdf'.format(noise, num_proj_used, reco_type)
    # fig.savefig(file_path_plots+file_name)
# %%
if __name__ == '__main__':
    # Construct the argument parser
    parser = argparse.ArgumentParser()
    
    # Add the arguments to the parser
    parser.add_argument("-num_proj_used", "--num_proj_used", required=False,
                        nargs='?', const=1, default=90, type=int, help="first operand")
    parser.add_argument("-noise", "--noise", required=False,
                        nargs='?', const=1, default='1.1', type=str, help="second operand")
    parser.add_argument("-reco_type", "--reco_type", required=False,
                        nargs='?', const=1, default=2, type=int, help="third operand")
    parser.add_argument("-compare_recos", "--compare_recos", required=False,
                        nargs='?', const=1, default=False, type=bool, help="third operand")
    args = vars(parser.parse_args())
    # %%
    main(args)