#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 19 13:14:28 2020

@author: Peter Winkel Rasmussen
"""
import os
import time
os.chdir('/home/pwra/simulation/')
import numpy as np
import argparse
import auxiliary_functions as af
# from skimage.draw import circle
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.style as style
# from matplotlib.widgets import Slider
style.use('seaborn')
style.use('ggplot')
from cycler import cycler
from matplotlib.colors import hsv_to_rgb, to_rgba, LogNorm
# import matplotlib.animation as animation
from mpl_toolkits.axes_grid1 import make_axes_locatable
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from matplotlib.ticker import NullFormatter
params = {'axes.grid': False, 'savefig.dpi': 200,
          'text.color': 'black',
          'axes.labelcolor': 'black',
          'xtick.color': 'black',
          'ytick.color': 'black',
          'lines.linewidth': 4,
          'patch.linewidth': 3,
          'lines.markersize': 10}
plt.rcParams.update(params)

SMALL_SIZE = 13+6
MEDIUM_SIZE = 15+6
BIGGER_SIZE = 17+6

plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=MEDIUM_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE+5)  # fontsize of the figure title

file_path_plots = '/home/pwra/simulation/plots/'
file_path_supplementary = '/home/pwra/simulation/supplementary_plots/'

reco_names = {0 : 'FBP',
             1 : 'SIRT',
             2 : 'SIRT-BC',
             3 : 'SIRT-IC',
             4 : 'SIRT-LC',
             5 : 'FBP ideal'}

# RGB = af.hsv_colors(15)
RGB = np.array([[228,26,28],
                [55,126,184],
                [77,175,74],
                [152,78,163],
                [255,127,0],
                [166,206,227]])/255
default_cycler = cycler(color=RGB)
plt.rc('axes', prop_cycle=default_cycler)
linestyle_tuple = [
    ('solid', (0, ())),
    ('dotted',                (0, (1, 1))),
    ('dashed',                (0, (5, 5))),
    ('dashdotted',            (0, (3, 5, 1, 5))),
    ('densely dashdotdotted', (0, (3, 1, 1, 1, 1, 1)))]

transparent = to_rgba(np.array([0,0,0]), 0)
# %%
def main():
    # %%
    num_proj_used_lst = args.num_proj_used
    noise_lst = args.noise
    reco_type_lst = args.reco_type
    
    scale = 2
    num_frames = 10
    num_pixels = 256
    bin_width = 0.02
    
    min_edge = -10
    max_edge = 10
    num_bins = (max_edge-min_edge)/bin_width
    if num_bins.is_integer():
        num_bins = int(num_bins)
    else:
        raise ValueError('bin_width or bin_range results in non-integer'+
                         'number of bins')
    bin_edges = np.linspace(min_edge, max_edge, num_bins+1, endpoint=True, dtype=np.float32)
    bin_centers = (bin_edges[:-1]+bin_edges[1:])/2
    
    min_val = -10
    max_val = 15
    num_bins_dist = (max_val-min_val)/bin_width
    if num_bins_dist.is_integer():
        num_bins_dist = int(num_bins_dist)
    else:
        raise ValueError('bin_width or bin_range results in non-integer'+
                         'number of bins')
    dist_edges = np.linspace(min_val, max_val, num_bins_dist+1, endpoint=True, dtype=np.float32)
    dist_centers = (dist_edges[:-1]+dist_edges[1:])/2
    num_data = len(num_proj_used_lst)*len(noise_lst)*len(reco_type_lst)
    # %%
    res_l2_norm = np.zeros((len(num_proj_used_lst), len(noise_lst), len(reco_type_lst),
                            num_frames), dtype=np.float32)
    res_l1_norm = np.zeros((len(num_proj_used_lst), len(noise_lst), len(reco_type_lst),
                            num_frames), dtype=np.float32)
    bin_values = np.zeros((len(num_proj_used_lst), len(noise_lst), len(reco_type_lst),
                            num_bins), dtype=np.float32)
    dist_values = np.zeros((len(num_proj_used_lst), len(noise_lst), len(reco_type_lst),
                            num_bins_dist), dtype=np.float32)
    bin_values_time = np.zeros((len(num_proj_used_lst), len(noise_lst), len(reco_type_lst),
                            num_frames, num_bins), dtype=np.float32)
    if args.load_reco:
        recos = np.zeros((len(num_proj_used_lst), len(noise_lst), len(reco_type_lst),
                          num_frames, num_pixels, num_pixels*scale, num_pixels*scale),
                         dtype=np.float32)
    
    num_ncp = np.sum(np.array(reco_type_lst) != 0)
    if num_ncp > 0:
        iterations_used = np.zeros((len(num_proj_used_lst), len(noise_lst), num_ncp, 
                            num_frames), dtype=np.float32)       
    file_labels = []
    
    for iProj, num_proj_used in enumerate(num_proj_used_lst):
        for iNoise, noise in enumerate(noise_lst):
            ncp_counter = 0
            for iReco, reco_type in enumerate(reco_type_lst):
                count = iReco + iNoise*len(reco_type_lst) + iProj*len(noise_lst)*len(reco_type_lst)
                print('{}. Loading dataset {} out of {}'.format(time.asctime(), count, num_data-1))
                if reco_type == 0:
                    num_itr = 'None'
                else:
                    num_itr = 'NCP'
                file_path = '/home/pwra/simulation/output_files/noise{}/num_proj_used{}/reco_type{}/'.format(noise, num_proj_used, reco_type)
                file_name = 'res_l2_norm_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}.npy'.format(noise, num_proj_used, reco_type, num_itr)
                res_l2_norm[iProj, iNoise, iReco] = np.load(file_path+file_name)
                
                file_name = 'res_l1_norm_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}.npy'.format(noise, num_proj_used, reco_type, num_itr)
                res_l1_norm[iProj, iNoise, iReco] = np.load(file_path+file_name)
                
                file_name = 'rec_binned_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}.npy'.format(noise, num_proj_used, reco_type, num_itr)
                dist_values[iProj, iNoise, iReco] = np.load(file_path+file_name)
                
                file_name = 'res_binned_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}.npy'.format(noise, num_proj_used, reco_type, num_itr)
                bin_values[iProj, iNoise, iReco] = np.load(file_path+file_name)
                
                file_name = 'res_binned_time_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}.npy'.format(noise, num_proj_used, reco_type, num_itr)
                bin_values_time[iProj, iNoise, iReco] = np.load(file_path+file_name)
                
                if args.load_reco:
                    file_name = 'dynamic_reconstruction_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}.npy'.format(noise, num_proj_used, reco_type, num_itr)
                    recos[iProj, iNoise, iReco] = np.load(file_path+file_name)
                
                if reco_type != 0:
                    file_path = '/home/pwra/simulation/output_files/noise{}/num_proj_used{}/reco_type{}/'.format(noise, num_proj_used, reco_type)
                    file_name = 'dynamic_reconstruction_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}_iterations_used.npy'.format(noise, num_proj_used, reco_type, 'NCP')
                    iterations_used[iProj, iNoise, ncp_counter] = np.load(file_path+file_name)
                    ncp_counter += 1
                
                tmp_lst = [[num_proj_used], [noise], [reco_type]]
                file_labels.append(tmp_lst)
    
    noise, num_proj_used, reco_type, num_itr = '0.25', '720', '0', 'None'
    file_path = '/home/pwra/simulation/output_files/noise{}/num_proj_used{}/reco_type{}/'.format(noise, num_proj_used, reco_type)
    file_name = 'dynamic_reconstruction_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}.npy'.format(noise, num_proj_used, reco_type, num_itr)
    recos_FBP = np.load(file_path+file_name)
      
    file_path = '/home/pwra/simulation/output_files/noise{}/num_proj_used{}/reco_type{}/'.format(noise, num_proj_used, reco_type)
    file_name = 'res_l2_norm_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}.npy'.format(noise, num_proj_used, reco_type, num_itr)
    res_l2_norm_FBP = np.load(file_path+file_name)
    
    file_name = 'res_l1_norm_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}.npy'.format(noise, num_proj_used, reco_type, num_itr)
    res_l1_norm_FBP = np.load(file_path+file_name)
    
    file_name = 'rec_binned_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}.npy'.format(noise, num_proj_used, reco_type, num_itr)
    dist_values_FBP = np.load(file_path+file_name)
    
    file_name = 'res_binned_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}.npy'.format(noise, num_proj_used, reco_type, num_itr)
    bin_values_FBP = np.load(file_path+file_name)
    
    file_name = 'res_binned_time_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}.npy'.format(noise, num_proj_used, reco_type, num_itr)
    bin_values_time_FBP = np.load(file_path+file_name)
    
    file_labels = np.array(file_labels)
    
    ground_truth_distribution = np.load('output_files/ground_truth_distribution.npy')
    ground_truth = np.load('output_files/simulation_attenuation_rotated.npy')
    # %% Across projections - l2-norm
    # Not to be used
    line_style = ['-','--','-.']
    fig, ax = plt.subplots(1,3, squeeze=False, figsize=(25,16))
    for iProj, num_proj_used in enumerate(num_proj_used_lst):
        for iNoise, noise in enumerate(noise_lst):
            for iReco, reco_type in enumerate(reco_type_lst):
                iData = iReco + iNoise*len(reco_type_lst) + iProj*len(noise_lst)*len(reco_type_lst)
                # print(iData)
                label_name =  'num_proj{}\nnoise{}\nreco_type{}'.format(file_labels[iData,0,0],
                                        file_labels[iData,1,0], file_labels[iData,2,0])
                ax[0,iProj].plot(res_l2_norm[iProj, iNoise, iReco], line_style[iNoise], label=label_name)
                ax[0,iProj].set_title('NumProj{}'.format(num_proj_used))
    for iNoise in range(ax.shape[1]):
        # ax[0,iNoise].set_ylim(200, 7000)
        ax[0,iNoise].set_xlim(0, num_frames-1)
        ax[0,iNoise].set_yscale('log')
        ax[0,iNoise].set_yticks([60, 100, 200, 300, 400, 500, 600, 700, 800, 900,
                            1000, 2000, 3000, 4000, 5000, 6000, 7000])
        ax[0,iNoise].get_yaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
        box = ax[0,iNoise].get_position()
        ax[0,iNoise].set_position([box.x0, box.y0, box.width * 0.8, box.height])
        # ax[0,iNoise].legend(loc='center left', bbox_to_anchor=(1.025, 0.55), ncol=1)
        # ax[0,iNoise].legend(loc='upper right', bbox_to_anchor=(0.5, 0.4, 0.5, 0.5))
        ax[0,iNoise].grid(True, which="both", ls="-")
        ax[0,iNoise].set_xlabel('Time step in simulation')
        ax[0,iNoise].set_ylabel('|reconstruction - ground truth|')
        # fig.suptitle('Noise = {}%. Number of projections used {}. Reconstruction type {}.'.format(noise, num_proj_used, reco_type))
        fig.tight_layout(rect=[0, 0, 1, 0.94])
    # file_name = 'TimeStepResidual_l2_norm_Noise{}_NumProjUsed{}_RecoType{}.pdf'.format(noise, num_proj_used, reco_type)
    # fig.savefig(file_path_plots+file_name)
    
    # %% Across noise - l2-norm
    # Not to be used

    fig, ax = plt.subplots(1,3, squeeze=False, figsize=(25,16))
    for iProj, num_proj_used in enumerate(num_proj_used_lst):
        for iNoise, noise in enumerate(noise_lst):
            for iReco, reco_type in enumerate(reco_type_lst):
                iData = iReco + iNoise*len(reco_type_lst) + iProj*len(noise_lst)*len(reco_type_lst)
                # print(iData)
                label_name =  'num_proj{}\nnoise{}\nreco_type{}'.format(file_labels[iData,0,0],
                                        file_labels[iData,1,0], file_labels[iData,2,0])
                ax[0,iNoise].plot(res_l2_norm[iProj, iNoise, iReco], linestyle=linestyle_tuple[iProj][1], label=label_name)
                ax[0,iNoise].set_title('noise{}'.format(noise))
    for iNoise in range(ax.shape[1]):
        ax[0,iNoise].set_ylim(200, 7000)
        ax[0,iNoise].set_xlim(0, num_frames-1)
        ax[0,iNoise].set_yscale('log')
        ax[0,iNoise].set_yticks([60, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000,
                            2000, 3000, 4000, 5000, 6000, 7000])
        ax[0,iNoise].get_yaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
        box = ax[0,iNoise].get_position()
        ax[0,iNoise].set_position([box.x0, box.y0, box.width * 0.8, box.height])
        # ax[0,iNoise].legend(loc='center left', bbox_to_anchor=(1.025, 0.55), ncol=1)
        # ax[0,iNoise].legend(loc='upper right', bbox_to_anchor=(0.5, 0.4, 0.5, 0.5))
        ax[0,iNoise].grid(True, which="both", ls="-")
        ax[0,iNoise].set_xlabel('Time step in simulation')
        ax[0,iNoise].set_ylabel('|reconstruction - ground truth|')
        # fig.suptitle('Noise = {}%. Number of projections used {}. Reconstruction type {}.'.format(noise, num_proj_used, reco_type))
        fig.tight_layout(rect=[0, 0, 1, 0.94])
    # file_name = 'TimeStepResidual_l2_norm_Noise{}_NumProjUsed{}_RecoType{}.pdf'.format(noise, num_proj_used, reco_type)
    # fig.savefig(file_path_plots+file_name)
    # %% Matrix - l2-norm
    RGB = np.array([[228,26,28],
                [55,126,184],
                [77,175,74],
                [152,78,163],
                [255,127,0],
                [166,206,227]])/255
    default_cycler = cycler(color=RGB)
    plt.rc('axes', prop_cycle=default_cycler)
    yticks_pos = [100, 200, 400, 600, 800, 1000, 2000, 4000, 6000, 8000, 8000]
    yticks_labels = [str(i) for i in yticks_pos]
    tick_min = [0, 0, 0]
    tick_max = [-5, -4, -1]
    
    fig, ax = plt.subplots(3,3, squeeze=False, figsize=(16,16))
    for iProj, num_proj_used in enumerate(num_proj_used_lst):
        for iNoise, noise in enumerate(noise_lst):
            ax[iNoise,iProj].plot(res_l2_norm_FBP[0], label=reco_names[5], color=RGB[-1])
            for iReco, reco_type in enumerate(reco_type_lst):
                iData = iReco + iNoise*len(reco_type_lst) + iProj*len(noise_lst)*len(reco_type_lst)
                label_name = reco_names[reco_type]
                ax[iNoise,iProj].plot(res_l2_norm[iProj, iNoise, iReco], linestyle=linestyle_tuple[iReco][1], label=label_name)
                
    
    for iProj, num_proj_used in enumerate(num_proj_used_lst):
        for iNoise, noise in enumerate(noise_lst):
            ax[iNoise,iProj].set_xlim(0, 100)
            ax[iNoise,iProj].set_yscale('log')
            ax[iNoise,iProj].set_yticks(yticks_pos[tick_min[iNoise]:tick_max[iNoise]])
            ax[iNoise,iProj].set_yticklabels(yticks_labels[tick_min[iNoise]:tick_max[iNoise]], minor=False)
            ax[iNoise,iProj].grid(True, which="both", ls="-")
            ax[iNoise,iProj].set_yticks([], minor=True)
            ax[iNoise,iProj].yaxis.set_major_formatter(NullFormatter())
            ax[iNoise,iProj].xaxis.set_major_formatter(NullFormatter())
            ax[iNoise,iProj].set_title('$\\rho$ = {}%. {} = {}.'.format(noise, '$N_{\\mathrm{proj}}$', num_proj_used))
    for iNoise, noise in enumerate(noise_lst):
        ax[iNoise,0].set_ylabel('$||x - \\bar{x}||_2$')
        ax[iNoise,0].set_yticks(yticks_pos[tick_min[iNoise]:tick_max[iNoise]])
        ax[iNoise,0].set_yticklabels(yticks_labels[tick_min[iNoise]:tick_max[iNoise]], minor=False)
        ax[iNoise,0].set_yticks([], minor=True)
    for iProj, num_proj_used in enumerate(num_proj_used_lst):
        ax[-1,iProj].set_xticks([0,25,50,75,100])
        ax[-1,iProj].set_xticklabels(['0','25','50','75','100'])
        ax[-1,iProj].set_xlabel('Time step in simulation')
    ax[0,0].legend(loc='center', bbox_to_anchor=(0.50, 0.825),
                   ncol=2, columnspacing=0.65)
    fig.tight_layout(rect=[0, 0, 1, 1], w_pad=2.0, h_pad=2.0)
    file_name = 'Matrix_l2_norm_paper.pdf'
    fig.savefig(file_path_supplementary+file_name)
    # %% Matrix - l1-norm
    RGB = np.array([[228,26,28],
                [55,126,184],
                [77,175,74],
                [152,78,163],
                [255,127,0],
                [166,206,227]])/255
    default_cycler = cycler(color=RGB)
    yticks_pos = [1e5, 2e5, 4e5, 6e5, 8e5,
                  1e6, 2e6, 4e6, 6e6, 8e6,
                  1e7, 2e7, 4e7, 4e7]
    yticks_labels = ['$1^5$', '$2^5$', '$4^5$', '$6^5$', '$8^5$',
                     '$1^6$', '$2^6$', '$4^6$', '$6^6$', '$8^6$',
                     '$1^7$', '$2^7$', '$4^7$', '$4^7$']
    tick_min = [1, 1, 1]
    tick_max = [-5, -5, -1]
    
    fig, ax = plt.subplots(3,3, squeeze=False, figsize=(16,16))
    for iProj, num_proj_used in enumerate(num_proj_used_lst):
        for iNoise, noise in enumerate(noise_lst):
            ax[iNoise,iProj].plot(res_l1_norm_FBP[0], label=reco_names[5], color=RGB[-1])
            for iReco, reco_type in enumerate(reco_type_lst):
                iData = iReco + iNoise*len(reco_type_lst) + iProj*len(noise_lst)*len(reco_type_lst)
                # print(iData)
                label_name = reco_names[reco_type]
                ax[iNoise,iProj].plot(res_l1_norm[iProj, iNoise, iReco], linestyle=linestyle_tuple[iReco][1], label=label_name)
                
    
    for iProj, num_proj_used in enumerate(num_proj_used_lst):
        for iNoise, noise in enumerate(noise_lst):
            ax[iNoise,iProj].set_xlim(0, 100)
            ax[iNoise,iProj].set_yscale('log')
            ax[iNoise,iProj].set_yticks(yticks_pos[tick_min[iNoise]:tick_max[iNoise]])
            ax[iNoise,iProj].set_yticklabels(yticks_labels[tick_min[iNoise]:tick_max[iNoise]], minor=False)
            ax[iNoise,iProj].grid(True, which="both", ls="-")
            ax[iNoise,iProj].set_yticks([], minor=True)
            ax[iNoise,iProj].yaxis.set_major_formatter(NullFormatter())
            ax[iNoise,iProj].xaxis.set_major_formatter(NullFormatter())
            ax[iNoise,iProj].set_title('$\\rho$ = {}%. {} = {}.'.format(noise, '$N_{\\mathrm{proj}}$', num_proj_used))
    for iNoise, noise in enumerate(noise_lst):
        ax[iNoise,0].set_ylabel('$||x - \\bar{x}||_1$')
        ax[iNoise,0].set_yticks(yticks_pos[tick_min[iNoise]:tick_max[iNoise]])
        ax[iNoise,0].set_yticklabels(yticks_labels[tick_min[iNoise]:tick_max[iNoise]], minor=False)
        ax[iNoise,0].set_yticks([], minor=True)
        # ax[iNoise,0].get_yaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
        # ax[iNoise,0].ticklabel_format(axis='y',style='sci', scilimits=(0,0))
    for iProj, num_proj_used in enumerate(num_proj_used_lst):
        ax[-1,iProj].set_xticks([0,25,50,75,100])
        ax[-1,iProj].set_xticklabels(['0','25','50','75','100'])
        ax[-1,iProj].set_xlabel('Time step in simulation')
    ax[0,0].legend(loc='center', bbox_to_anchor=(0.50, 0.825),
                   ncol=2, columnspacing=0.65)
    fig.tight_layout(rect=[0, 0, 1, 1], w_pad=2.0, h_pad=2.0)
    file_name = 'Matrix_l1_norm_paper.pdf'
    fig.savefig(file_path_supplementary+file_name)
    # %% Distribution across time
    for iReco, reco_type in enumerate(reco_type_lst): #reco_type_lst
        fig, ax = plt.subplots(3,3, squeeze=False, figsize=(16,16))
        for iProj, num_proj_used in enumerate(num_proj_used_lst):
            for iNoise, noise in enumerate(noise_lst):
                # iData = iReco + iNoise*len(reco_type_lst) + iProj*len(noise_lst)*len(reco_type_lst)
                min_idx = (bin_values_time[:, iNoise, iReco]!=0).argmax(axis=2).min()
                max_idx = num_bins-1-(np.fliplr(bin_values_time[:, iNoise, iReco])!=0).argmax(axis=2).min()
                min_edge = bin_centers[min_idx]
                max_edge = bin_centers[max_idx]
                # print(bin_centers[min_idx],bin_centers[max_idx])
                ax[iNoise,iProj].imshow(np.log(bin_values_time[iProj, iNoise, iReco, :, min_idx:max_idx]+1),
                                        extent=[min_edge, max_edge, 0, 100],
                                        cmap='inferno', vmin=0)
                ax[iNoise,iProj].set_aspect('auto')
                # ax[iNoise,iProj].set_xlabel('$x - \\bar{x}$')
                # ax[iNoise,iProj].set_ylabel('Time step in simulation')
                ax[iNoise,iProj].set_title('$\\rho$ = {}%. {} = {}.'.format(noise, '$N_{\\mathrm{proj}}$', num_proj_used))
                ax[iNoise,iProj].set_yticks([])
                # ax[iNoise,iProj].set_yticks([], minor=True)
        for iProj, num_proj_used in enumerate(num_proj_used_lst):
            ax[-1,iProj].set_xlabel('$x - \\bar{x}$')
        for iNoise, noise in enumerate(noise_lst):
            ax[iNoise,0].set_yticks([0,25,50,75,100])
            ax[iNoise,0].set_yticklabels(['0','25','50','75','100'])
            ax[iNoise,0].set_ylabel('Time step in simulation')
        # fig.suptitle('{}'.format(reco_names[reco_type]))
        fig.tight_layout(rect=[0, 0, 1, 1], w_pad=2.0, h_pad=2.0)
        file_name = 'time_dependent_residual_RecoType{}.pdf'.format(reco_type)
        fig.savefig(file_path_supplementary+file_name)
        plt.close()
    # %% Distributions
    proxy_legend = []
    proxy_legend.append(matplotlib.patches.Patch(color=RGB[-1], label=reco_names[5]))
    yticks_pos = [1e0, 1e2, 1e3, 1e4, 1e5,
                  1e6, 1e7, 1e8, 1e9]
    yticks_labels = ['$1$', '$1^{2}$', '$1^{3}$', '$1^{4}$', '$1^{5}$',
                     '$1^{6}$', '$1^{7}$', '$1^{8}$', '$1^{9}$']
    xticks_pos = [-2, -1, 0, 1, 2]
    xticks_labels = [str(i) for i in xticks_pos]
    fig, ax = plt.subplots(3,3, squeeze=False, figsize=(15,15))
    for iProj, num_proj_used in enumerate(num_proj_used_lst):
        for iNoise, noise in enumerate(noise_lst):
            ax[iNoise,iProj].fill_between(bin_centers, 0, bin_values_FBP[0],
                                step='mid', linewidth=1.0,
                                facecolor=transparent, edgecolor=RGB[-1])
            for iReco, reco_type in enumerate(reco_type_lst):
                if iProj == 0 and iNoise == 0:
                    label_name = reco_names[reco_type]
                    proxy_legend.append(matplotlib.patches.Patch(color=RGB[iReco], label=label_name))
                iData = iReco + iNoise*len(reco_type_lst) + iProj*len(noise_lst)*len(reco_type_lst)
                # print(iData)
                label_name = 'reco_type{}'.format(reco_type)
                # ax[iNoise,iProj].plot(res_l1_norm[iProj, iNoise, iReco], linestyle=linestyle_tuple[iReco][1], label=label_name)
                ax[iNoise,iProj].fill_between(bin_centers, 0, bin_values[iProj, iNoise, iReco],
                                        step='mid', linewidth=1.0, label=label_name,
                                        facecolor=transparent, edgecolor=RGB[iReco],
                                        linestyle=linestyle_tuple[iReco][1])
    for iProj, num_proj_used in enumerate(num_proj_used_lst):
        for iNoise, noise in enumerate(noise_lst):
            min_edge = (bin_values[iProj, iNoise, :]!=0).argmax(axis=1).min()
            max_edge = num_bins-1-(np.fliplr(bin_values[iProj, iNoise, :])!=0).argmax(axis=1).min()
            # ax[iNoise,iProj].set_xlim(bin_centers[min_edge], bin_centers[max_edge])
            ax[iNoise,iProj].set_xlim(-2.5, 2.5)
            ax[iNoise,iProj].set_xticks(xticks_pos)
            ax[iNoise,iProj].set_xticklabels(xticks_labels, minor=False)
            
            ax[iNoise,iProj].set_yscale('log')
            ax[iNoise,iProj].set_yticks(yticks_pos)
            ax[iNoise,iProj].set_yticklabels(yticks_labels, minor=False)
            ax[iNoise,iProj].grid(True, which="both", ls="-")
            ax[iNoise,iProj].set_yticks([], minor=True)
            
            ax[iNoise,iProj].yaxis.set_major_formatter(NullFormatter())
            ax[iNoise,iProj].xaxis.set_major_formatter(NullFormatter())
            ax[iNoise,iProj].set_title('$\\rho$ = {}%. {} = {}.'.format(noise, '$N_{\\mathrm{proj}}$', num_proj_used))
    for iProj, num_proj_used in enumerate(num_proj_used_lst):
        ax[-1,iProj].set_xlabel('$x - \\bar{x}$')
        ax[-1,iProj].set_xticks(xticks_pos)
        ax[-1,iProj].set_xticklabels(xticks_labels, minor=False)
    for iNoise, noise in enumerate(noise_lst):
        ax[iNoise,0].set_ylabel('Counts')
        ax[iNoise,0].set_yticks(yticks_pos)
        ax[iNoise,0].set_yticklabels(yticks_labels, minor=False, ha='left')
        ax[iNoise,0].set_yticks([], minor=True)
        
        yax = ax[iNoise,0].get_yaxis()
        pad = 35
        yax.set_tick_params(pad=pad)
    ax[0,0].legend(loc='upper left', handles=proxy_legend, fontsize=13)
    fig.tight_layout(rect=[0, 0, 1, 1], w_pad=2.0, h_pad=2.0)
    file_name = 'matrix_residual.pdf'
    fig.savefig(file_path_supplementary+file_name)
    # %% Distributions
    proxy_legend = [matplotlib.patches.Patch(color='k', label='Simulation')]
    proxy_legend.append(matplotlib.patches.Patch(color=RGB[-1], label=reco_names[5]))
    fig, ax = plt.subplots(3,3, squeeze=False, figsize=(15,15))
    for iProj, num_proj_used in enumerate(num_proj_used_lst):
        for iNoise, noise in enumerate(noise_lst):
            ax[iNoise,iProj].fill_between(dist_centers, 0, dist_values_FBP[0],
                                          step='mid', linewidth=1.0,
                                          facecolor=transparent, edgecolor=RGB[-1])
            for iReco, reco_type in enumerate(reco_type_lst):
                if iProj == 0 and iNoise == 0:
                    label_name = reco_names[reco_type]
                    proxy_legend.append(matplotlib.patches.Patch(color=RGB[iReco], label=label_name))
                iData = iReco + iNoise*len(reco_type_lst) + iProj*len(noise_lst)*len(reco_type_lst)
                # print(iData)
                label_name = reco_names[reco_type]
                # ax[iNoise,iProj].plot(res_l1_norm[iProj, iNoise, iReco], linestyle=linestyle_tuple[iReco][1], label=label_name)
                ax[iNoise,iProj].fill_between(dist_centers, 0, dist_values[iProj, iNoise, iReco],
                                        step='mid', linewidth=1.5, label=label_name,
                                        facecolor=transparent, edgecolor=RGB[iReco],
                                        linestyle=linestyle_tuple[iReco][1])
            ax[iNoise,iProj].fill_between(dist_centers, 0, ground_truth_distribution,
                                        step='mid', linewidth=2.0, label=label_name,
                                        facecolor=transparent, edgecolor='k',
                                        linestyle='-')
    for iProj, num_proj_used in enumerate(num_proj_used_lst):
        for iNoise, noise in enumerate(noise_lst):
            min_edge = (dist_values[iProj, iNoise, :]!=0).argmax(axis=1).min()
            max_edge = num_bins_dist-1-(np.fliplr(dist_values[iProj, iNoise, :])!=0).argmax(axis=1).min()
            
            # ax[iNoise,iProj].set_xlim(dist_centers[min_edge],
            #     dist_centers[max_edge])
            ax[iNoise,iProj].set_xlim(-0.7, 3)
            ax[iNoise,iProj].set_ylim(1, 1e10)
            ax[iNoise,iProj].set_yscale('log')
            ax[iNoise,iProj].grid(True, which="both", ls="-")
            
            ax[iNoise,iProj].set_title('$\\rho$ = {}%. {} = {}.'.format(noise, '$N_{\\mathrm{proj}}$', num_proj_used))
            ax[iNoise,iProj].set_xlabel('Pixel values')
    # for iProj, num_proj_used in enumerate(num_proj_used_lst):
        # ax[iNoise,iProj].yaxis.set_major_formatter(NullFormatter())
    # for iNoise, noise in enumerate(noise_lst):
    ax[0,0].legend(loc='upper left', handles=proxy_legend, fontsize=13)
    fig.tight_layout(rect=[0, 0, 1, 1], w_pad=2.0, h_pad=2.0)
    file_name = 'matrix_distribution.pdf'
    fig.savefig(file_path_supplementary+file_name)
    # %%
    ncp_lst = [reco_type if reco_type != 0 else None for reco_type in reco_type_lst]
    if num_ncp > np.sum(np.array(reco_type_lst) == 0):
        ncp_lst.remove(None)
    fig, ax = plt.subplots(3,3, squeeze=False, figsize=(15,15))
    for iProj, num_proj_used in enumerate(num_proj_used_lst):
        for iNoise, noise in enumerate(noise_lst):
            for iReco, reco_type in enumerate(ncp_lst):
                label_name = reco_names[reco_type]
                ax[iNoise,iProj].plot(iterations_used[iProj,iNoise,iReco], label=label_name, color=RGB[iReco+1])
                ax[iNoise,iProj].set_xticks([0,25,50,75,100])
                ax[iNoise,iProj].set_xticklabels(['0','25','50','75','100'])
                ax[iNoise,iProj].grid(True, which="both", ls="-")
                ax[iNoise,iProj].xaxis.set_major_formatter(NullFormatter())
                ax[iNoise,iProj].set_title('$\\rho$ = {}%. {} = {}.'.format(noise, '$N_{\\mathrm{proj}}$', num_proj_used))
    ax[0,0].legend(loc='upper right', bbox_to_anchor=(0.5, 0.4, 0.5, 0.475))
    for iNoise, noise in enumerate(noise_lst):
        ax[iNoise,0].set_ylabel('$N_k$')
    for iProj, num_proj_used in enumerate(num_proj_used_lst):
        ax[-1,iProj].set_xticks([0,25,50,75,100])
        ax[-1,iProj].set_xticklabels(['0','25','50','75','100'])
        ax[-1,iProj].set_xlabel('Time step in simulation')
    fig.tight_layout(rect=[0, 0, 1, 1], w_pad=2.0, h_pad=2.0)
    
    file_name = 'ncp_iterations_needed_matrix.pdf'
    # fig.savefig(file_path_supplementary+file_name)
    # %%
    if args.load_reco:
        # %%
        iFrame = 50
        iSlice = 170
        # diff_tuple = [(0,0),(0,0),(0,1),(0,0),(0,0),(0,2),(2,0),(2,1),(1,1)]
        diff_tuple = [(0,0),(0,0),(1,0),(0,0),(0,0),(2,0),(0,1),(0,2),(1,1)]
        for iReco, reco_type in enumerate(reco_type_lst):
            fig, ax = plt.subplots(3,3, squeeze=False, figsize=(15,15))
            for iProj, num_proj_used in enumerate(num_proj_used_lst):
                for iNoise, noise in enumerate(noise_lst):
                    # print('({iProj},{iNoise})')
                    iData = iNoise +  iProj*len(noise_lst)
                    tmp = recos[iProj,iNoise,iReco,iFrame,iSlice]-recos[diff_tuple[iData]][iReco,iFrame,iSlice]
                    tmp_sd = np.std(tmp)
                    ax[iProj,iNoise].imshow(tmp, cmap='coolwarm', vmin=-3*tmp_sd, vmax=3*tmp_sd)
                    ax[iProj,iNoise].set_title('$\\rho$ = {}%. {} = {}.'.format(noise, '$N_{\\mathrm{proj}}$', num_proj_used))
                    ax[iProj,iNoise].set_xticks([])
                    ax[iProj,iNoise].set_yticks([])
            fig.tight_layout(rect=[0, 0, 0.96, 1])
        # %%
        iFrame = 50
        iSlice = 170
        for iReco, reco_type in enumerate(reco_type_lst):
            fig, ax = plt.subplots(3,3, squeeze=False, figsize=(16,16))
            for iProj, num_proj_used in enumerate(num_proj_used_lst):
                for iNoise, noise in enumerate(noise_lst):
                    # print('({iProj},{iNoise})')
                    iData = iNoise +  iProj*len(noise_lst)
                    tmp = recos[iProj,iNoise,iReco,iFrame,iSlice]-ground_truth[iFrame,iSlice]
                    tmp_sd = np.std(tmp)
                    im = ax[iProj,iNoise].imshow(tmp, cmap='coolwarm',
                                                 vmin=-3*tmp_sd, vmax=3*tmp_sd)
                    ax[iProj,iNoise].set_title('$\\rho$ = {}%. {} = {}.'.format(noise, '$N_{\\mathrm{proj}}$', num_proj_used))
                    ax[iProj,iNoise].set_xticks([])
                    ax[iProj,iNoise].set_yticks([])
                    cb_ax = inset_axes(ax[iProj,iNoise], width="5%",
                                      height="100%", loc='right',
                                      bbox_to_anchor=(0.5, 0.0, 0.575, 1.0),
                                      bbox_transform=ax[iProj,iNoise].transAxes)
                    fig.colorbar(im, cax=cb_ax)
            file_name = 'reco_error_examples_RecoType{}.pdf'.format(reco_type)
            fig.tight_layout(rect=[0, 0, 0.96, 1], w_pad=4.0, h_pad=1.0)
            fig.savefig(file_path_supplementary+file_name)
            plt.close()
        # %%
        iFrame = 50
        iSlice = 170
        for iReco, reco_type in enumerate(reco_type_lst):
            fig, ax = plt.subplots(3,3, squeeze=False, figsize=(16,16))
            for iProj, num_proj_used in enumerate(num_proj_used_lst):
                for iNoise, noise in enumerate(noise_lst):
                    # print('({iProj},{iNoise})')
                    iData = iNoise +  iProj*len(noise_lst)
                    tmp = recos[iProj,iNoise,iReco,iFrame,iSlice]
                    tmp_sd = np.std(tmp)
                    im = ax[iProj,iNoise].imshow(tmp, cmap='gray',
                                                 vmin=0, vmax=2.5)
                    ax[iProj,iNoise].set_title('$\\rho$ = {}%. {} = {}.'.format(noise, '$N_{\\mathrm{proj}}$', num_proj_used))
                    ax[iProj,iNoise].set_xticks([])
                    ax[iProj,iNoise].set_yticks([])
                    # cb_ax = inset_axes(ax[iProj,iNoise], width="5%",
                    #                   height="100%", loc='right',
                    #                   bbox_to_anchor=(0.5, 0.0, 0.575, 1.0),
                    #                   bbox_transform=ax[iProj,iNoise].transAxes)
                    # fig.colorbar(im, cax=cb_ax)
            file_name = 'reco_examples_RecoType{}.pdf'.format(reco_type)
            fig.tight_layout(rect=[0, 0, 1, 1], w_pad=1.0, h_pad=1.0)
            fig.savefig(file_path_supplementary+file_name)
            plt.close()
# %%
if __name__ == '__main__':
    # Construct the argument parser
    parser = argparse.ArgumentParser()
    
    # Add the arguments to the parser
    parser.add_argument("-num_proj_used", "--num_proj_used", required=False,
                        nargs='?', default=[720, 240, 90],
                        type=lambda s: [item for item in s.split(',')], help="first operand")
    
    parser.add_argument("-noise", "--noise", required=False,
                        nargs='?', default=['5.00'],
                        type=lambda s: [item for item in s.split(',')], help="second operand")
    
    parser.add_argument("-reco_type", "--reco_type", required=False,
                        nargs='?', default=[4],
                        type=lambda s: [item for item in s.split(',')], help="third operand")
    
    parser.add_argument("-num_itr", "--num_itr", required=False,
                        nargs='?', default=['NCP'],
                        type=lambda s: [item for item in s.split(',')], help="third operand")
    
    parser.add_argument("-load_reco", "--load_reco", required=False,
                        default=True, type=bool, help="third operand")
    args = parser.parse_args()
    # %%
    main(args)
