#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 16 10:20:22 2020

@author: Peter Winkel Rasmussen
"""
import os
os.chdir('/home/pwra/simulation/')
import numpy as np
import argparse
import auxiliary_functions as af
import constants as const
# from skimage.draw import circle
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.style as style
import matplotlib.patches as mpatches
from matplotlib.widgets import Slider
style.use('seaborn')
style.use('ggplot')
from cycler import cycler
from matplotlib.colors import hsv_to_rgb, to_rgba, LogNorm
# import matplotlib.animation as animation
from mpl_toolkits.axes_grid1 import make_axes_locatable
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
params = {'axes.grid': False, 'savefig.dpi': 200,
          'text.color': 'black',
          'axes.labelcolor': 'black',
          'xtick.color': 'black',
          'ytick.color': 'black',
          'lines.linewidth': 4,
          'patch.linewidth': 3,
          'lines.markersize': 10}
plt.rcParams.update(params)

SMALL_SIZE = 13+6
MEDIUM_SIZE = 15+6
BIGGER_SIZE = 17+6

plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=MEDIUM_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title

cfig_size = (7.5, 18)

file_path_plots = '/home/pwra/simulation/paper_plots/'
file_path_supplementary = '/home/pwra/simulation/supplementary_plots/'
reco_names = {0 : 'FBP',
             1 : 'SIRT',
             2 : 'SIRT-BC',
             3 : 'SIRT-IC',
             4 : 'SIRT-LC',
             5 : 'FBP ideal'}
# RGB = np.array([[228,26,28],
#                 [55,126,184],
#                 [77,175,74],
#                 [152,78,163],
#                 [255,127,0]])/255
RGB = np.array([[228,26,28],
                [55,126,184],
                [77,175,74],
                [152,78,163],
                [255,127,0],
                [166,206,227]])/255
default_cycler = cycler(color=RGB)
plt.rc('axes', prop_cycle=default_cycler)
linestyle_tuple = [
    ('solid', (0, ())),
    ('dotted',                (0, (1, 1))),
    ('dashed',                (0, (5, 5))),
    ('dashdotted',            (0, (3, 5, 1, 5))),
    ('densely dashdotdotted', (0, (3, 1, 1, 1, 1, 1)))]
# plt.rc('axes', prop_cycle=default_cycler)

transparent = to_rgba(np.array([0,0,0]), 0)

left, width = 0.25, 0.5
bottom, height = 0.25, 0.5
right = left + width
top = bottom + height
# %%
def main(args):
    # %%
    num_proj_used_lst = args.num_proj_used
    noise_lst = args.noise
    num_data = min(len(num_proj_used_lst), len(noise_lst))
    idx_lst = [i for i in range(num_data)]
    reco_type_lst = args.reco_type
    
    num_pixels = const.num_rows
    num_frames = const.num_frames
    bin_width = const.bin_width
    
    min_edge = -10
    max_edge = 10
    num_bins = (max_edge-min_edge)/bin_width
    if num_bins.is_integer():
        num_bins = int(num_bins)
    else:
        raise ValueError('bin_width or bin_range results in non-integer'+
                         'number of bins')
    bin_edges = np.linspace(min_edge, max_edge, num_bins+1, endpoint=True, dtype=np.float32)
    bin_centers = (bin_edges[:-1]+bin_edges[1:])/2
    
    min_val = -10
    max_val = 15
    num_bins_dist = (max_val-min_val)/bin_width
    if num_bins_dist.is_integer():
        num_bins_dist = int(num_bins_dist)
    else:
        raise ValueError('bin_width or bin_range results in non-integer'+
                         'number of bins')
    dist_edges = np.linspace(min_val, max_val, num_bins_dist+1, endpoint=True, dtype=np.float32)
    dist_centers = (dist_edges[:-1]+dist_edges[1:])/2
    # %%
    recos = np.zeros((num_data, len(reco_type_lst),
                      num_frames, num_pixels, num_pixels,
                      num_pixels), dtype=np.float32)
    res_l2_norm = np.zeros((num_data, len(reco_type_lst),
                            num_frames), dtype=np.float32)
    res_l1_norm = np.zeros((num_data, len(reco_type_lst),
                            num_frames), dtype=np.float32)
    bin_values = np.zeros((num_data, len(reco_type_lst),
                            num_bins), dtype=np.float32)
    dist_values = np.zeros((num_data, len(reco_type_lst),
                            num_bins_dist), dtype=np.float32)
    bin_values_time = np.zeros((num_data, len(reco_type_lst),
                            num_frames, num_bins), dtype=np.float32)
    dist_values_time = np.zeros((num_data, len(reco_type_lst),
                            num_frames, num_bins_dist), dtype=np.float32)
    
    num_ncp = np.sum(np.array(reco_type_lst) != 0)
    if num_ncp > 0:
        iterations_used = np.zeros((num_data, num_ncp,
                            num_frames), dtype=np.float32)
    file_labels = []
    
    for iData, num_proj_used, noise in zip(idx_lst, num_proj_used_lst, noise_lst):
        print('Loading analysis of dataset {} out of {}'.format(iData, num_data-1))
        ncp_counter = 0
        for iReco, reco_type in enumerate(reco_type_lst):
            if iReco == 0:
                num_itr = 'None'
            else:
                num_itr = 'NCP'
            file_path = '/home/pwra/simulation/output_files/noise{}/num_proj_used{}/reco_type{}/'.format(noise, num_proj_used, reco_type)
            file_name = 'dynamic_reconstruction_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}.npy'.format(noise, num_proj_used, reco_type, num_itr)
            recos[iData, iReco] = np.load(file_path+file_name)
                
            file_path = '/home/pwra/simulation/output_files/noise{}/num_proj_used{}/reco_type{}/'.format(noise, num_proj_used, reco_type)
            file_name = 'res_l2_norm_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}.npy'.format(noise, num_proj_used, reco_type, num_itr)
            res_l2_norm[iData, iReco] = np.load(file_path+file_name)
            
            file_name = 'res_l1_norm_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}.npy'.format(noise, num_proj_used, reco_type, num_itr)
            res_l1_norm[iData, iReco] = np.load(file_path+file_name)
            
            file_name = 'rec_binned_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}.npy'.format(noise, num_proj_used, reco_type, num_itr)
            dist_values[iData, iReco] = np.load(file_path+file_name)
            
            file_name = 'res_binned_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}.npy'.format(noise, num_proj_used, reco_type, num_itr)
            bin_values[iData, iReco] = np.load(file_path+file_name)
            
            file_name = 'res_binned_time_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}.npy'.format(noise, num_proj_used, reco_type, num_itr)
            bin_values_time[iData, iReco] = np.load(file_path+file_name)
            
            file_name = 'rec_binned_time_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}.npy'.format(noise, num_proj_used, reco_type, num_itr)
            dist_values_time[iData, iReco] = np.load(file_path+file_name)
            
            if reco_type != 0:
                file_path = '/home/pwra/simulation/output_files/noise{}/num_proj_used{}/reco_type{}/'.format(noise, num_proj_used, reco_type)
                file_name = 'dynamic_reconstruction_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}_iterations_used.npy'.format(noise, num_proj_used, reco_type, 'NCP')
                iterations_used[iData, ncp_counter] = np.load(file_path+file_name)
                ncp_counter += 1
            
            tmp_lst = [[num_proj_used], [noise], [reco_type]]
            file_labels.append(tmp_lst)
            
    noise, num_proj_used, reco_type, num_itr = '0.25', '720', '0', 'None'
    file_path = '/home/pwra/simulation/output_files/noise{}/num_proj_used{}/reco_type{}/'.format(noise, num_proj_used, reco_type)
    file_name = 'dynamic_reconstruction_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}.npy'.format(noise, num_proj_used, reco_type, num_itr)
    recos_FBP = np.load(file_path+file_name)
      
    file_path = '/home/pwra/simulation/output_files/noise{}/num_proj_used{}/reco_type{}/'.format(noise, num_proj_used, reco_type)
    file_name = 'res_l2_norm_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}.npy'.format(noise, num_proj_used, reco_type, num_itr)
    res_l2_norm_FBP = np.load(file_path+file_name)
    
    file_name = 'res_l1_norm_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}.npy'.format(noise, num_proj_used, reco_type, num_itr)
    res_l1_norm_FBP = np.load(file_path+file_name)
    
    file_name = 'rec_binned_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}.npy'.format(noise, num_proj_used, reco_type, num_itr)
    dist_values_FBP = np.load(file_path+file_name)
    
    file_name = 'res_binned_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}.npy'.format(noise, num_proj_used, reco_type, num_itr)
    bin_values_FBP = np.load(file_path+file_name)
    
    file_name = 'res_binned_time_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}.npy'.format(noise, num_proj_used, reco_type, num_itr)
    bin_values_time_FBP = np.load(file_path+file_name)
    
    file_labels = np.array(file_labels)
    
    ground_truth_distribution = np.load('output_files/ground_truth_distribution.npy')
    ground_truth_distribution_time = np.load('output_files/ground_truth_distribution_time.npy')
    ground_truth = np.load('output_files/simulation_attenuation_rotated.npy')
    # %% l2-norm
    yticks_pos = [100, 200, 400, 600, 800, 1000, 2000, 4000, 6000, 8000, 10000, 10000]
    yticks_labels = [str(i) for i in yticks_pos]
    tick_min = [0, 0, 0]
    tick_max = [-9, -6, -1]
    plot_names = ['a', 'b', 'c']
    
    fig, ax = plt.subplots(1,3, squeeze=False, figsize=(21,7))
    for iData, num_proj_used, noise in zip(idx_lst, num_proj_used_lst, noise_lst):
        ax[0,iData].plot(res_l2_norm_FBP[0], label=reco_names[5], color=RGB[-1])
        for iReco, reco_type in enumerate(reco_type_lst):
            # label_name =  'Reconstruction method {}'.format(iReco)
            label_name = reco_names[iReco]
            ax[0,iData].plot(res_l2_norm[iData, iReco], linestyle=linestyle_tuple[iReco][1], label=label_name)
    for iData, num_proj_used, noise in zip(idx_lst, num_proj_used_lst, noise_lst):
        ax[0,iData].set_xlim(0, 100)
        ax[0,iData].set_yscale('log')
        ax[0,iData].set_yticks(yticks_pos[tick_min[iData]:tick_max[iData]])
        ax[0,iData].set_yticklabels(yticks_labels[tick_min[iData]:tick_max[iData]], minor=False)
        ax[0,iData].set_yticks([], minor=True)
        # ax[0,iData].get_yaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
        ax[0,iData].grid(True, which="both", ls="-")
        ax[0,iData].set_xlabel('Time step in simulation')
        ax[0,iData].set_ylabel('$||x - \\bar{x}||_2$')
        ax[0,iData].set_title('$\\rho$ = {}%. {} = {}.'.format(noise, '$N_{\\mathrm{proj}}$', num_proj_used))
        ax[0,iData].text(0.975, 0.975, plot_names[iData],
                         horizontalalignment='right',
                         verticalalignment='top',
                         fontweight='bold',
                         fontsize=BIGGER_SIZE,
                         transform=ax[0,iData].transAxes)
    fig.tight_layout(rect=[0, 0, 1, 1], w_pad=1.0, h_pad=1.0)
    ax[0,0].legend(loc='center', bbox_to_anchor=(0.45, 0.875), ncol=2)
    file_name = 'TimeStepResidual_l2_norm_diagonal.pdf'
    fig.savefig(file_path_plots+file_name)
    # %% l1-norm
    yticks_pos = [1e5, 2e5, 4e5, 6e5, 8e5,
                  1e6, 2e6, 4e6, 6e6, 8e6,
                  1e7, 2e7, 2e7]
    tick_min = [0, 1, 1]
    tick_max = [-8, -5, -1]
    plot_names = ['a', 'b', 'c']
    
    fig, ax = plt.subplots(1,3, squeeze=False, figsize=(21, 7))
    for iData, num_proj_used, noise in zip(idx_lst, num_proj_used_lst, noise_lst):
        for iReco, reco_type in enumerate(reco_type_lst):
            # label_name =  'Reconstruction method {}'.format(iReco)
            label_name = reco_names[iReco]
            ax[0,iData].plot(res_l1_norm[iData, iReco], linestyle=linestyle_tuple[iReco][1], label=label_name)
    for iData, num_proj_used, noise in zip(idx_lst, num_proj_used_lst, noise_lst):
        # ax[0,iNoise].set_ylim(200, 7000)
        ax[0,iData].set_xlim(0, 100)
        ax[0,iData].set_yscale('log')
        ax[0,iData].set_yticks(yticks_pos[tick_min[iData]:tick_max[iData]], minor=False)
        ax[0,iData].set_yticks([], minor=True)
        ax[0,iData].get_yaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
        ax[0,iData].ticklabel_format(axis='y',style='sci', scilimits=(0,0))
        ax[0,iData].set_xlabel('Time step in simulation')
        ax[0,iData].set_ylabel('$||x - \\bar{x}||_1$')
        ax[0,iData].set_title('$\\rho$ = {}%. {} = {}.'.format(noise, '$N_{\\mathrm{proj}}$', num_proj_used))
        ax[0,iData].grid(True, which="both", ls="-")
        ax[0,iData].text(0.025, 0.9, plot_names[iData],
                         horizontalalignment='left',
                         verticalalignment='top',
                         fontweight='bold',
                         fontsize=BIGGER_SIZE,
                         transform=ax[0,iData].transAxes)
    fig.tight_layout(rect=[0, 0, 1, 1], w_pad=1.0, h_pad=1.0)
    ax[0,0].legend(loc='center', bbox_to_anchor=(0.175, 0.175))
    file_name = 'TimeStepResidual_l1_norm_diagonal.pdf'
    fig.savefig(file_path_plots+file_name)
    # %% l1 and l2 3x2
    yticks_pos = [60, 80, 100, 200, 400, 600, 800, 1000, 2000, 4000, 6000, 8000, 8000]
    yticks_labels = [str(i) for i in yticks_pos]
    tick_min = [0, 2, 2]
    tick_max = [-6, -5, -1]
    
    fig, ax = plt.subplots(num_data,2, squeeze=False, figsize=(12, 19))
    for iData, num_proj_used, noise in zip(idx_lst, num_proj_used_lst, noise_lst):
        for iReco, reco_type in enumerate(reco_type_lst):
            # label_name =  'Reconstruction method {}'.format(iReco)
            label_name = reco_names[iReco]
            ax[iData,0].plot(res_l2_norm[iData, iReco], linestyle=linestyle_tuple[iReco][1], label=label_name)
        label_name = reco_names[5]
        ax[iData,0].plot(res_l2_norm_FBP[0],
                         label=label_name, color='k')
    
    for iData, num_proj_used, noise in zip(idx_lst, num_proj_used_lst, noise_lst):
        ax[iData,0].set_xlim(0, 100)
        ax[iData,0].set_yscale('log')
        ax[iData,0].set_yticks(yticks_pos[tick_min[iData]:tick_max[iData]])
        ax[iData,0].set_yticklabels(yticks_labels[tick_min[iData]:tick_max[iData]], minor=False)
        ax[iData,0].set_yticks([], minor=True)
        # ax[iData,0].get_yaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
        ax[iData,0].grid(True, which="both", ls="-")
        ax[iData,0].set_xlabel('Time step in simulation')
        ax[iData,0].set_ylabel('$||x - \\bar{x}||_2$')
        ax[iData,0].set_title('$\\rho$ = {}%. {} = {}.'.format(noise, '$N_{\\mathrm{proj}}$', num_proj_used), pad=5)
        
    yticks_pos = [4e4, 6e4, 8e4,
                  1e5, 2e5, 4e5, 6e5, 8e5,
                  1e6, 2e6, 4e6, 6e6, 8e6,
                  1e7, 2e7, 2e7]
    tick_min = [3, 4, 4]
    tick_max = [-8, -5, -1]
    
    for iData, num_proj_used, noise in zip(idx_lst, num_proj_used_lst, noise_lst):
        for iReco, reco_type in enumerate(reco_type_lst):
            # label_name =  'Reconstruction method {}'.format(iReco)
            label_name = reco_names[iReco]
            ax[iData,1].plot(res_l1_norm[iData, iReco], linestyle=linestyle_tuple[iReco][1], label=label_name)
        label_name = reco_names[5]
        ax[iData,1].plot(res_l1_norm_FBP[0],
                         label=label_name, color='k')
    for iData, num_proj_used, noise in zip(idx_lst, num_proj_used_lst, noise_lst):
        # ax[0,iNoise].set_ylim(200, 7000)
        ax[iData,1].set_xlim(0, 100)
        ax[iData,1].set_yscale('log')
        ax[iData,1].set_yticks(yticks_pos[tick_min[iData]:tick_max[iData]], minor=False)
        ax[iData,1].set_yticks([], minor=True)
        ax[iData,1].get_yaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
        ax[iData,1].ticklabel_format(axis='y',style='sci', scilimits=(0,0))
        ax[iData,1].set_xlabel('Time step in simulation')
        ax[iData,1].set_ylabel('$||x - \\bar{x}||_1$')
        ax[iData,1].set_title('$\\rho$ = {}%. {} = {}.'.format(noise, '$N_{\\mathrm{proj}}$', num_proj_used), pad=5)
        ax[iData,1].grid(True, which="both", ls="-")
    fig.tight_layout(rect=[0, 0, 1, 1], w_pad=1, h_pad=2.0)
    ax[0,0].legend(bbox_to_anchor=(0.5, 1), loc='upper center', borderaxespad=0., ncol=2)
    file_name = 'TimeStepResidual_l2_l1_norm_diagonal.pdf'
    fig.savefig(file_path_plots+file_name)
    # %% l1 and l2 2x3
    yticks_pos = [100, 200, 400, 600, 800, 1000, 2000, 4000, 6000, 8000, 8000]
    yticks_labels = [str(i) for i in yticks_pos]
    tick_min = [0, 0, 0]
    tick_max = [-8, -5, -1]
    
    fig, ax = plt.subplots(2,3, squeeze=False, figsize=(20, 12))
    for iData, num_proj_used, noise in zip(idx_lst, num_proj_used_lst, noise_lst):
        ax[0,iData].plot(res_l2_norm_FBP[0], label=label_name, color=RGB[-1])
        for iReco, reco_type in enumerate(reco_type_lst):
            label_name = reco_names[iReco]
            ax[0,iData].plot(res_l2_norm[iData, iReco], linestyle=linestyle_tuple[iReco][1], label=label_name)
        label_name = reco_names[5]
    
    for iData, num_proj_used, noise in zip(idx_lst, num_proj_used_lst, noise_lst):
        ax[0,iData].set_xlim(0, 100)
        ax[0,iData].set_yscale('log')
        ax[0,iData].set_yticks(yticks_pos[tick_min[iData]:tick_max[iData]])
        ax[0,iData].set_yticklabels(yticks_labels[tick_min[iData]:tick_max[iData]], minor=False)
        ax[0,iData].set_yticks([], minor=True)
        # ax[0,iData].get_yaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
        ax[0,iData].grid(True, which="both", ls="-")
        ax[0,iData].set_xlabel('Time step in simulation')
        ax[0,iData].set_ylabel('$||x - \\bar{x}||_2$')
        ax[0,iData].set_title('$\\rho$ = {}%. {} = {}.'.format(noise, '$N_{\\mathrm{proj}}$', num_proj_used), pad=10)
        
    yticks_pos = [4e4, 6e4, 8e4,
                  1e5, 2e5, 4e5, 6e5, 8e5,
                  1e6, 2e6, 4e6, 6e6, 8e6,
                  1e7, 2e7, 2e7]
    tick_min = [4, 4, 4]
    tick_max = [-9, -5, -1]
    
    for iData, num_proj_used, noise in zip(idx_lst, num_proj_used_lst, noise_lst):
        ax[1,iData].plot(res_l1_norm_FBP[0], label=label_name, color=RGB[-1])
        for iReco, reco_type in enumerate(reco_type_lst):
            label_name = reco_names[iReco]
            ax[1,iData].plot(res_l1_norm[iData, iReco], linestyle=linestyle_tuple[iReco][1], label=label_name)
        label_name = reco_names[5]
    for iData, num_proj_used, noise in zip(idx_lst, num_proj_used_lst, noise_lst):
        # ax[0,iNoise].set_ylim(200, 7000)
        ax[1,iData].set_xlim(0, 100)
        ax[1,iData].set_yscale('log')
        ax[1,iData].set_yticks(yticks_pos[tick_min[iData]:tick_max[iData]], minor=False)
        ax[1,iData].set_yticks([], minor=True)
        ax[1,iData].get_yaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
        ax[1,iData].ticklabel_format(axis='y',style='sci', scilimits=(0,0))
        ax[1,iData].set_xlabel('Time step in simulation')
        ax[1,iData].set_ylabel('$||x - \\bar{x}||_1$')
        # ax[1,iData].set_title('$\\rho$ = {}%. {} = {}.'.format(noise, '$N_{\\mathrm{proj}}$', num_proj_used), pad=21)
        ax[1,iData].grid(True, which="both", ls="-")
    fig.tight_layout(rect=[0, 0, 1, 1], w_pad=1.0, h_pad=1.0)
    ax[0,0].legend(bbox_to_anchor=(0.5, 1.0), loc='upper center', borderaxespad=0., ncol=2)
    file_name = 'TimeStepResidual_l2_l1_norm_diagonal_row.pdf'
    fig.savefig(file_path_plots+file_name)
    # %% Distribution across time
    fig, ax = plt.subplots(num_data,5, squeeze=False, figsize=(25,15))
    for iReco, reco_type in enumerate(reco_type_lst):
        for iData, num_proj_used, noise in zip(idx_lst, num_proj_used_lst, noise_lst):
            min_idx = (bin_values_time[iData, iReco]!=0).argmax(axis=1).min()
            max_idx = num_bins-1-(np.fliplr(bin_values_time[iData, iReco])!=0).argmax(axis=1).min()
            min_edge = bin_centers[min_idx]
            max_edge = bin_centers[max_idx]
            ax[iData,iReco].imshow(np.log(bin_values_time[iData, iReco, :, min_idx:max_idx]+1),
                                    extent=[min_edge, max_edge, 0, 100],
                                    cmap='inferno', vmin=1)
            ax[iData,iReco].set_aspect('auto')
            ax[iData,iReco].set_xlabel('$x - \\bar{x}$')
            ax[iData,iReco].set_ylabel('Time step in simulation')
            ax[iData,iReco].set_title('$\\rho$ = {}%. {} = {}.'.format(noise, '$N_{\\mathrm{proj}}$', num_proj_used))
    fig.tight_layout(rect=[0, 0, 1, 0.95], w_pad=2.0, h_pad=2.0)
    file_name = 'TimeStepResidual_distribution_diagonal.pdf'
    fig.savefig(file_path_plots+file_name)
    # %% Distribution across time
    fig, ax = plt.subplots(num_data,5, squeeze=False, figsize=(25,15))
    for iReco, reco_type in enumerate(reco_type_lst):
        for iData, num_proj_used, noise in zip(idx_lst, num_proj_used_lst, noise_lst):
            min_idx = (dist_values_time[iData, iReco]!=0).argmax(axis=1).min()
            max_idx = num_bins_dist-1-(np.fliplr(dist_values_time[iData, iReco])!=0).argmax(axis=1).min()
            min_edge = dist_centers[min_idx]
            max_edge = dist_centers[max_idx+1]
            ax[iData,iReco].imshow(np.log(dist_values_time[iData, iReco, :, min_idx:max_idx]+1),
                                    extent=[min_edge, max_edge, 0, 100],
                                    cmap='inferno', vmin=1)
            ax[iData,iReco].set_aspect('auto')
            ax[iData,iReco].set_xlabel('Pixel intensity')
            ax[iData,iReco].set_ylabel('Time step in simulation')
            ax[iData,iReco].set_title('$\\rho$ = {}%. {} = {}.'.format(noise, '$N_{\\mathrm{proj}}$', num_proj_used))
    fig.tight_layout(rect=[0, 0, 1, 0.95], w_pad=2.0, h_pad=2.0)
    file_name = 'TimeStep_distribution_diagonal.pdf'
    fig.savefig(file_path_plots+file_name)
    # %% Distribution across time ground truth
    fig, ax = plt.subplots(1,1, squeeze=False, figsize=(10,10))
    min_idx = (ground_truth_distribution_time[0]!=0).argmax(axis=1).min()
    max_idx = num_bins_dist-1-(np.fliplr(ground_truth_distribution_time[0])!=0).argmax(axis=1).min()
    min_edge = dist_centers[min_idx]
    max_edge = dist_centers[max_idx+1]
    ax[0,0].imshow(np.log(ground_truth_distribution_time[0, :, min_idx:max_idx]+1),
                            extent=[min_edge, max_edge, 0, 100],
                            cmap='inferno', vmin=1)
    ax[0,0].set_aspect('auto')
    ax[0,0].set_xlabel('Pixel intensity')
    ax[0,0].set_ylabel('Time step in simulation')
    ax[0,0].set_title('Simulation')
    fig.tight_layout(rect=[0, 0, 1, 0.95], w_pad=2.0, h_pad=2.0)
    # %% Distributions
    fig, ax = plt.subplots(1,3, squeeze=False, figsize=(24,7.5))
    proxy_legend = []
    for iData, num_proj_used, noise in zip(idx_lst, num_proj_used_lst, noise_lst):
        for iReco, reco_type in enumerate(reco_type_lst):
            if iData == 0:
                label_name = reco_names[reco_type]
                proxy_legend.append(mpatches.Patch(color=RGB[iReco], label=label_name))
            ax[0,iData].fill_between(bin_centers, 0, bin_values[iData, iReco],
                                    step='mid',
                                    facecolor=transparent, edgecolor=RGB[iReco])
        label_name = reco_names[5]
        if iData == 0:
            proxy_legend.append(mpatches.Patch(color=RGB[-1], label=label_name))
        ax[0,iData].fill_between(bin_centers, 0, bin_values_FBP[0],
                                step='mid',
                                facecolor=transparent, edgecolor=RGB[-1])
    for iData, num_proj_used, noise in zip(idx_lst, num_proj_used_lst, noise_lst):
        min_idx = (bin_values[iData, :]!=0).argmax(axis=1).min()
        max_idx = num_bins-1-(np.fliplr(bin_values[iData, :])!=0).argmax(axis=1).min()
        min_edge = bin_edges[min_idx]
        max_edge = bin_edges[max_idx+1]
        
        # ax[0,iData].set_xlim(min_edge*1.1, max_edge*1.1)
        ax[0,iData].set_xlim(-2.5*1.1, 2.5*1.1)
        ax[0,iData].set_yscale('log')
        ax[0,iData].set_ylim(1, 1e10)
        ax[0,iData].grid(True, which="both", ls="-")
        
        ax[0,iData].set_title('$\\rho$ = {}%. {} = {}.'.format(noise, '$N_{\\mathrm{proj}}$', num_proj_used))
        ax[0,iData].set_xlabel('$x - \\bar{x}$')
    fig.tight_layout(rect=[0, 0, 1, 1], w_pad=2.0, h_pad=2.0)
    ax[0,0].legend(loc='upper left', handles=proxy_legend)
    file_name = 'Residual_distribution_diagonal.pdf'
    fig.savefig(file_path_plots+file_name)
    # %% Distributions row
    plot_names = ['a', 'b', 'c']
    
    fig, ax = plt.subplots(1,3, squeeze=False, figsize=(21,7))
    proxy_legend = [mpatches.Patch(color='k', label='Simulation')]
    proxy_legend.append(mpatches.Patch(color=RGB[-1], label=reco_names[5]))
    for iData, num_proj_used, noise in zip(idx_lst, num_proj_used_lst, noise_lst):
        for iReco, reco_type in enumerate(reco_type_lst):
            if iData == 0:
                label_name = reco_names[reco_type]
                proxy_legend.append(mpatches.Patch(color=RGB[iReco], label=label_name))
            ax[0,iData].fill_between(dist_centers, 0, dist_values[iData, iReco],
                                    step='mid', label=label_name,
                                    facecolor=transparent, edgecolor=RGB[iReco])
        ax[0,iData].fill_between(dist_centers, 0, dist_values_FBP[0],
                                    step='mid',
                                    facecolor=transparent, edgecolor=RGB[-1])
        ax[0,iData].fill_between(dist_centers, 0, ground_truth_distribution,
                                    step='mid',
                                    facecolor=transparent, edgecolor='k')
    
    for iData, num_proj_used, noise in zip(idx_lst, num_proj_used_lst, noise_lst):
        min_idx = (dist_values[iData]!=0).argmax(axis=1).min()
        max_idx = num_bins_dist-1-(np.fliplr(dist_values[iData])!=0).argmax(axis=1).min()
        min_edge = dist_edges[min_idx]
        max_edge = dist_edges[max_idx+1]

        # ax[0,iData].set_xlim(min(0,min_edge*1.1), max_edge*1.1)
        ax[0,iData].set_xlim(-0.5, 3.0)
        ax[0,iData].set_yscale('log')
        ax[0,iData].set_ylim(1, 1e10)
        ax[0,iData].grid(True, which="both", ls="-")
        
        ax[0,iData].set_title('$\\rho$ = {}%. {} = {}.'.format(noise, '$N_{\\mathrm{proj}}$', num_proj_used))
        ax[0,iData].set_xlabel('Pixel value')
        ax[0,iData].text(0.975, 0.975, plot_names[iData],
                         horizontalalignment='right',
                         verticalalignment='top',
                         fontweight='bold',
                         fontsize=BIGGER_SIZE,
                         transform=ax[0,iData].transAxes)
    fig.tight_layout(rect=[0, 0, 1, 1], w_pad=2.0, h_pad=2.0)
    ax[0,0].legend(loc='upper left', handles=proxy_legend)
    file_name = 'distribution_diagonal.pdf'
    fig.savefig(file_path_plots+file_name)
    # %% Distributions column
    fig, ax = plt.subplots(num_data,1, squeeze=False, figsize=cfig_size)
    proxy_legend = [mpatches.Patch(color='k', label='Simulation')]
    # proxy_legend.append(mpatches.Patch(color=RGB[-1], label=reco_names[5]))
    for iData, num_proj_used, noise in zip(idx_lst, num_proj_used_lst, noise_lst):
        for iReco, reco_type in enumerate(reco_type_lst):
            if iData == 0:
                label_name = reco_names[reco_type]
                proxy_legend.append(mpatches.Patch(color=RGB[iReco], label=label_name))
            ax[iData,0].fill_between(dist_centers, 0, dist_values[iData, iReco],
                                    step='mid', label=label_name,
                                    facecolor=transparent, edgecolor=RGB[iReco])
        # ax[iData,0].fill_between(dist_centers, 0, dist_values_FBP[0],
        #                             step='mid',
        #                             facecolor=transparent, edgecolor=RGB[-1])
        ax[iData,0].fill_between(dist_centers, 0, ground_truth_distribution,
                                    step='mid',
                                    facecolor=transparent, edgecolor='k')
    
    for iData, num_proj_used, noise in zip(idx_lst, num_proj_used_lst, noise_lst):
        min_idx = (dist_values[iData]!=0).argmax(axis=1).min()
        max_idx = num_bins_dist-1-(np.fliplr(dist_values[iData])!=0).argmax(axis=1).min()
        min_edge = dist_edges[min_idx]
        max_edge = dist_edges[max_idx+1]

        # ax[iData,0].set_xlim(min(0,min_edge*1.1), max_edge*1.1)
        ax[iData,0].set_xlim(-0.5, 3.0)
        ax[iData,0].set_yscale('log')
        ax[iData,0].set_ylim(1, 1e10)
        ax[iData,0].grid(True, which="both", ls="-")
        
        ax[iData,0].set_title('$\\rho$ = {}%. {} = {}.'.format(noise, '$N_{\\mathrm{proj}}$', num_proj_used))
        ax[iData,0].set_xlabel('Pixel value')
    # fig.tight_layout(rect=[0, 0, 0.85, 1], w_pad=0.5, h_pad=3.0)
    # ax[0,0].legend(handles=proxy_legend, bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0.)
    fig.tight_layout(rect=[0, 0, 1, 1], w_pad=0.5, h_pad=1.5)
    # ax[0,0].legend(loc='upper center', handles=proxy_legend, ncol=3)
    ax[0,0].legend(loc='upper left', handles=proxy_legend, ncol=1, bbox_to_anchor=(-0.02, 1))
    file_name = 'distribution_diagonal_column.pdf'
    fig.savefig(file_path_plots+file_name)
    # %% Iterations used column
    plot_names = ['a', 'b', 'c']
    ncp_lst = [reco_type if reco_type != 0 else None for reco_type in reco_type_lst]
    if num_ncp > np.sum(np.array(reco_type_lst) == 0):
        ncp_lst.remove(None)
    fig, ax = plt.subplots(num_data,1, squeeze=False, figsize=cfig_size)
    for iData, num_proj_used, noise in zip(idx_lst, num_proj_used_lst, noise_lst):
            for iReco, reco_type in enumerate(ncp_lst):
                # if reco_type == 4 or reco_type == 3:
                    # print(iProj, iNoise, iReco, '\n\t', iterations_used[iProj, iNoise, iReco],'\n')
                label_name = reco_names[reco_type]
                ax[iData,0].plot(iterations_used[iData, iReco], label=label_name, color=RGB[iReco+1])
                ax[iData,0].grid(True, which="both", ls="-")
                # ax[iData,0].legend(loc='best')
                ax[iData,0].set_xlabel('Time step in simulation')
                ax[iData,0].set_ylabel('Number of Iterations')
                ax[iData,0].set_title('$\\rho$ = {}%. {} = {}.'.format(noise, '$N_{\\mathrm{proj}}$', num_proj_used))
            ax[iData,0].set_ylim(0, np.max(iterations_used[iData])*1.2)
            ax[iData,0].set_xlim(0, 100)
            ax[iData,0].text(0.925, 0.95, plot_names[iData],
                     horizontalalignment='left',
                     verticalalignment='top',
                     fontweight='bold',
                     fontsize=BIGGER_SIZE,
                     transform=ax[iData,0].transAxes)
    ax[0,0].legend(loc='center right', bbox_to_anchor=(1.0, 0.55))
    fig.tight_layout(rect=[0, 0, 1, 1], w_pad=2.0, h_pad=2.0)
    file_name = 'ncp_iterations_diagonal_column.pdf'
    fig.savefig(file_path_plots+file_name)
    # %% Iterations used row
    plot_names = ['a', 'b', 'c']
    ncp_lst = [reco_type if reco_type != 0 else None for reco_type in reco_type_lst]
    if num_ncp > np.sum(np.array(reco_type_lst) == 0):
        ncp_lst.remove(None)
    fig, ax = plt.subplots(1, num_data, squeeze=False, figsize=(21,7))
    for iData, num_proj_used, noise in zip(idx_lst, num_proj_used_lst, noise_lst):
            for iReco, reco_type in enumerate(ncp_lst):
                label_name = reco_names[reco_type]
                ax[0,iData].plot(iterations_used[iData, iReco], label=label_name, color=RGB[iReco+1])
                ax[0,iData].grid(True, which="both", ls="-")
                # ax[0,iData].legend(loc='best')
                ax[0,iData].set_xlabel('Time step in simulation')
                ax[0,iData].set_ylabel('Number of Iterations')
                ax[0,iData].set_title('$\\rho$ = {}%. {} = {}.'.format(noise, '$N_{\\mathrm{proj}}$', num_proj_used))
                ax[0,iData].set_ylim(0, np.max(iterations_used[iData])*1.1)
                ax[0,iData].text(0.975, 0.975, plot_names[iData],
                         horizontalalignment='right',
                         verticalalignment='top',
                         fontweight='bold',
                         fontsize=BIGGER_SIZE,
                         transform=ax[0,iData].transAxes)
    ax[0,0].legend(loc='center right', bbox_to_anchor=(1.0, 0.55))
    fig.tight_layout(rect=[0, 0, 1, 1], w_pad=2.0, h_pad=2.0)
    file_name = 'ncp_iterations_diagonal_row.pdf'
    fig.savefig(file_path_plots+file_name)
    # %% Reconstruction examples 3x5
    slice_idx = 170
    frame_idx = 50
    fig, ax = plt.subplots(num_data,5, squeeze=False, figsize=(20,10))
    for iReco, reco_type in enumerate(reco_type_lst):
        for iData, num_proj_used, noise in zip(idx_lst, num_proj_used_lst, noise_lst):
            tmp_data = recos[iData, iReco, frame_idx, slice_idx]-ground_truth[frame_idx, slice_idx]
            sd = np.std(tmp_data)            
            im = ax[iData,iReco].imshow(tmp_data, cmap='coolwarm',
                                        vmin=-2.5*sd, vmax=2.5*sd)
            ax[iData,iReco].set_xticks([])
            ax[iData,iReco].set_yticks([])
            if iData == 0:
                ax[iData,iReco].set_title(reco_names[reco_type])
            if iReco == 0:
                ax[iData,iReco].set_ylabel('$\\rho$ = {}%\n{} = {}'.format(noise, '$N_{\\mathrm{proj}}$', num_proj_used))
            divider = make_axes_locatable(ax[iData,iReco])
            cax = divider.append_axes("right", size="5%", pad=0.1)
            plt.colorbar(im, cax=cax)
    fig.suptitle('Time step {}, slice {}'.format(frame_idx, slice_idx))
    fig.tight_layout(rect=[0, 0, 1, 0.95], w_pad=1.5, h_pad=0.5)
    file_name = 'reco_examples_slice{}_frame{}.pdf'.format(frame_idx, slice_idx)
    fig.savefig(file_path_plots+file_name)
    # %% Reconstruction examples 5x2
    slice_idx = 170
    frame_idx = 50
    # for color in ['viridis', 'plasma', 'inferno', 'magma', 'cividis']:
    fig, ax = plt.subplots(5,2, squeeze=False, figsize=cfig_size)
    for iReco, reco_type in enumerate(reco_type_lst):
        iCol = 0
        for iData, num_proj_used, noise in zip(idx_lst, num_proj_used_lst, noise_lst):
            if iData == 1:
                continue
            if iData == 2:
                iCol = 1
            ax[iReco, iCol].imshow(recos[iData, iReco, frame_idx, slice_idx],
                                    cmap='gray', vmin=0, vmax=2.5)
            ax[iReco, iCol].set_xticks([])
            ax[iReco, iCol].set_yticks([])
            if iData == 0:
                ax[iReco, iCol].set_ylabel(reco_names[reco_type])
            if iReco == 0:
                ax[iReco, iCol].set_title('$\\rho$ = {}%\n{} = {}'.format(noise, '$N_{\\mathrm{proj}}$', num_proj_used))
    # fig.suptitle('Time step {}/{}. Vertical slice {}'.format(frame_idx+1, num_frames, slice_idx))
    fig.tight_layout(rect=[0, 0, 1, 1], w_pad=1.0, h_pad=0.5)
    file_name = 'reco_examples_extreme_column_slice{}_frame{}.pdf'.format(frame_idx, slice_idx)
    fig.savefig(file_path_plots+file_name)
    # %% Reconstruction examples 2x5
    slice_idx = 170
    frame_idx = 50
    # for color in ['viridis', 'plasma', 'inferno', 'magma', 'cividis']:
    fig, ax = plt.subplots(2,5, squeeze=False, figsize=(cfig_size[1], cfig_size[0]))
    for iReco, reco_type in enumerate(reco_type_lst):
        iRow = 0
        for iData, num_proj_used, noise in zip(idx_lst, num_proj_used_lst, noise_lst):
            if iData == 1:
                continue
            if iData == 2:
                iRow = 1
            im = ax[iRow,iReco].imshow(recos[iData, iReco, frame_idx, slice_idx],
                                    cmap='gray', vmin=0, vmax=2.5)
            ax[iRow,iReco].set_xticks([])
            ax[iRow,iReco].set_yticks([])
            if iData == 0:
                ax[iRow,iReco].set_title(reco_names[reco_type])
            if iReco == 0:
                ax[iRow,iReco].set_ylabel('$\\rho$ = {}%\n{} = {}'.format(noise, '$N_{\\mathrm{proj}}$', num_proj_used))
            if iReco == 4:
                cb_ax = inset_axes(ax[iRow,iReco], width="5%",
                                      height="100%", loc='right',
                                      bbox_to_anchor=(0.5, 0.0, 0.6, 1.0),
                                      bbox_transform=ax[iRow,iReco].transAxes)
                fig.colorbar(im, cax=cb_ax)
    # fig.suptitle('Time step {}/{}. Vertical slice {}'.format(frame_idx+1, num_frames, slice_idx))
    fig.tight_layout(rect=[0, 0, 0.96, 1], w_pad=1.0, h_pad=1.0)
    file_name = 'reco_examples_extreme_row_slice{}_frame{}.pdf'.format(slice_idx, frame_idx)
    fig.savefig(file_path_plots+file_name)
    # %% Reconstruction examples 2x5
    slice_idx = 170
    frame_idx = 50
    num_sigma = 3
    
    clims = [0.1, 0.5]
    fig, ax = plt.subplots(2,5, squeeze=False, figsize=(cfig_size[1], cfig_size[0]-0.95))
    for iReco, reco_type in enumerate(reco_type_lst):
        iRow = 0
        for iData, num_proj_used, noise in zip(idx_lst, num_proj_used_lst, noise_lst):
            if iData == 1:
                continue
            if iData == 2:
                iRow = 1
            tmp_data = recos[iData, iReco, frame_idx, slice_idx]-ground_truth[frame_idx, slice_idx]
            tmp_sigma = np.std(tmp_data)
            # im = ax[iRow,iReco].pcolormesh(tmp_data,
            #            norm=matplotlib.colors.SymLogNorm(linthresh=0.03, linscale=1,
            #                                   vmin=-clims[iRow], vmax=clims[iRow]),
            #            cmap='coolwarm', shading='auto')
            im = ax[iRow,iReco].imshow(tmp_data, cmap='coolwarm',
                                    vmin=-clims[iRow], vmax=clims[iRow])
                                    # vmin=-num_sigma*tmp_sigma, vmax=num_sigma*tmp_sigma)
                                  # )
            ax[iRow,iReco].set_xticks([])
            ax[iRow,iReco].set_yticks([])
            if iData == 0:
                ax[iRow,iReco].set_title(reco_names[reco_type])
            if iReco == 0:
                ax[iRow,iReco].set_ylabel('$\\rho$ = {}%\n{} = {}'.format(noise, '$N_{\\mathrm{proj}}$', num_proj_used))
            if iReco == 4:
                cb_ax = inset_axes(ax[iRow,iReco], width="5%",
                                      height="100%", loc='right',
                                      bbox_to_anchor=(0.5, 0.0, 0.575, 1.0),
                                      bbox_transform=ax[iRow,iReco].transAxes)
                fig.colorbar(im, cax=cb_ax)
    # fig.suptitle('Time step {}/{}. Vertical slice {}'.format(frame_idx+1, num_frames, slice_idx))
    fig.tight_layout(rect=[0, 0, 0.96, 1], w_pad=1, h_pad=0.5)
    file_name = 'reco_error_examples_extreme_row_slice{}_frame{}.pdf'.format(slice_idx, frame_idx)
    fig.savefig(file_path_plots+file_name)
    # %% Static reco - ncp min vs. true min
    noise, num_proj, reco_type = '0.25', '720', 2
    
    file_path = '/home/pwra/simulation/output_files/'
    file_name = 'static_reconstruction_residual_Noise{}_NumProj{}_RecoType{}_NumItr{}.npy'.format(noise, num_proj, reco_type, 'NCP')
    residual = np.load(file_path+file_name)
    
    file_name = 'static_reconstruction_ncp_Noise{}_NumProj{}_RecoType{}_NumItr{}.npy'.format(noise, num_proj, reco_type, 'NCP')
    ncp = np.load(file_path+file_name)
    # %
    true_min = np.argmin(residual, axis=1)
    est_min = np.argmin(ncp, axis=1)
    min_val = np.min([residual.min(), ncp.min()])-10
    max_val = np.max([residual.max(), ncp.max()])+10
    
    # %% Incorrect
    fig, ax = plt.subplots(1,2,squeeze=False,figsize=(12,6))
    
    ax[0,0].plot(residual[0, :], '-', label='$||x - \\bar{x}||_2$')
    label_name = 'min({})'.format('$||x - \\bar{x}||_2$')
    ax[0,0].scatter(true_min[0], residual[0, true_min[0]], label=label_name)
    ax[0,0].plot(ncp[0, :], '-', label='NCP')
    ax[0,0].scatter(est_min[0], ncp[0, est_min[0]], label='min(NCP)')
    ax[0,0].set_yscale('log')
    ax[0,0].set_ylim(min_val, max_val)
    ax[0,0].grid()
    ax[0,0].set_title('Initial time frame')
    ax[0,0].set_xlabel('Iteration number')
    ax[0,0].set_ylabel('Arbitrary units')
    ax[0,0].legend(loc='best')
    
    ax[0,1].plot(residual[1, :], '-', label='$||x - \\bar{x}||_2$')
    label_name = 'min({})'.format('$||x - \\bar{x}||_2$')
    ax[0,1].scatter(true_min[1], residual[1, true_min[1]], label=label_name)
    ax[0,1].plot(ncp[1, :], '-', label='NCP')
    ax[0,1].scatter(est_min[1], ncp[1, est_min[1]], label='min(NCP)')
    ax[0,1].set_yscale('log')
    ax[0,1].set_ylim(min_val, max_val)
    ax[0,1].grid()
    ax[0,1].set_title('Final time frame')
    ax[0,1].set_xlabel('Iteration number')
    ax[0,1].set_ylabel('Arbitrary units')
    ax[0,1].legend(loc='best')
    
    # fig.suptitle('True minima vs. NCP', fontsize = 16)
    
    fig.tight_layout(rect=[0, 0, 1, 1], w_pad=0.5, h_pad=0.5)
    fig.savefig(file_path_plots+'ncp_true.pdf')
    # %% Static reco - SIRT 1 vs SIRT 2
    noise, num_proj, reco_type = '0.25', '360', [1, 2]
    num_static_data = len([noise])*len([num_proj])*len(reco_type)
    
    residual_static = np.zeros((num_static_data, 2, 1800), dtype=np.float32)
    ncp_static = np.zeros((num_static_data, 2, 1800), dtype=np.float32)
    
    for iData in range(num_static_data):
        print(iData, num_proj, noise, reco_type[iData])
        file_path = '/home/pwra/simulation/output_files/'
        file_name = 'static_reconstruction_residual_Noise{}_NumProj{}_RecoType{}_NumItr{}.npy'.format(noise, num_proj, reco_type[iData], 'NCP')
        residual_static[iData] = np.load(file_path+file_name)[:, 0:1800]
    
        file_name = 'static_reconstruction_ncp_Noise{}_NumProj{}_RecoType{}_NumItr{}.npy'.format(noise, num_proj, reco_type[iData], 'NCP')
        ncp_static[iData] = np.load(file_path+file_name)[:, 0:1800]
    
    # %
    fig, ax = plt.subplots(1,2,squeeze=False,figsize=(21,7))
    for iData in range(num_static_data):
        
        true_min = np.argmin(residual_static[iData], axis=1)
        est_min = np.argmin(ncp_static[iData], axis=1)
        min_val = np.min([residual_static[iData].min(), ncp_static[iData].min()])-10
        max_val = np.max([residual_static[iData].max(), ncp_static[iData].max()])+10
        
        ax[0,0].plot(residual_static[iData, 0, :], '-', label='$||x - \\bar{x}||_2$')
        label_name = 'min({})'.format('$||x - \\bar{x}||_2$')
        ax[0,0].scatter(true_min[0], residual_static[iData, 0, true_min[0]])
        ax[0,0].plot(ncp_static[iData, 0, :], '-', label='NCP')
        ax[0,0].scatter(est_min[0], ncp_static[iData, 0, est_min[0]])
        # ax[0,0].set_yscale('log')
        ax[0,0].set_xlim(0, 1800)
        ax[0,0].set_ylim(0, 500)
        ax[0,0].set_title('Initial time frame')
        ax[0,0].set_xlabel('Iteration number')
        ax[0,0].set_ylabel('Arbitrary units')
        
        ax[0,1].plot(residual_static[iData, 1, :], '-', label='$||x - \\bar{x}||_2$')
        label_name = 'min({})'.format('$||x - \\bar{x}||_2$')
        ax[0,1].scatter(true_min[1], residual_static[iData, 1, true_min[1]])
        ax[0,1].plot(ncp_static[iData, 1, :], '-', label='NCP')
        ax[0,1].scatter(est_min[1], ncp_static[iData, 1, est_min[1]])
        # ax[0,1].set_yscale('log')
        ax[0,1].set_xlim(0, 1800)
        ax[0,1].set_ylim(0, 500)
        ax[0,1].set_title('Final time frame')
        ax[0,1].set_xlabel('Iteration number')
        ax[0,1].set_ylabel('Arbitrary units')
    ax[0,0].legend(loc='best', ncol=1)
    
    for iRow in range(ax.shape[1]):
        ax[0,iRow].grid()
    fig.tight_layout(rect=[0, 0, 1, 1], w_pad=0.5, h_pad=0.5)
    fig.savefig(file_path_plots+'NCP_min_GT_min_Noise{}_NumProj{}.pdf'.format(noise, num_proj))
    # %% Iterations used flip
    reco_type = 4
    noise = '1.00'
    num_proj_used = '120'
    iterations_used_flip = np.zeros((2, 100), dtype=np.float32)
    file_path = '/home/pwra/simulation/output_files/noise{}/num_proj_used{}/reco_type{}/'.format(noise, num_proj_used, reco_type)
    file_name = 'dynamic_reconstruction_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}_iterations_used.npy'.format(noise, num_proj_used, reco_type, 'NCP')
    iterations_used_flip[0] = np.load(file_path+file_name)
    file_name = 'dynamic_reconstruction_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}_iterations_used_Flip.npy'.format(noise, num_proj_used, reco_type, 'NCP')
    iterations_used_flip[1] = np.load(file_path+file_name)
    
    leg_name = ['Normal time order',
                'Reversed time order']
    style = ['-', '--']
    fig, ax = plt.subplots(1,1,squeeze=False, figsize=(cfig_size[0], cfig_size[1]/3))
    for iData in range(iterations_used_flip.shape[0]):
            ax[0,0].plot(iterations_used_flip[iData], label=leg_name[iData])
    ax[0,0].legend(loc='best')
    ax[0,0].grid()
    ax[0,0].set_title('{}, $\\rho$ = {}%. {} = {}.'.format(reco_names[reco_type], noise, '$N_{\\mathrm{proj}}$', num_proj_used))
    ax[0,0].set_xlabel('Time step in simulation')
    ax[0,0].set_ylabel('Number of iterations')
    fig.tight_layout(rect=[0, 0, 1, 1], w_pad=1.0, h_pad=0.5)
    fig.savefig(file_path_plots+'iterations_used_flip.pdf')
    # %%
    reco_diffs = np.zeros((num_data, len(reco_type_lst)-2,num_pixels, num_pixels),
                          dtype=np.float32)
    for iData in range(num_data):
        for iDiff in range(reco_diffs.shape[0]):
            reco_diffs[iData, iDiff] = recos[iData, iDiff+1, frame_idx, slice_idx]-recos[iData, iDiff+2, frame_idx, slice_idx]
    sd = np.std(reco_diffs, axis=(2,3))
    # %%
    fig, ax = plt.subplots(3,num_data,squeeze=False, figsize=(15,13))
    for iData in range(num_data):
        for iRow in range(ax.shape[0]):
            im = ax[iRow,iData].imshow(reco_diffs[iData, iRow], cmap='coolwarm', vmin=-3*sd[iData, iRow], vmax=3*sd[iData, iRow])
            ax[iRow,iData].set_xticks([])
            ax[iRow,iData].set_yticks([])
            divider = make_axes_locatable(ax[iRow,iData])
            cax = divider.append_axes("right", size="5%", pad=0.1)
            plt.colorbar(im, cax=cax)
    for iData, num_proj_used, noise in zip(idx_lst, num_proj_used_lst, noise_lst):
        ax[0,iData].set_title('$\\rho$ = {}%. {} = {}.'.format(noise, '$N_{\\mathrm{proj}}$', num_proj_used))
    for iRow in range(ax.shape[0]):
        ax[iRow, 0].set_ylabel(reco_names[iRow+1]+' - '+reco_names[iRow+2])
    fig.tight_layout(rect=[0, 0, 1, 1], w_pad=1.5, h_pad=0.5)
    fig.savefig(file_path_plots+'reco_differences.pdf')
    # %%
    prefix = 'frozen_'
    noise = '0.25'
    num_proj_used = '360'
    
    res_norm_frozen = np.zeros((2, 4, num_frames), dtype=np.float32)
    
    for iReco, reco_type in enumerate(args.reco_type):
        if iReco == 0:
            continue
        file_path = '/home/pwra/simulation/output_files/noise{}/num_proj_used{}/reco_type{}/'.format(noise, num_proj_used, reco_type)
        file_name = '{}res_l2_norm_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}.npy'.format(prefix, noise, num_proj_used, reco_type, 'NCP')
        res_norm_frozen[0, iReco-1] = np.load(file_path+file_name)
        
        file_name = '{}res_l1_norm_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}.npy'.format(prefix, noise, num_proj_used, reco_type, 'NCP')
        res_norm_frozen[1, iReco-1] = np.load(file_path+file_name)
    # %%
    plot_names = ['a', 'b']
    yticks_pos = [1e2, 1.1e2, 1.2e2, 1.3e2, 1.4e2, 1.5e2, 1.6e2, 1.7e2, 1.8e2, 1.9e2]
    yticks_labels = [str(i) for i in yticks_pos]
    
    fig, ax = plt.subplots(1,2, squeeze=False, figsize=(14.5,7))
    for iReco, reco_type in enumerate(args.reco_type[1:]):
        ax[0,1].plot(res_norm_frozen[0,iReco,:], color=RGB[reco_type], label=reco_names[reco_type])
    
    ax[0,1].set_yscale('log')
    ax[0,1].set_yticks(yticks_pos)
    ax[0,1].set_yticklabels(yticks_labels, minor=False)
    ax[0,1].set_yticks([], minor=True)
    ax[0,1].get_yaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
    ax[0,1].grid(True, which="both", ls="-")
    
    # ax[0,0].set_xlim(-0.5,19.5)
    # ax[0,0].set_xticks([0,2,4,6,8,10,12,14,16,18])
    ax[0,1].set_title('$\\rho$ = {}%. {} = {}.'.format(noise, '$N_{\\mathrm{proj}}$', num_proj_used))
    ax[0,1].set_xlabel('Time step in simulation')
    ax[0,1].set_ylabel('$\ell_2$-norm difference')
    
    yticks_pos = [1.5e5, 2e5, 3e5, 4e5, 5e5, 6e5]
    yticks_labels = [str(i) for i in yticks_pos]
    
    for iReco, reco_type in enumerate(args.reco_type[1:]):
        ax[0,0].plot(res_norm_frozen[1,iReco,:], color=RGB[reco_type], label=reco_names[reco_type])
    
    ax[0,0].set_yscale('log')
    ax[0,0].set_yticks(yticks_pos)
    ax[0,0].set_yticklabels(yticks_labels, minor=False)
    ax[0,0].set_yticks([], minor=True)
    ax[0,0].get_yaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
    ax[0,0].grid(True, which="both", ls="-")
    ax[0,0].ticklabel_format(axis='y',style='sci', scilimits=(0,0))
    
    # ax[0,1].set_xlim(-0.5,19.5)
    # ax[0,1].set_xticks([0,2,4,6,8,10,12,14,16,18])
    ax[0,0].set_title('$\\rho$ = {}%. {} = {}.'.format(noise, '$N_{\\mathrm{proj}}$', num_proj_used))
    ax[0,0].set_xlabel('Time step in simulation')
    ax[0,0].set_ylabel('$\ell_1$-norm difference')
    ax[0,1].legend(loc='best')
    fig.tight_layout(rect=[0, 0, 1, 1], w_pad=2.0, h_pad=2.0)
    
    for iCol in range(ax.shape[1]):
        ax[0,iCol].text(0.975, 0.975, plot_names[iCol],
                        horizontalalignment='right',
                        verticalalignment='top',
                        fontweight='bold',
                        fontsize=BIGGER_SIZE,
                        transform=ax[0,iCol].transAxes)
    
    file_name = 'frozen_reconstruction.pdf'
    fig.savefig(file_path_supplementary+file_name)
    
    # %%
    frame_idx, slice_idx = 50, 170
    reco_error = np.zeros((num_data, len(reco_type_lst), num_pixels, num_pixels),
                          dtype=np.float32)
    for iData in range(num_data):
        for iReco in range(len(reco_type_lst)):
            reco_error[iData, iReco] = recos[iData, iReco, frame_idx, slice_idx]-ground_truth[frame_idx, slice_idx]
    sd = np.std(reco_error, axis=(2,3))
    # %%
    fig, ax = plt.subplots(5,num_data,squeeze=False, figsize=(15,13))
    for iData in range(num_data):
        for iReco in range(len(reco_type_lst)):
            im = ax[iReco,iData].imshow(reco_error[iData, iReco], cmap='coolwarm', vmin=-3*sd[iData, iReco], vmax=3*sd[iData, iReco])
            ax[iReco,iData].set_xticks([])
            ax[iReco,iData].set_yticks([])
            divider = make_axes_locatable(ax[iReco,iData])
            cax = divider.append_axes("right", size="5%", pad=0.1)
            plt.colorbar(im, cax=cax)
    # for iData, num_proj_used, noise in zip(idx_lst, num_proj_used_lst, noise_lst):
        # ax[0,iData].set_title('$\\rho$ = {}%. {} = {}.'.format(noise, '$N_{\\mathrm{proj}}$', num_proj_used))
    # for iRow in range(ax.shape[0]):
        # ax[iRow, 0].set_ylabel(reco_names[iRow+1]+' - '+reco_names[iRow+2])
    fig.tight_layout(rect=[0, 0, 1, 1], w_pad=1.5, h_pad=0.5)
    fig.savefig(file_path_plots+'reco_errors.pdf')
    
    
    # %%
    def update_data(val):
        iFrame = int(sframe.val)
        iSlice = int(sslice.val)
        for iReco, reco_type in enumerate(reco_type_lst):
            for iData, num_proj_used, noise in zip(idx_lst, num_proj_used_lst, noise_lst):
                iIdx = iData + len(idx_lst)*iReco
                tmp_data = recos[iData, iReco, iFrame, iSlice]-ground_truth[iFrame, iSlice]
                sd = np.std(tmp_data)
                im[iIdx].set_data(tmp_data)
                # im[iIdx].set_clim(-2.5*sd, 2.5*sd)
    
    im = []
    lim = [0.1, 0.15, 0.3]
    fig, ax = plt.subplots(num_data,5, squeeze=False, figsize=(20,10))
    for iReco, reco_type in enumerate(reco_type_lst):
        for iData, num_proj_used, noise in zip(idx_lst, num_proj_used_lst, noise_lst):
            iIdx = iData + len(idx_lst)*iReco
            tmp_data = recos[iData, iReco, 0, 0]-ground_truth[0, 0]
            sd = np.std(tmp_data)
            im.append(ax[iData,iReco].imshow(tmp_data, cmap='coolwarm',
                                        vmin=-lim[iData], vmax=lim[iData]))
            ax[iData,iReco].set_xticks([])
            ax[iData,iReco].set_yticks([])
            if iData == 0:
                ax[iData,iReco].set_title(reco_names[reco_type])
            if iReco == 0:
                ax[iData,iReco].set_ylabel('$\\rho$ = {}%\n{} = {}'.format(noise, '$N_{\\mathrm{proj}}$', num_proj_used))
            divider = make_axes_locatable(ax[iData,iReco])
            cax = divider.append_axes("right", size="5%", pad=0.1)
            plt.colorbar(im[iIdx], cax=cax)
    # fig.suptitle('Time step {}, slice {}'.format(frame_idx, slice_idx))
    
    axcolor = 'lightgoldenrodyellow'
    
    ax_frame = plt.axes([0.2, 0.05, 0.6, 0.02], facecolor=axcolor)
    sframe = Slider(ax_frame, 'Frame', 0, num_frames-1, valinit=0, valstep=1)
    
    ax_slice = plt.axes([0.2, 0.02, 0.6, 0.02], facecolor=axcolor)
    sslice = Slider(ax_slice, 'Slice', 0, num_pixels-1, valinit=0, valstep=1)
    
    sframe.on_changed(update_data)
    sslice.on_changed(update_data)
    
    fig.tight_layout(rect=[0, 0.06, 1, 1], w_pad=1.5, h_pad=0.5)
    # %%
    def update_data(val):
        iFrame = int(sframe.val)
        iSlice = int(sslice.val)
        for iReco, reco_type in enumerate(reco_type_lst):
            for iData, num_proj_used, noise in zip(idx_lst, num_proj_used_lst, noise_lst):
                iIdx = iData + len(idx_lst)*iReco
                tmp_data = recos[iData, iReco, iFrame, iSlice]
                im[iIdx].set_data(tmp_data)
                # im[iIdx].set_clim(-2.5*sd, 2.5*sd)
    
    im = []
    lim = [0.1, 0.15, 0.3]
    fig, ax = plt.subplots(num_data,5, squeeze=False, figsize=(20,10))
    for iReco, reco_type in enumerate(reco_type_lst):
        for iData, num_proj_used, noise in zip(idx_lst, num_proj_used_lst, noise_lst):
            iIdx = iData + len(idx_lst)*iReco
            tmp_data = recos[iData, iReco, 0, 0]
            im.append(ax[iData,iReco].imshow(tmp_data, cmap='gray',
                                        vmin=0, vmax=2.5))
            ax[iData,iReco].set_xticks([])
            ax[iData,iReco].set_yticks([])
            if iData == 0:
                ax[iData,iReco].set_title(reco_names[reco_type])
            if iReco == 0:
                ax[iData,iReco].set_ylabel('$\\rho$ = {}%\n{} = {}'.format(noise, '$N_{\\mathrm{proj}}$', num_proj_used))
            divider = make_axes_locatable(ax[iData,iReco])
            cax = divider.append_axes("right", size="5%", pad=0.1)
            plt.colorbar(im[iIdx], cax=cax)
    # fig.suptitle('Time step {}, slice {}'.format(frame_idx, slice_idx))
    
    axcolor = 'lightgoldenrodyellow'
    
    ax_frame = plt.axes([0.2, 0.05, 0.6, 0.02], facecolor=axcolor)
    sframe = Slider(ax_frame, 'Frame', 0, num_frames-1, valinit=0, valstep=1)
    
    ax_slice = plt.axes([0.2, 0.02, 0.6, 0.02], facecolor=axcolor)
    sslice = Slider(ax_slice, 'Slice', 0, num_pixels-1, valinit=0, valstep=1)
    
    sframe.on_changed(update_data)
    sslice.on_changed(update_data)
    
    fig.tight_layout(rect=[0, 0.06, 1, 1], w_pad=1.5, h_pad=0.5)
# %%
if __name__ == '__main__':
    # Construct the argument parser
    parser = argparse.ArgumentParser()
    
    # Add the arguments to the parser
    parser.add_argument("-num_proj_used", "--num_proj_used", required=False,
                        nargs='?', default=[360, 120, 45],
                        type=lambda s: [item for item in s.split(',')], help="first operand")
    
    parser.add_argument("-noise", "--noise", required=False,
                        nargs='?', default=['0.25', '1.00', '5.00'],
                        type=lambda s: [item for item in s.split(',')], help="second operand")
    
    parser.add_argument("-reco_type", "--reco_type", required=False,
                        nargs='?', default=[0, 1, 2, 3, 4],
                        type=lambda s: [item for item in s.split(',')], help="third operand")
    
    parser.add_argument("-num_itr", "--num_itr", required=False,
                        nargs='?', default=['NCP'],
                        type=lambda s: [item for item in s.split(',')], help="third operand")
    
    parser.add_argument("-prefix", "--prefix", required=False, default='')
    args = parser.parse_args()
    # %%
    main(args)

