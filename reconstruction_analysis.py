#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr  8 14:22:55 2020

@author: Peter Winkel Rasmussen
"""
# import os
# import sys
import numpy as np
import cupy as cp
import argparse
from skimage.draw import circle

file_path = '/home/pwra/simulation/output_files/'
file_path_plots = '/home/pwra/simulation/plots/'
# ground_truth = np.load(file_path+'rescaled512_simulation_attenuation_rotated.npy')
ground_truth = np.load(file_path+'simulation_attenuation_rotated.npy')
num_frame, num_slice, num_row, num_col = ground_truth.shape

prefix = 'frozen_'

scale = 1
mask_outer = np.ones((num_row, num_col), dtype='bool')
mask_inner = np.zeros((num_row, num_col), dtype='bool')
rr, cc = circle(num_row//2-1, num_col//2-1, 124*scale) #127
mask_outer[rr, cc] = False
mask_inner[rr, cc] = True
mask_inner_rock = np.tile(mask_inner, (num_frame, num_slice, 1, 1))
# %%
def main(args):
    # %%
    noise = args.noise
    num_proj_used = args.num_proj_used
    reco_type = args.reco_type
    num_itr_list = args.file_list
    num_files = len(num_itr_list)
    
    res_l2_norm = np.zeros((num_files, num_frame), dtype=np.float32)
    res_l1_norm = np.zeros((num_files, num_frame), dtype=np.float32)
    
    res_l2_norm_4d = np.zeros((num_files,), dtype=np.float32)
    res_l1_norm_4d = np.zeros((num_files,), dtype=np.float32)
    
    vol_elements = np.count_nonzero(mask_inner_rock)//num_frame
    bin_width = 0.02
    
    min_edge = -10
    max_edge = 10
    num_bins = (max_edge-min_edge)/bin_width
    if num_bins.is_integer():
        num_bins = int(num_bins)
    else:
        raise ValueError('bin_width or bin_range results in non-integer'+
                         'number of bins')
    bin_values = np.zeros((num_files, num_bins), dtype=np.float32)
    bin_values_time = np.zeros((num_files, num_frame, num_bins), dtype=np.float32)
    bin_edges = np.linspace(min_edge, max_edge, num_bins+1, endpoint=True, dtype=np.float32)
    
    min_val = -10
    max_val = 15
    num_bins_dist = (max_val-min_val)/bin_width
    if num_bins_dist.is_integer():
        num_bins_dist = int(num_bins_dist)
    else:
        raise ValueError('bin_width or bin_range results in non-integer'+
                         'number of bins')
    dist_values = np.zeros((num_files, num_bins_dist), dtype=np.float32)
    dist_values_time = np.zeros((num_files, num_frame, num_bins_dist), dtype=np.float32)
    # dist_edges = np.linspace(min_val, max_val, num_bins_dist+1, endpoint=True, dtype=np.float32)
    # %%
    if args.mode == 'cpu':
        ground_truth_inner = ground_truth[mask_inner_rock].reshape(num_frame, vol_elements)
        for iData, num_itr in enumerate(num_itr_list):
            file_path = '/home/pwra/simulation/output_files/noise{}/num_proj_used{}/reco_type{}/'.format(noise, num_proj_used, reco_type)
            file_name = '{}dynamic_reconstruction_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}.npy'.format(prefix, noise, num_proj_used, reco_type, num_itr)
            data = np.load(file_path+file_name)[mask_inner_rock].reshape(num_frame, vol_elements)
            
            tmp_bin, tmp_edge = np.histogram(data.ravel(), bins=num_bins_dist, range=(min_val, max_val))
            dist_values[iData] = tmp_bin
            
            for iFrame in range(num_frame):
                tmp_bin, tmp_edge = np.histogram(data[iFrame, :].ravel(), bins=num_bins_dist, range=(min_val, max_val))
                dist_values_time[iData, iFrame] = tmp_bin
        
            res = (data-ground_truth_inner[0][None])
            tmp_bin, tmp_edge = np.histogram(res.ravel(), bins=num_bins, range=(min_edge, max_edge))
            bin_values[iData] = tmp_bin
            
            for iFrame in range(num_frame):
                tmp_bin, tmp_edge = np.histogram(res[iFrame, :].ravel(), bins=num_bins, range=(min_edge, max_edge))
                bin_values_time[iData, iFrame] = tmp_bin
            
            res_l2_norm[iData] = np.linalg.norm(res, ord=2, axis=1)
            res_l1_norm[iData] = np.linalg.norm(res, ord=1, axis=1)
            
            res_l2_norm_4d[iData] = np.linalg.norm(res.ravel(), ord=2)
            res_l1_norm_4d[iData] = np.linalg.norm(res.ravel(), ord=1)
            
        ground_truth_inner = None
        # data = None
        # res = None
        tmp_bin = None
        tmp_edge = None
    
    elif args.mode == 'gpu':
        with cp.cuda.Device(args.gpu):
            tmp_edge = cp.array(bin_edges)
            mempool = cp.get_default_memory_pool()
            ground_truth_inner = cp.array(ground_truth[mask_inner_rock])
            for iData, num_itr in enumerate(num_itr_list):
                file_path = '/home/pwra/simulation/output_files/noise{}/num_proj_used{}/reco_type{}/'.format(noise, num_proj_used, reco_type)
                file_name = 'dynamic_reconstruction_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}.npy'.format(noise, num_proj_used, reco_type, num_itr)
                data = cp.array(np.load(file_path+file_name)[mask_inner_rock])
            
                res = (data-ground_truth_inner).reshape(num_frame, vol_elements)
                data = None
                mempool.free_all_blocks()            
                
                tmp_bin, tmp_edge = cp.histogram(res.ravel(), bins=tmp_edge)
                bin_values[iData] = cp.asnumpy(tmp_bin)
                
                for iFrame in range(num_frame):
                    tmp_bin, tmp_edge = cp.histogram(res[iFrame, :].ravel(), bins=num_bins, range=(min_edge, max_edge))
                    bin_values_time[iData, iFrame] = tmp_bin
                
                res_l2_norm[iData] = cp.asnumpy(cp.linalg.norm(res, ord=2, axis=1))
                res_l1_norm[iData] = cp.asnumpy(cp.linalg.norm(res, ord=1, axis=1))
                res = None
                mempool.free_all_blocks()
            
            ground_truth_inner = None
            data = None
            res = None
            tmp_bin = None
            tmp_edge = None
            mempool.free_all_blocks()
    else:
        raise ValueError("Chosen mode of calculation does not exist!\n"
                         "Select 'gpu' or 'cpu' instead of {}".format(args.mode))
        
    # %%
    for iData, num_itr in enumerate(num_itr_list):
        file_name = '{}res_l2_norm_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}.npy'.format(prefix, noise, num_proj_used, reco_type, num_itr)
        np.save(file_path+file_name, res_l2_norm)
        file_name = '{}res_l1_norm_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}.npy'.format(prefix, noise, num_proj_used, reco_type, num_itr)
        np.save(file_path+file_name, res_l1_norm)
        
        file_name = '{}res_l2_norm_4d_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}.npy'.format(prefix, noise, num_proj_used, reco_type, num_itr)
        np.save(file_path+file_name, res_l2_norm_4d)
        file_name = '{}res_l1_norm_4d_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}.npy'.format(prefix, noise, num_proj_used, reco_type, num_itr)
        np.save(file_path+file_name, res_l1_norm_4d)
        
        file_name = '{}rec_binned_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}.npy'.format(prefix, noise, num_proj_used, reco_type, num_itr)
        np.save(file_path+file_name, dist_values)
        file_name = '{}rec_binned_time_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}.npy'.format(prefix, noise, num_proj_used, reco_type, num_itr)
        np.save(file_path+file_name, dist_values_time)
        
        file_name = '{}res_binned_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}.npy'.format(prefix, noise, num_proj_used, reco_type, num_itr)
        np.save(file_path+file_name, bin_values)
        file_name = '{}res_binned_time_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}.npy'.format(prefix, noise, num_proj_used, reco_type, num_itr)
        np.save(file_path+file_name, bin_values_time)
        
# %%
if __name__ == '__main__':
    # Construct the argument parser
    parser = argparse.ArgumentParser()
    
    # Add the arguments to the parser
    # File selection
    parser.add_argument("-num_proj_used", "--num_proj_used", required=False,
                        nargs='?', default=360, type=int, help="first operand")
    parser.add_argument("-noise", "--noise", required=False,
                        nargs='?', default='0.25', type=str, help="second operand")
    parser.add_argument("-reco_type", "--reco_type", required=False,
                        nargs='?', default=2, type=int, help="third operand")
    # Files to analyse
    parser.add_argument('-file_list', '--file_list', required=False,
                        nargs='?', default=['NCP'], 
                        type=lambda s: [item for item in s.split(',')], help='fourth operand')

    # How to compute
    parser.add_argument("-mode", "--mode", required=False,
                        nargs='?', default='cpu', type=str, help="fifth operand")
    parser.add_argument("-gpu", "--gpu", required=False,
                        nargs='?', default=0, type=int, help="sixth operand")
    args = parser.parse_args()
    # %%
    main(args)