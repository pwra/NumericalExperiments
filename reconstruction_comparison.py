#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 19 10:56:08 2020

@author: Peter Winkel Rasmussen
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 10 13:55:58 2020

@author: Peter Winkel Rasmussen
"""
# import os
# import sys
import numpy as np
# import cupy as cp
from skimage.metrics import structural_similarity as ssim
import argparse
from skimage.draw import circle
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import matplotlib.style as style
from matplotlib.widgets import Slider
style.use('seaborn')
style.use('ggplot')
from cycler import cycler
from matplotlib.colors import hsv_to_rgb, to_rgba
# import matplotlib.animation as animation
from mpl_toolkits.axes_grid1 import make_axes_locatable
params = {'axes.grid': False, 'savefig.dpi': 200,
          'text.color': 'black'}
plt.rcParams.update(params)

SMALL_SIZE = 13
MEDIUM_SIZE = 15
BIGGER_SIZE = 17

plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title


file_path = '/home/pwra/simulation/output_files/'
file_path_plots = '/home/pwra/simulation/plots/'
ground_truth = np.load(file_path+'simulation_attenuation.npy')
num_frame, num_slice, num_row, num_col = ground_truth.shape
# %
mask_outer = np.ones((num_row, num_col), dtype='bool')
mask_inner = np.zeros((num_row, num_col), dtype='bool')
rr, cc = circle(num_row//2-1, num_col//2-1, 127)
mask_outer[rr, cc] = False
mask_inner[rr, cc] = True
# %
mask_inner_rock = np.tile(mask_inner, (100, 256, 1, 1))
# %
H = np.linspace(0, 1, 17)
V = np.ones_like(H)
S = np.ones_like(H)
HSV = np.dstack((H,S,V))
seed = 137
np.random.seed(seed)
RGB = hsv_to_rgb(HSV)[0][:-1]
# np.random.shuffle(RGB)
# %
default_cycler = (
                    cycler(color=RGB)
#                    cycler(color=['aqua', 'black', 'blue', 'brown',
#                                'chartreuse', 'chocolate', 'coral', 'crimson',
#                                'darkblue', 'darkgreen', 'fuchsia', 'goldenrod',
#                                'green', 'grey', 'indigo', 'lime', 
#                                'magenta', 'maroon', 'navy', 'olive'])
                   + cycler(linestyle=['-', '--', '-', '--',
                                    '-', '--', '-', '--',
                                    '-.', ':', '-.', ':',
                                    '-.', ':', '-.', ':'])
                                    # '-', ':', '-', ':'])
#                  + cycler(marker=['.', '.', '.', '.',  
#                                 '.', '.', '.', '.', 
#                                 '.', '.', '.', '.', 
#                                 '.', '.', '.', '.',])
                 )
plt.rc('axes', prop_cycle=default_cycler)
# %%
def SSIM(reco, ground_truth):
    num_frame, num_slice, num_row, num_col = reco.shape
    ssim_im = np.zeros_like(reco, dtype=np.float32)
    for iFrame in range(num_frame):
        for iSlice in range(num_slice):
            mssim, ssim_im[iFrame, iSlice] = ssim(reco[iFrame, iSlice], ground_truth[iFrame, iSlice], full=True)
    return ssim_im
    
# %%
def main(args):
    # %%
    noise = args['noise']
    num_proj_used = args['num_proj_used']
    reco_type = args['reco_type']
    comp_type = 'best_recos'#args['comp_type']
    if reco_type == 0 and comp_type != 'best_recos':
        num_itr_list = [None]
    elif any(reco_type == np.array([1,2,3])) or comp_type == 'best_recos':
        num_itr_list = [1, 5, 10, 15, 20, 50, 100, 500]
    else:
        raise ValueError('"reco_type" does not exist!\n'
                         'Select 0,1,2,3 instead of {}'.format(reco_type))
        
    # %%
    if comp_type == 'reco_type':
        data = []
        for iData, num_itr in enumerate(num_itr_list):
            file_path = '/home/pwra/simulation/output_files/noise{}/num_proj_used{}/reco_type{}/'.format(noise, num_proj_used, reco_type)
            file_name = 'dynamic_reconstruction_Noise{}_NumProjUsed{}_NumItr{}.npy'.format(noise, num_proj_used, num_itr)
            data.append(np.load(file_path+file_name))
        data = np.array(data)
    elif comp_type == 'best_recos':
        data = []
        res_norm = []
        file_path = '/home/pwra/simulation/output_files/noise{}/'.format(noise)
        f = open(file_path+'best_performance.txt', 'r')
        file_descriptors = []
        for full_file_name in f:
            tmp = full_file_name.strip().split('/')[-1].split('_')[3:]
            # tmp_dict = {tmp[0][:11] : int(tmp[0][11:]),
            #             tmp[1][:8] : int(tmp[1][8:]),
            #             tmp[2][:6] : int(tmp[2][6:-4])}
            tmp_lst = [[tmp[0][:11],int(tmp[0][11:])],
                        [tmp[1][:8],int(tmp[1][8:])],
                        [tmp[2][:6],int(tmp[2][6:-4])]]
            file_descriptors.append(tmp_lst)
            file_name = full_file_name.strip()
            data.append(np.load(file_name))
            
            file_path = '/home/pwra/simulation/output_files/noise{}/num_proj_used{}/reco_type{}/'.format(noise, int(tmp[0][11:]), int(tmp[1][8:]))
            file_name = 'res_norm_Noise{}_NumProjUsed{}_RecoType{}.npy'.format(noise, int(tmp[0][11:]), int(tmp[1][8:]))
            res_tmp = np.load(file_path+file_name)
            itr_idx = np.argwhere(int(tmp[2][6:-4]) == np.array(num_itr_list))[0][0]
            res_norm.append(res_tmp[itr_idx])
        data = np.array(data)
        res_norm = np.array(res_norm)
        file_descriptors = np.array(file_descriptors)
        f.close()
    else:
        raise ValueError('"comp_type" does not exist!\n'
                         'Select "reco_type" or "best_recos" instead of {}'.format(reco_type))
    # %%
    num_frame, num_slice, num_row, num_col = ground_truth.shape
    all_SSIM = np.zeros((len(num_itr_list), num_frame, num_slice, num_row, num_col), dtype=np.float32)
    for iData in range(len(num_itr_list)):
        all_SSIM[iData] = SSIM(data[iData], ground_truth)
    # %%
    if comp_type == 'best_recos':
        fig, ax = plt.subplots(1,1, squeeze=False, figsize=(10,8))
        for iData in range(res_norm.shape[0]):
            label_name = 'Reco Type {}.\nNumber of projections used {}.\nReconstruction type {}.'.format(file_descriptors[iData, 1, 1],
                                                                                                          file_descriptors[iData, 0, 1],
                                                                                                          file_descriptors[iData, 2, 1])
            ax[0,0].plot(res_norm[iData], label=label_name)
        ax[0,0].set_ylim(100, 1000)
        ax[0,0].set_xlim(0, 100)
        ax[0,0].set_yscale('log')
        ax[0,0].set_yticks([100, 200, 300, 400, 500, 600, 700, 800, 900, 1000])
        ax[0,0].get_yaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
        box = ax[0,0].get_position()
        ax[0,0].set_position([box.x0, box.y0, box.width * 0.8, box.height])
        ax[0,0].legend(loc='center left', bbox_to_anchor=(1, 0.5), fontsize=8)
        # ax[0,0].legend(loc='upper right', bbox_to_anchor=(0.5, 0.4, 0.5, 0.5))
        ax[0,0].grid(True, which="both", ls="-")
        ax[0,0].set_xlabel('Time step in simulation')
        ax[0,0].set_ylabel('|reconstruction - ground truth|')
        # fig.suptitle('Noise = {}%. Number of projections used {}. Reconstruction type {}.'.format(noise, num_proj_used, reco_type))
        fig.tight_layout(rect=[0, 0, 0.85, 0.94])
        file_name = 'TimeStepResidual_Noise{}_NumProjUsed{}_RecoType{}.pdf'.format(noise, num_proj_used, reco_type)
        fig.savefig(file_path_plots+file_name)
    # %%
    def update_data(val):
        iData = int(sdata.val)
        iFrame = int(sframe.val)
        iSlice = int(sslice.val)
        im.set_data(data[iData, iFrame, iSlice, :, :])
        # arr_min = np.nanmin(all_SSIM[iData, iFrame, iSlice, :, :])
        # arr_max = np.nanmax(all_SSIM[iData, iFrame, iSlice, :, :])
        # im.set_clim(arr_min, arr_max)

    fig = plt.figure(figsize=(9,9))
    ax = fig.add_subplot(111)
    im = ax.imshow(data[0,0,0,:,:], cmap='viridis', vmin=0, vmax=2.5)
    
    # ax.set_title('Slice number = %i' % (0))
    axcolor = 'lightgoldenrodyellow'
    
    ax_frame = plt.axes([0.2, 0.08, 0.6, 0.02], facecolor=axcolor)
    sdata = Slider(ax_frame, 'Dataset', 0, data.shape[0]-1, valinit=0, valstep=1)
    
    ax_frame = plt.axes([0.2, 0.05, 0.6, 0.02], facecolor=axcolor)
    sframe = Slider(ax_frame, 'Frame', 0, data.shape[1]-1, valinit=0, valstep=1)
    
    ax_slice = plt.axes([0.2, 0.02, 0.6, 0.02], facecolor=axcolor)
    sslice = Slider(ax_slice, 'Slice', 0, data.shape[2]-1, valinit=0, valstep=1)
    
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.1)
    plt.colorbar(im, cax=cax)
    
    sdata.on_changed(update_data)
    sframe.on_changed(update_data)
    sslice.on_changed(update_data)
    
    plt.tight_layout(rect=[0, 0.1, 1, 1])
    plt.show()
    # %%
    if comp_type == 'reco_type':
        def update_plot(iUpdate):
            fig.suptitle('Noise = {}%. Number of projections used {}.\nTime Step = {}'.format(noise, num_proj_used, iUpdate))
            iReco = 0
            for iRow in range(ax.shape[0]):
                for iCol in range(ax.shape[1]):
                    iCount = iCol + iRow*ax.shape[1]
                    if iCount == 4:
                        im[iCount].set_data(ground_truth[iUpdate, 0])
                    else:
                        im[iCount].set_data(data[iReco, iUpdate, 0])
                        iReco += 1
            
            # ax.set_title('Slice nr. %i' % (iUpdate+1))
        # return im_ax, im_circ,
        
        iReco = 0
        fig, ax = plt.subplots(3,3, squeeze=False, figsize=(10,10))
        im = []
        for iRow in range(ax.shape[0]):
            for iCol in range(ax.shape[1]):
                iCount = iCol + iRow*ax.shape[1]
                if iCount == 4:
                        im.append(ax[iRow, iCol].imshow(ground_truth[0, 0], cmap='viridis',
                                              vmin=0, vmax=2.5))
                        ax[iRow, iCol].set_title('Ground Truth')
                else:
                    im.append(ax[iRow, iCol].imshow(data[iReco, 0, 0], cmap='viridis',
                                          vmin=0, vmax=2.5))
                    ax[iRow, iCol].set_title('Number of iterations {}.'.format(num_itr_list[iReco]))
                    iReco += 1
                divider = make_axes_locatable(ax[iRow, iCol])
                cax = divider.append_axes("right", size="5%", pad=0.1)
                plt.colorbar(im[iCount], cax=cax)
                
        fig.suptitle('Noise = {}%. Number of projections used {}.\nTime Step = {}'.format(noise, num_proj_used, 0))
        plt.tight_layout(rect=[0, 0, 1, 0.925])
        
        ani = animation.FuncAnimation(fig, update_plot, num_frame-1, interval=100)
        writer = animation.writers['ffmpeg'](fps=10)
        movie_name = 'animation_Noise{}_NumProjUsed{}.mp4'.format(noise, num_proj_used)
        ani.save(file_path_plots+movie_name, writer=writer, dpi=300)
    else:
        def update_plot(iUpdate):
            fig.suptitle('Time Step = {}'.format(iUpdate))
            dumb_list = [0, 3, 6, 9, 12,
                      1, 4, 7, 10, 13,
                      2, 5, 8, 11, 14]
            for iRow in range(ax.shape[0]):
                for iCol in range(ax.shape[1]):
                    iCount = iCol + iRow*ax.shape[1]
                    data_idx = dumb_list[iCount]
                    if iCount == 15:
                        im[iCount].set_data(ground_truth[iUpdate, 0])
                    else:
                        im[iCount].set_data(data[data_idx, iUpdate, 0])        
        iReco = 0
        dumb_list = [0, 3, 6, 9, 12,
                      1, 4, 7, 10, 13,
                      2, 5, 8, 11, 14]
        fig, ax = plt.subplots(3,5, squeeze=False, figsize=(5*2.2,3.25*2.2))
        im = []
        for iRow in range(ax.shape[0]):
            for iCol in range(ax.shape[1]):
                iCount = iCol + iRow*ax.shape[1]
                data_idx = dumb_list[iCount]
                if iCount == 15:
                        im.append(ax[iRow, iCol].imshow(ground_truth[0, 0], cmap='viridis',
                                              vmin=0, vmax=2.5))
                        ax[iRow, iCol].set_title('Ground Truth')
                else:
                    im.append(ax[iRow, iCol].imshow(data[data_idx, 0, 0], cmap='viridis',
                                          vmin=0, vmax=2.5))
                    title = 'Reco Type {}\nNumber of projections {}\nNumber of iterations {}.'.format(file_descriptors[data_idx, 1, 1],
                                                                                                      file_descriptors[data_idx, 0, 1],
                                                                                                      file_descriptors[data_idx, 2, 1])
                    ax[iRow, iCol].set_title(title, fontsize=9)
                    iReco += 1
                ax[iRow, iCol].set_xticks([])
                ax[iRow, iCol].set_yticks([])
                # divider = make_axes_locatable(ax[iRow, iCol])
                # cax = divider.append_axes("right", size="5%", pad=0.1)
                # plt.colorbar(im[iCount], cax=cax)
                
        fig.suptitle('Time Step = {}'.format(0))
        plt.tight_layout(rect=[0, 0, 1, 0.95])
        
        ani = animation.FuncAnimation(fig, update_plot, num_frame-1, interval=100)
        writer = animation.writers['ffmpeg'](fps=8)
        movie_name = 'animation_Noise{}_best.mp4'.format(noise)
        ani.save(file_path_plots+movie_name, writer=writer, dpi=250)
    # %%
    def update_plot(iUpdate):
        fig.suptitle('Noise = {}%. Number of projections used {}.\nTime Step = {}'.format(noise, num_proj_used, iUpdate))
        iReco = 0
        for iRow in range(ax.shape[0]):
            for iCol in range(ax.shape[1]):
                iCount = iCol + iRow*ax.shape[1]
                if iCount == 4:
                    im[iCount].set_data(ground_truth[iUpdate, 0])
                    pass
                else:
                    im[iCount].set_data(all_SSIM[iReco, iUpdate, 0])
                    iReco += 1
        
        # ax.set_title('Slice nr. %i' % (iUpdate+1))
    # return im_ax, im_circ,
    
    iReco = 0
    fig, ax = plt.subplots(3,3, squeeze=False, figsize=(10,10))
    im = []
    for iRow in range(ax.shape[0]):
        for iCol in range(ax.shape[1]):
            iCount = iCol + iRow*ax.shape[1]
            if iCount == 4:
                im.append(ax[iRow, iCol].imshow(ground_truth[0, 0], cmap='viridis',
                                          vmin=0, vmax=2.5))
                ax[iRow, iCol].set_title('Ground Truth')
            else:
                im.append(ax[iRow, iCol].imshow(all_SSIM[iReco, 0, 0], cmap='inferno',
                                      vmin=0, vmax=1))
                ax[iRow, iCol].set_title('Number of iterations {}.'.format(num_itr_list[iReco]))
                iReco += 1
                divider = make_axes_locatable(ax[iRow, iCol])
                cax = divider.append_axes("right", size="5%", pad=0.1)
                plt.colorbar(im[iCount], cax=cax)
            
    fig.suptitle('Noise = {}%. Number of projections used {}.\nTime Step = {}'.format(noise, num_proj_used, 0))
    plt.tight_layout(rect=[0, 0, 1, 0.925])
    
    ani = animation.FuncAnimation(fig, update_plot, num_frame-1, interval=100)
    writer = animation.writers['ffmpeg'](fps=30)
    movie_name = 'SSIM_animation_Noise{}_NumProjUsed{}.mp4'.format(noise, num_proj_used)
    ani.save(file_path_plots+movie_name, writer=writer, dpi=300)
# %%
if __name__ == '__main__':
    # Construct the argument parser
    parser = argparse.ArgumentParser()
    
    # Add the arguments to the parser
    parser.add_argument("-num_proj_used", "--num_proj_used", required=False,
                        nargs='?', const=1, default=360, type=int, help="first operand")
    parser.add_argument("-noise", "--noise", required=False,
                        nargs='?', const=1, default='1.1', type=str, help="second operand")
    parser.add_argument("-reco_type", "--reco_type", required=False,
                        nargs='?', const=1, default=0, type=int, help="third operand")
    parser.add_argument("-comp_type", "--comp_type", required=False,
                        nargs='?', const=1, default='reco_type', type=str, help="third operand")
    args = vars(parser.parse_args())
    # %%
    main(args)