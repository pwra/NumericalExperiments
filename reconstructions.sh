# num_proj_used_arr=(45 120 360)
# reco_type_arr=(0 1 2 3 4)
# # gpu_offset=2
# for num_proj_used in "${num_proj_used_arr[@]}"; do
# 	# echo "num_proj_used: $num_proj_used"
# 	for reco_type in "${reco_type_arr[@]}"; do
# 		gpu_idx="$(($reco_type-$gpu_offset))"
# 		# nohup python reconstruction_analysis.py -num_proj_used $num_proj_used -reco_type $reco_type -noise 1.1 &
# 		# nohup python plotting.py -num_proj_used $num_proj_used -reco_type $reco_type &
# 		# nohup python dynamic_reconstruction_SIRT.py -reco_type $reco_type -num_proj_used $num_proj_used -gpu $gpu_idx -noise 1.1 &
# 		# sleep 5
# 	done
# done

# num_proj_used_arr=(360)
# noise_arr=(0.25)
# reco_type_arr=(1 2 3)
# for idx_num_proj_used in "${!num_proj_used_arr[@]}"; do
# 	for idx_noise in "${!noise_arr[@]}"; do
# 		for idx_reco_type in "${!reco_type_arr[@]}"; do
# 			# nohup python reconstruction_analysis.py -num_proj_used ${num_proj_used_arr[$idx_num_proj_used]} -reco_type ${reco_type_arr[$idx_reco_type]} -noise ${noise_arr[$idx_noise]} -file_list 'NCP' -mode 'cpu' &
# 			count=$(($idx_reco_type+$idx_num_proj_used*3*2+$idx_noise*2))
# 			if ! ((count % 2));
# 			then
# 			    nohup python dynamic_reconstruction_SIRT.py -reco_type ${reco_type_arr[$idx_reco_type]} -num_proj_used ${num_proj_used_arr[$idx_num_proj_used]} -gpu '1' -noise ${noise_arr[$idx_noise]} -use_NCP 'True' &
# 			    # nohup python stopping_rules.py -num_proj_used ${num_proj_used_arr[$idx_num_proj_used]} -reco_type ${reco_type_arr[$idx_reco_type]} -noise ${noise_arr[$idx_noise]} -gpu '0' -num_steps 20 -num_itr 500 &
# 			    # echo $count ${reco_type_arr[$idx_reco_type]} ${num_proj_used_arr[$idx_num_proj_used]} ${noise_arr[$idx_noise]} "0"
# 			else
# 				nohup python dynamic_reconstruction_SIRT.py -reco_type ${reco_type_arr[$idx_reco_type]} -num_proj_used ${num_proj_used_arr[$idx_num_proj_used]} -gpu '0' -noise ${noise_arr[$idx_noise]} -use_NCP 'True' &
# 				# nohup python stopping_rules.py -num_proj_used ${num_proj_used_arr[$idx_num_proj_used]} -reco_type ${reco_type_arr[$idx_reco_type]} -noise ${noise_arr[$idx_noise]} -gpu '1' -num_steps 20 -num_itr 500 &
# 				# echo $count ${reco_type_arr[$idx_reco_type]} ${num_proj_used_arr[$idx_num_proj_used]} ${noise_arr[$idx_noise]} "1"
# 			fi
# 			# echo ${reco_type_arr[$idx_reco_type]} ${num_proj_used_arr[$idx_num_proj_used]} ${noise_arr[$idx_noise]}
# 			# nohup python dynamic_reconstruction_SIRT.py -reco_type $reco_type -num_proj_used $num_proj_used -gpu 0 -noise $noise &
# 			# sleep 5
# 		done
# 	done
# done

# nohup python stopping_rules.py -noise 5.00 -num_proj_used 360 -reco_type 1 -num_itr 700 -num_steps 40 -gpu 0 &
nohup python stopping_rules.py -noise 5.00 -num_proj_used 120 -reco_type 1 -num_itr 700 -num_steps 40 -gpu 1 &
# nohup python stopping_rules.py -noise 5.00 -num_proj_used 45 -reco_type 1 -num_itr 700 -num_steps 40 -gpu 0 &
# nohup python stopping_rules.py -noise 1.00 -num_proj_used 360 -reco_type 1 -num_itr 700 -num_steps 40 -gpu 0 &
# nohup python stopping_rules.py -noise 1.00 -num_proj_used 120 -reco_type 1 -num_itr 700 -num_steps 40 -gpu 0 &
nohup python stopping_rules.py -noise 1.00 -num_proj_used 45 -reco_type 1 -num_itr 700 -num_steps 40 -gpu 0 &
# nohup python stopping_rules.py -noise 0.25 -num_proj_used 360 -reco_type 1 -num_itr 700 -num_steps 40 -gpu 1 &
# nohup python stopping_rules.py -noise 0.25 -num_proj_used 120 -reco_type 1 -num_itr 700 -num_steps 40 -gpu 1 &
# nohup python stopping_rules.py -noise 0.25 -num_proj_used 45 -reco_type 1 -num_itr 700 -num_steps 40 -gpu 1 &


nohup python stopping_rules.py -noise 5.00 -num_proj_used 360 -reco_type 2 -num_itr 700 -num_steps 40 -gpu 1 &
# nohup python stopping_rules.py -noise 5.00 -num_proj_used 120 -reco_type 2 -num_itr 700 -num_steps 40 -gpu 1 &
# nohup python stopping_rules.py -noise 5.00 -num_proj_used 45 -reco_type 2 -num_itr 700 -num_steps 40 -gpu 0 &
nohup python stopping_rules.py -noise 1.00 -num_proj_used 360 -reco_type 2 -num_itr 700 -num_steps 40 -gpu 0 &
# nohup python stopping_rules.py -noise 1.00 -num_proj_used 120 -reco_type 2 -num_itr 700 -num_steps 40 -gpu 0 &
nohup python stopping_rules.py -noise 1.00 -num_proj_used 45 -reco_type 2 -num_itr 700 -num_steps 40 -gpu 1 &
# nohup python stopping_rules.py -noise 0.25 -num_proj_used 360 -reco_type 2 -num_itr 700 -num_steps 40 -gpu 0 &
nohup python stopping_rules.py -noise 0.25 -num_proj_used 120 -reco_type 2 -num_itr 700 -num_steps 40 -gpu 0 &
nohup python stopping_rules.py -noise 0.25 -num_proj_used 45 -reco_type 2 -num_itr 700 -num_steps 40 -gpu 1 &


# nohup python stopping_rules.py -noise 5.00 -num_proj_used 360 -reco_type 3 -num_itr 700 -num_steps 40 -gpu 1 &
nohup python stopping_rules.py -noise 5.00 -num_proj_used 120 -reco_type 3 -num_itr 700 -num_steps 40 -gpu 0 &
# nohup python stopping_rules.py -noise 5.00 -num_proj_used 45 -reco_type 3 -num_itr 700 -num_steps 40 -gpu 1 &
# nohup python stopping_rules.py -noise 1.00 -num_proj_used 360 -reco_type 3 -num_itr 700 -num_steps 40 -gpu 1 &
# nohup python stopping_rules.py -noise 1.00 -num_proj_used 120 -reco_type 3 -num_itr 700 -num_steps 40 -gpu 1 &
# nohup python stopping_rules.py -noise 1.00 -num_proj_used 45 -reco_type 3 -num_itr 700 -num_steps 40 -gpu 1 &
nohup python stopping_rules.py -noise 0.25 -num_proj_used 360 -reco_type 3 -num_itr 700 -num_steps 40 -gpu 1 &
# nohup python stopping_rules.py -noise 0.25 -num_proj_used 120 -reco_type 3 -num_itr 700 -num_steps 40 -gpu 0 &
# nohup python stopping_rules.py -noise 0.25 -num_proj_used 45 -reco_type 3 -num_itr 700 -num_steps 40 -gpu 0 &


nohup python stopping_rules.py -noise 5.00 -num_proj_used 360 -reco_type 4 -num_itr 700 -num_steps 40 -gpu 0 &
# nohup python stopping_rules.py -noise 5.00 -num_proj_used 120 -reco_type 4 -num_itr 700 -num_steps 40 -gpu 0 &
nohup python stopping_rules.py -noise 5.00 -num_proj_used 45 -reco_type 4 -num_itr 700 -num_steps 40 -gpu 1 &
# nohup python stopping_rules.py -noise 1.00 -num_proj_used 360 -reco_type 4 -num_itr 700 -num_steps 40 -gpu 0 &
nohup python stopping_rules.py -noise 1.00 -num_proj_used 120 -reco_type 4 -num_itr 700 -num_steps 40 -gpu 0 &
# nohup python stopping_rules.py -noise 1.00 -num_proj_used 45 -reco_type 4 -num_itr 700 -num_steps 40 -gpu 0 &
nohup python stopping_rules.py -noise 0.25 -num_proj_used 360 -reco_type 4 -num_itr 700 -num_steps 40 -gpu 1 &
# nohup python stopping_rules.py -noise 0.25 -num_proj_used 120 -reco_type 4 -num_itr 700 -num_steps 40 -gpu 1 &
nohup python stopping_rules.py -noise 0.25 -num_proj_used 45 -reco_type 4 -num_itr 700 -num_steps 40 -gpu 0 &

# nohup python dynamic_reconstruction_SIRT.py -reco_type 4 -num_proj_used 120 -gpu '1' -noise '1.00' -use_NCP 'True' -rvrs_time True &

# reco_type_arr=(0)
# for idx_num_proj_used in "${!num_proj_used_arr[@]}"; do
# 	for idx_noise in "${!noise_arr[@]}"; do
# 		for idx_reco_type in "${!reco_type_arr[@]}"; do
# 			nohup python reconstruction_analysis.py -num_proj_used ${num_proj_used_arr[$idx_num_proj_used]} -reco_type ${reco_type_arr[$idx_reco_type]} -noise ${noise_arr[$idx_noise]} -file_list 'None' -mode 'cpu' &
# 			sleep 5
# 		done
# 	done
# done

# nohup python reconstruction_analysis.py -num_proj_used 720 -reco_type 0 -noise 0.25 -file_list 'None' -mode 'cpu' &

# nohup python reconstruction_analysis.py -num_proj_used 45 -reco_type 2 -file_list 'NCP' -mode 'cpu' -noise 0.25 &
# nohup python reconstruction_analysis.py -num_proj_used 45 -reco_type 4 -file_list 'NCP' -mode 'cpu' -noise 0.25 &
# nohup python reconstruction_analysis.py -num_proj_used 120 -reco_type 1 -file_list 'NCP' -mode 'cpu' -noise 0.25 &

# nohup python reconstruction_analysis.py -num_proj_used 45 -reco_type 1 -file_list 'NCP' -mode 'cpu' -noise 1.00 &
# nohup python reconstruction_analysis.py -num_proj_used 120 -reco_type 4 -file_list 'NCP' -mode 'cpu' -noise 1.00 &
# nohup python reconstruction_analysis.py -num_proj_used 360 -reco_type 2 -file_list 'NCP' -mode 'cpu' -noise 1.00 &

# nohup python reconstruction_analysis.py -num_proj_used 45 -reco_type 2 -file_list 'NCP' -mode 'cpu' -noise 5.00 &
# nohup python dynamic_reconstruction_SIRT.py -num_proj_used 360 -reco_type 2 -gpu 0 -noise 0.25 -use_NCP 'True' &
# nohup python dynamic_reconstruction_SIRT.py -num_proj_used 360 -reco_type 4 -gpu 1 -noise 5.00 -use_NCP 'True' &
# nohup python dynamic_reconstruction_SIRT.py -num_proj_used 180 -reco_type 3 -use_NCP True -gpu 0 -noise 1.1 &
# nohup python dynamic_reconstruction_SIRT.py -num_proj_used 360 -reco_type 3 -use_NCP True -gpu 0 -noise 1.1 &


# reco_type_arr=(1)
# reco_shift=
#1 for num_proj_used in "${num_proj_used_arr[@]}"; do
# 	# echo "num_proj_used: $num_proj_used"
# 	for reco_type in "${reco_type_arr[@]}"; do
# 		# for f in output_files/noise*/num_proj_used$num_proj_used/reco_type$reco_type/*.npy; do
# 		for f in output_files/noise*/num_proj_used$num_proj_used/reco_type$reco_type/*.npy; do
# 			old_name=$f
# 			echo $old_name
# 			new_reco="$(($reco_type+$reco_shift))"
# 			# new_name=$(echo $old_name |sed 's/_NumItrNPC/_NumItrNCP/')
# 			# test="_RecoType$reco_type"
# 			# echo $test
# 			new_name=$(echo $old_name |sed "s/_RecoType$reco_type/_RecoType$new_reco/; s/reco_type$reco_type/reco_type$new_reco/")
# 			echo $new_name
# 			mv $old_name $new_name
# 		done
# 	done
# done

# num_proj_used_arr=(45 120 360)
# noise_arr=(0.25 1.00 5.00)
# reco_type_arr=(0 1 2 3 4)
# for num_proj_used in "${num_proj_used_arr[@]}"; do
# 	for noise in "${noise_arr[@]}"; do
# 		for reco_type in "${reco_type_arr[@]}"; do
# 			new_folder="$(pwd)/output_files/noise$noise/num_proj_used$num_proj_used/reco_type$reco_type"
# 			# echo $new_folder
# 			mkdir -p $new_folder
# 		done
# 	done
# done