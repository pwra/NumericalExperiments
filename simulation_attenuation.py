#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  2 09:56:09 2020

@author: Peter Winkel Rasmussen
"""

import os
os.chdir('/home/pwra/simulation/')
import cupy as cp
from cupyx.scipy import ndimage
import numpy as np
import constants as const
import imageio
from skimage.draw import circle
import matplotlib.pyplot as plt
import matplotlib.style as style
from matplotlib.widgets import Slider
style.use('seaborn')
style.use('ggplot')
# import matplotlib.animation as animation
from mpl_toolkits.axes_grid1 import make_axes_locatable
params = {'axes.grid': False, 'savefig.dpi': 200,
          'text.color': 'black',
          'image.cmap': 'viridis'}
plt.rcParams.update(params)

mempool = cp.get_default_memory_pool()
file_path_plots = '/home/pwra/simulation/plots/'

num_frame = 100
num_slice = 512
# %%
def simulation_to_numpy():
    folder_name = '2364_P3_5.1.16C_bottom_01/phi/{:04d}/'.format(0)
    im = imageio.imread(folder_name+'phi{:04d}.tif'.format(0))
    r, c = im.shape
    
    data = np.zeros((num_frame, num_slice, r, c), dtype=type(im[0,0]))
    for iFolder in range(num_frame):
        print('At folder %i/%i' % (iFolder, num_frame))
        folder_name = 'simulation/2364_P3_5.1.16C_bottom_01/phi/{:04d}/'.format(iFolder)
        for iIm in range(num_slice):
            file_name = 'phi{:04d}.tif'.format(iIm)
            data[iFolder, iIm, :, :] = imageio.imread(folder_name+file_name)
            
    file_name = 'phi.npy'
    np.save('output_files/'+file_name, data)
    
def labels_to_numpy():
    folder_name = '2364_P3_5.1.16C_bottom_01/phi/{:04d}/'.format(0)
    im = imageio.imread(folder_name+'phi{:04d}.tif'.format(0))
    r, c = im.shape
    
    folder_name = '2364_P3_5.1.16C_bottom_01/labels/'

    im = imageio.imread(folder_name+'labels{:04d}.tif'.format(0))
    labels = np.zeros((num_slice//2, r, c), dtype=type(im[0,0]))
    
    for iIm in range(num_slice//2):
        file_name = 'labels{:04d}.tif'.format(iIm)
        labels[iIm, :, :] = imageio.imread(folder_name+file_name)
            
    file_name = 'labels.npy'
    np.save('output_files/'+file_name, labels)

# %%
data = np.load('output_files/phi.npy')[:, :num_slice//2, :, :]
labels = np.load('output_files/labels.npy')
# %%
num_rows = data.shape[2]
num_cols = data.shape[3]

mask_outer = np.ones((num_rows, num_cols), dtype='bool')
mask_inner = np.zeros((num_rows, num_cols), dtype='bool')
rr, cc = circle(num_rows//2-1, num_cols//2-1, 127)
mask_outer[rr, cc] = 0
mask_inner[rr, cc] = 1
mask_inner_rock = np.tile(mask_inner, (100, 256, 1, 1))

mask_outer_blur = np.ones((num_rows, num_cols), dtype='bool')
mask_inner_blur = np.zeros((num_rows, num_cols), dtype='bool')
rr, cc = circle(num_rows//2-1, num_cols//2-1, 127-3)
mask_outer_blur[rr, cc] = 0
mask_inner_blur[rr, cc] = 1

# %
idx_rock = np.logical_and(labels[None,:,:,:] == 2, data == 0)
idx_rock_small = np.logical_and(idx_rock, mask_inner_blur)
idx_fluid = np.logical_and(labels[None,:,:,:] != 2, data != 0)
# %%
# Attenuation coefficient from Blunt paper
water_attenuation = const.water_attenuation
oil_attenuation = const.oil_attenuation
data_attenuation = np.copy(data)
data_attenuation[idx_rock] = const.rock_attenuation
data_attenuation[idx_fluid] += 0.5
data_attenuation[idx_fluid] = data_attenuation[idx_fluid]*water_attenuation + (1-data_attenuation[idx_fluid])*oil_attenuation
data_attenuation[:, :, mask_outer[:,:] == 1] = 0
# %%
def gaussian(t=2, s=5):
    '''
    t is sigma^2 and s represents the size of the kernel in standard deviations.
    '''
    sigma = np.sqrt(t)
    x = cp.arange(-np.ceil(s*sigma), np.ceil((s*sigma))+1)
    filt = 1.0/np.sqrt(t*2.0*np.pi)*cp.exp(-(x**2.0)/(2.0*t))
    x = None
    sigma = None
    filt = cp.reshape(filt,(1, filt.size))
    mempool.free_all_blocks()
    return filt
# %%
t = 2
gauss_kernel_1d = gaussian(t)
gauss_kernel_2d = cp.outer(gauss_kernel_1d, gauss_kernel_1d.T)
data_attenuation_blur = cp.asarray(data_attenuation)
data_attenuation_rotated = cp.zeros_like(data_attenuation_blur, dtype=cp.float32)
for iTime in range(num_frame):
    for iSlice in range(num_slice//2):
        tmp_idx = idx_rock_small[iTime,iSlice,:,:]
        
        tmp_smooth = ndimage.convolve(data_attenuation_blur[iTime,iSlice,:,:], gauss_kernel_2d)
        tmp_clean = data_attenuation_blur[iTime,iSlice,:,:]
        tmp_clean[tmp_idx] = tmp_smooth[tmp_idx]
        # tmp_clean[mask_outer_blur] = 2.5
        # tmp_clean[mask_outer] = 0
        data_attenuation_blur[iTime,iSlice,:,:] = tmp_clean
        ndimage.rotate(tmp_clean, angle=-const.angle_shift_deg,
                       axes=(1,0), reshape=False,
                       output=data_attenuation_rotated[iTime,iSlice,:,:])
        tmp_smooth = None
        tmp_clean = None
        mempool.free_all_blocks()
# %%
data_attenuation_rotated = cp.asnumpy(data_attenuation_rotated)
data_attenuation_blur = cp.asnumpy(data_attenuation_blur)

tmp_smooth = None
tmp_clean = None
gauss_kernel_1d = None
gauss_kernel_2d = None
mempool.free_all_blocks()
# %%
import sys
sys.path.append("/home/pwra/TomographicReconstruction/")
import sharpness_functions as sf
scale_factor = 4
tmp = sf.downscale_data(data_attenuation_blur[:10], (1, 1, scale_factor, scale_factor), num_split=6)
np.save('output_files/rescaled{}_simulation_attenuation_blur.npy'.format(tmp.shape[2]), tmp)
tmp = sf.downscale_data(data_attenuation_rotated[:10], (1, 1, scale_factor, scale_factor), num_split=6)
np.save('output_files/rescaled{}_simulation_attenuation_rotated.npy'.format(tmp.shape[2]), tmp)
# %%
def update_data(val):
    iSlice = int(sslice.val)
    iFrame = int(sframe.val)
    im[0].set_data(tmp[iFrame, iSlice, :, :])
    # arr_min = np.nanmin(data_attenuation[iFrame, iSlice, :, :])
    # arr_max = np.nanmax(data_attenuation[iFrame, iSlice, :, :])
    # im.set_clim(arr_min, arr_max)

fig, ax = plt.subplots(1, 1, squeeze=False, figsize=(8,8))
im = []
im.append(ax[0,0].imshow(tmp[0,0,:,:], cmap='viridis'))

# ax.set_title('Slice number = %i' % (0))
axcolor = 'lightgoldenrodyellow'

ax_slice = plt.axes([0.2, 0.02, 0.6, 0.02], facecolor=axcolor)
sslice = Slider(ax_slice, 'Slice', 0, tmp.shape[1]-1,
                valinit=tmp.shape[1]//2-1, valstep=1)

ax_frame = plt.axes([0.2, 0.05, 0.6, 0.02], facecolor=axcolor)
sframe = Slider(ax_frame, 'Frame', 0, tmp.shape[0]-1, valinit=0, valstep=1)

sslice.on_changed(update_data)
sframe.on_changed(update_data)

fig.tight_layout(rect=[0, 0.05, 1, 1])
# %%
mask_inner = np.zeros((num_rows, num_cols), dtype='bool')
rr, cc = circle(num_rows//2-1, num_cols//2-1, 124)
mask_outer[rr, cc] = 0
mask_inner[rr, cc] = 1
mask_inner_rock = np.tile(mask_inner, (100, 256, 1, 1))

bin_width = const.bin_width
min_val = -10
max_val = 15
num_bins = (max_val-min_val)/bin_width
vol_elements = np.count_nonzero(mask_inner_rock)//num_frame
if num_bins.is_integer():
    num_bins = int(num_bins)
else:
    raise ValueError('bin_width or bin_range results in non-integer'+
                     'number of bins')

tmp_data = data_attenuation_rotated[mask_inner_rock].reshape(num_frame, vol_elements)
dist_values, tmp_edge = np.histogram(tmp_data.ravel(), bins=num_bins, range=(min_val, max_val))

dist_values_time = np.zeros((1, num_frame, num_bins), dtype=np.float32)
for iFrame in range(num_frame):
    dist_values_time[0, iFrame], tmp_edge = np.histogram(tmp_data[iFrame].ravel(), bins=num_bins, range=(min_val, max_val))
# %%
np.save('output_files/simulation_attenuation.npy', data_attenuation)
np.save('output_files/simulation_attenuation_blur.npy', data_attenuation_blur)
np.save('output_files/simulation_attenuation_rotated.npy', data_attenuation_rotated)

np.save('output_files/ground_truth_distribution.npy', dist_values)
np.save('output_files/ground_truth_distribution_time.npy', dist_values_time)
# %%
def update_data(val):
    iSlice = int(sslice.val)
    iFrame = int(sframe.val)
    im.set_data(data[iFrame, iSlice, :, :])
    # arr_min = np.nanmin(data[iFrame, iSlice, :, :])
    # arr_max = np.nanmax(data[iFrame, iSlice, :, :])
    # im.set_clim(arr_min, arr_max)

fig = plt.figure(figsize=(9,9))
ax = fig.add_subplot(111)
im = ax.imshow(data[0,0,:,:], cmap='coolwarm', vmin=-0.5, vmax=0.5)

# ax.set_title('Slice number = %i' % (0))
axcolor = 'lightgoldenrodyellow'

ax_slice = plt.axes([0.2, 0.02, 0.6, 0.02], facecolor=axcolor)
sslice = Slider(ax_slice, 'Slice', 0, num_slice//2-1, valinit=0, valstep=1)

ax_frame = plt.axes([0.2, 0.05, 0.6, 0.02], facecolor=axcolor)
sframe = Slider(ax_frame, 'Frame', 0, num_frame-1, valinit=0, valstep=1)

divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.1)
plt.colorbar(im, cax=cax)

sslice.on_changed(update_data)
sframe.on_changed(update_data)

plt.tight_layout(rect=[0, 0.05, 1, 1])
plt.show()
# %%
im = []
fig, ax = plt.subplots(1,2, squeeze=False, figsize=(11,5))
im.append(ax[0,0].imshow(data_attenuation[0,165,:,:], cmap='viridis'))
im.append(ax[0,1].imshow(data_attenuation_blur[0,165,:,:], cmap='viridis'))
ax[0,0].set_title('Ground truth - before blurring')
ax[0,1].set_title('Ground truth - after blurring')

divider = make_axes_locatable(ax[0,0])
cax = divider.append_axes("right", size="5%", pad=0.1)
plt.colorbar(im[0], cax=cax)

divider = make_axes_locatable(ax[0,1])
cax = divider.append_axes("right", size="5%", pad=0.1)
plt.colorbar(im[1], cax=cax)

fig.tight_layout(rect=[0, 0, 1, 0.95])
# fig.savefig(file_path_plots+'ground_truth_blur.pdf')
# %%
def update_data(val):
    iSlice = int(sslice.val)
    iFrame = int(sframe.val)
    im[0].set_data(data_attenuation[iFrame, iSlice, :, :])
    im[1].set_data(data_attenuation_blur[iFrame, iSlice, :, :])
    # arr_min = np.nanmin(data_attenuation[iFrame, iSlice, :, :])
    # arr_max = np.nanmax(data_attenuation[iFrame, iSlice, :, :])
    # im.set_clim(arr_min, arr_max)

fig, ax = plt.subplots(1, 2, squeeze=False, figsize=(12,6.5))
im = []
im.append(ax[0,0].imshow(data_attenuation[0,0,:,:], cmap='viridis'))
im.append(ax[0,1].imshow(data_attenuation_blur[0,0,:,:], cmap='viridis'))

# ax.set_title('Slice number = %i' % (0))
axcolor = 'lightgoldenrodyellow'

ax_slice = plt.axes([0.2, 0.02, 0.6, 0.02], facecolor=axcolor)
sslice = Slider(ax_slice, 'Slice', 0, num_slice//2-1, valinit=0, valstep=1)

ax_frame = plt.axes([0.2, 0.05, 0.6, 0.02], facecolor=axcolor)
sframe = Slider(ax_frame, 'Frame', 0, num_frame-1, valinit=0, valstep=1)

# divider = make_axes_locatable(ax)
# cax = divider.append_axes("right", size="5%", pad=0.1)
# plt.colorbar(im, cax=cax)

sslice.on_changed(update_data)
sframe.on_changed(update_data)

fig.tight_layout(rect=[0, 0.05, 1, 1])
fig.show()
# %%
def update_data(val):
    iSlice = int(sslice.val)
    im.set_data(idx_rock[0,iSlice, :, :])

fig = plt.figure(figsize=(9,9))
ax = fig.add_subplot(111)
im = ax.imshow(idx_rock[0,0,:,:], cmap='gray')

# ax.set_title('Slice number = %i' % (0))
axcolor = 'lightgoldenrodyellow'

ax_slice = plt.axes([0.2, 0.02, 0.6, 0.02], facecolor=axcolor)
sslice = Slider(ax_slice, 'Slice', 0, num_slice//2-1, valinit=0, valstep=1)

# divider = make_axes_locatable(ax)
# cax = divider.append_axes("right", size="5%", pad=0.1)
# plt.colorbar(im, cax=cax)

sslice.on_changed(update_data)

plt.tight_layout(rect=[0, 0.05, 1, 1])
plt.show()

# %%
figure_path = '/home/pwra/simulation/'

fig, ax = plt.subplots(1,3,squeeze=False,figsize=(16,5))

im = ax[0,0].imshow(labels[0,:,:], cmap='gray')
ax[0,0].set_title('Labels')
divider = make_axes_locatable(ax[0,0])
cax = divider.append_axes("right", size="5%", pad=0.1)
plt.colorbar(im, cax=cax)

im = ax[0,1].imshow(data[0,0,:,:], cmap='coolwarm', vmin=-0.5, vmax=0.5)
ax[0,1].set_title('Simulation')
divider = make_axes_locatable(ax[0,1])
cax = divider.append_axes("right", size="5%", pad=0.1)
plt.colorbar(im, cax=cax)

im = ax[0,2].imshow(data_attenuation[0,0,:,:], cmap='gray')
ax[0,2].set_title('Simulation attenuation coefficients')
divider = make_axes_locatable(ax[0,2])
cax = divider.append_axes("right", size="5%", pad=0.1)
plt.colorbar(im, cax=cax)

fig.tight_layout(rect=[0, 0, 1, 1])
fig.savefig(figure_path+'simulation.pdf')