#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar  6 09:19:33 2020

@author: Peter Winkel Rasmussen
"""
import os
os.chdir('/home/pwra/simulation/')
import argparse
import time
import numpy as np
# import cupy as cp
import astra
import constants as const
import auxiliary_functions as af
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider
# import matplotlib.animation as animation
from mpl_toolkits.axes_grid1 import make_axes_locatable
params = {'axes.grid': False, 'savefig.dpi': 200,
          'text.color': 'black'}
plt.rcParams.update(params)

# mempool = cp.get_default_memory_pool()
file_path = '/home/pwra/simulation/output_files/'
# %%
def main(args):
    # %%
    astra.set_gpu_index(args.gpu)
    num_proj = args.num_proj
    noise = args.noise
    use_NCP = args.use_NCP
    # %%
    file_name = 'simulation_projections_Noise{}_NumProj{}.npy'.format(noise, num_proj)
    projections = np.load(file_path+file_name)[:,:,:1024]
    num_frame, num_slice, num_projection, num_pixel = projections.shape
    
    file_name = 'rescaled1024_simulation_attenuation_rotated.npy'
    ground_truth = np.load(file_path+file_name)
    num_frame, num_slice, num_row, num_col = ground_truth.shape
    
    angle_shift = int(np.round(const.angle_shift/(np.pi/args.num_proj)))
    
    projections = np.roll(projections, shift=angle_shift, axis=2)
    projections[:,:,:angle_shift,:] = np.flip(projections[:,:,:angle_shift,:], axis=-1)
    # projections[projections < 0] = 0
    # %%
    angles = np.linspace(0,np.pi,num_projection, endpoint=False)
    proj_geom = astra.create_proj_geom('parallel3d', 1, 1, num_slice, num_pixel, angles);
    vol_geom = astra.create_vol_geom(num_row, num_col, num_slice) # (rows, columns, slices)

    reconstruction = np.zeros((2, num_slice, num_row, num_col), dtype=np.float32)
    reconstruction_GT = np.zeros((2, num_slice, num_row, num_col), dtype=np.float32)
    reconstruction_NumItr = np.zeros((2, num_slice, num_row, num_col), dtype=np.float32)
    residual = np.zeros((2, args.num_itr), dtype=np.float32)
    ncp = np.zeros((2, args.num_itr), dtype=np.float32)
    # %
    for iterator, iFrame in enumerate([0, num_frame-1]):
        print('{}. Reconstructing time step {} out of {} steps'.format(time.asctime(), iFrame, num_frame))
        sinograms = projections[iFrame]
        sinograms_id = astra.data3d.create('-sino', proj_geom, sinograms)
        proj_id = astra.create_projector('cuda3d', proj_geom, vol_geom)
    
        alg_id, rec_id = af.setup_reco_alg_plugin(1, vol_geom, sinograms_id, proj_id, 0)
        
        if use_NCP:
            b = sinograms.ravel()
            # mempool.free_all_blocks()
            rec_npc = np.zeros((5, num_slice, num_row, num_col), dtype=np.float32)
            ncp_tmp = np.zeros((5), dtype=np.float32)
            res_norm_tmp = np.zeros((5), dtype=np.float32)
            iStop = 0
            iItr = 0
            ncp_idx = 0
            gt_idx = 0
            idx_min = 2
            NCP_min = False
            GT_min = False
    
            ## %% Reconstruction
            while iItr < args.num_itr:
                # print('{}. Iteration number {}'.format(time.asctime(), iItr))
                # 257 ms
                astra.algorithm.run(alg_id, 1)
                rec_npc[ncp_idx] = astra.data3d.get(rec_id)

                # 34.6 ms
                residual[iterator, iItr] = np.linalg.norm(rec_npc[ncp_idx].ravel()-ground_truth[iFrame].ravel())
                res_norm_tmp[gt_idx] = residual[iterator, iItr]
                
                # 351 ms
                Ax = af.forward_reco(vol_geom, proj_geom, rec_npc[ncp_idx]).ravel()
                # astra.set_gpu_index(args.gpu)
                # print(astra.astra.info(alg_id),
                #       astra.astra.info(rec_id),
                #       astra.astra.info(sinograms_id))
                
                # 4.39 s at num_proj = 720
                c, ncp_tmp[ncp_idx] = af.cuda_ncp(b, Ax, args.gpu[0])
                ncp[iterator, iItr] = ncp_tmp[ncp_idx]
                
                if np.sum(ncp_tmp[idx_min] < ncp_tmp) == 4 and iItr >= 4:
                    print('\tA NCP minimum reached!')
                    NCP_min = True
                    reconstruction[iterator] = rec_npc[idx_min]
                if np.sum(res_norm_tmp[idx_min] < res_norm_tmp) == 4 and iItr >= 4:
                    print('\tTrue minimum reached!')
                    GT_min = True
                    reconstruction_GT[iterator] = astra.data3d.get(rec_id)
                    
                if not NCP_min and not GT_min:
                    iStop += 1
            
                iItr += 1
                if ncp_idx < 4:
                    ncp_idx += 1
                elif iItr >= 4:
                    # 9.44 mu s
                    ncp_tmp = np.roll(ncp_tmp, -1)
                    # 122 ms
                    rec_npc = np.roll(rec_npc, -1, axis=0)
                if gt_idx < 4:
                    gt_idx += 1
                elif iItr >= 4:
                    res_norm_tmp = np.roll(res_norm_tmp, -1)
            reconstruction_NumItr[iterator] = astra.data3d.get(rec_id)
        else:
            for iItr in range(args.num_itr):
                astra.algorithm.run(alg_id, 1)
        ## Clean up.
        astra.algorithm.delete(alg_id)
        astra.data3d.delete(rec_id)
        astra.data3d.delete(sinograms_id)
    # %%
    if NCP_min:
        file_name = 'static_reconstruction_Noise{}_NumProj{}_RecoType{}_NumItr{}.npy'.format(noise,
                                                                                             num_proj,
                                                                                             args.reco_type,
                                                                                             'NCP')
        np.save(file_path+file_name, reconstruction)
        
        file_name = 'static_reconstruction_residual_Noise{}_NumProj{}_RecoType{}_NumItr{}.npy'.format(noise,
                                                                                                      num_proj,
                                                                                                      args.reco_type,
                                                                                                      'NCP')
        np.save(file_path+file_name, residual)
        
        file_name = 'static_reconstruction_ncp_Noise{}_NumProj{}_RecoType{}_NumItr{}.npy'.format(noise,
                                                                                                 num_proj,
                                                                                                 args.reco_type,
                                                                                                 'NCP')
        np.save(file_path+file_name, ncp)
    
    if GT_min:
        file_name = 'static_reconstruction_Noise{}_NumProj{}_RecoType{}_NumItr{}.npy'.format(noise,
                                                                                             num_proj,
                                                                                             args.reco_type,
                                                                                             'GT')
        np.save(file_path+file_name, reconstruction_GT)
    
    file_name = 'static_reconstruction_Noise{}_NumProj{}_RecoType{}_NumItr{}.npy'.format(noise,
                                                                                         num_proj,
                                                                                         args.reco_type,
                                                                                         args.num_itr)
    np.save(file_path+file_name, reconstruction_NumItr)
    # %%
    # try:
    file_name = 'static_reconstruction_residual_Noise{}_NumProj{}_RecoType{}_NumItr{}.npy'.format(noise, num_proj, args.reco_type, 'NCP')
    residual = np.load(file_path+file_name)
    # except:
    #     pass
    #     # file_name = 'static_reconstruction_residual_Noise{}_NumProj{}_RecoType{}_NumItr{}.npy'.format(noise, num_proj, args.reco_type, args.num_itr)
    #     # residual = np.load(file_path+file_name)
    
    # try:
    file_name = 'static_reconstruction_ncp_Noise{}_NumProj{}_RecoType{}_NumItr{}.npy'.format(noise, num_proj, args.reco_type, 'NCP')
    ncp = np.load(file_path+file_name)
    # except:
    #     file_name = 'static_reconstruction_ncp_Noise{}_NumProj{}_RecoType{}_NumItr{}.npy'.format(noise, num_proj, args.reco_type, args.num_itr)
    #     ncp = np.load(file_path+file_name)
    # %%
    # true_min = np.argmin(residual, axis=1)
    # est_min = np.argmin(ncp, axis=1)
    # min_val = np.min([residual.min(), ncp.min()])-10
    # max_val = np.max([residual.max(), ncp.max()])+10
    
    # fig, ax = plt.subplots(1,2,squeeze=False,figsize=(12,6))
    
    # ax[0,0].plot(residual[0, :], '-', label='|reco - ground truth|')
    # ax[0,0].scatter(true_min[0], residual[0, true_min[0]], label='min(|reco - ground truth|) = {}'.format(true_min[0]))
    # ax[0,0].plot(ncp[0, :], '-', label='NCP')
    # ax[0,0].scatter(est_min[0], ncp[0, est_min[0]], label='min(NCP) = {}'.format(est_min[0]))
    # ax[0,0].set_yscale('log')
    # ax[0,0].set_ylim(min_val, max_val)
    # ax[0,0].grid()
    # ax[0,0].set_title('Initial time frame')
    # ax[0,0].set_xlabel('Iteration number')
    # ax[0,0].set_ylabel('Arbitrary units')
    # ax[0,0].legend(loc='best')
    
    # ax[0,1].plot(residual[1, :], '-', label='|reco - ground truth|')
    # ax[0,1].scatter(true_min[1], residual[1, true_min[1]], label='min(|reco - ground truth|) = {}'.format(true_min[1]))
    # ax[0,1].plot(ncp[1, :], '-', label='NCP')
    # ax[0,1].scatter(est_min[1], ncp[1, est_min[1]], label='min(NCP) = {}'.format(est_min[1]))
    # ax[0,1].set_yscale('log')
    # ax[0,1].set_ylim(min_val, max_val)
    # ax[0,1].grid()
    # ax[0,1].set_title('Final time frame')
    # ax[0,1].set_xlabel('Iteration number')
    # ax[0,1].set_ylabel('Arbitrary units')
    # ax[0,1].legend(loc='best')
    
    # fig.suptitle('True minima vs. NCP', fontsize = 16)
    
    # fig.tight_layout(rect=[0, 0, 1, 0.95])
    # file_path_plots = '/home/pwra/simulation/plots/'
    # fig.savefig(file_path_plots+'ncp_true.pdf')
    
    # print(true_min, est_min)
    # print(residual[:, np.argmin(residual, axis=1)], residual[:, -1])
    # %%
    # q = c.shape[0]
    # c_white = np.arange(1, q+1)/q
    # fig, ax = plt.subplots(1,1,squeeze=False)
    # ax[0,0].plot(c, '--')
    # ax[0,0].plot(c_white)
    # fig.tight_layout()
    # %%
    # data = reconstruction[0]
    # def get_next_itr(val):
    #     iItr = int(sitr.val)
    #     tmp = data[iItr]
    #     im.set_data(tmp)
    #     arr_min = np.nanmin(tmp)
    #     arr_max = np.nanmax(tmp)
    #     im.set_clim(arr_min, arr_max)
    
    # fig, ax = plt.subplots(1, 1, squeeze=False, figsize=(9,9))
    # im = ax[0,0].imshow(data[0], cmap='viridis')
    
    # axcolor = 'lightgoldenrodyellow'
    # ax_itr = plt.axes([0.2, 0.02, 0.6, 0.02], facecolor=axcolor)
    # sitr = Slider(ax_itr, 'Iterator', 0, data.shape[0]-1, valinit=0, valstep=1)
    
    # # divider = make_axes_locatable(ax)
    # # cax = divider.append_axes("right", size="5%", pad=0.1)
    # # plt.colorbar(im, cax=cax)
    # sitr.on_changed(get_next_itr)
    # fig.tight_layout(rect=[0, 0.05, 1, 1])
    # %%    
if __name__ == '__main__':
    # Construct the argument parser
    parser = argparse.ArgumentParser()
    
    # Add the arguments to the parser
    parser.add_argument("-num_proj", "--num_proj", required=False,
                        nargs='?', default=int(360*4), type=int, help="first operand")
    
    parser.add_argument("-noise", "--noise", required=False,
                        nargs='?', default='0.25', type=str, help="second operand")
    
    parser.add_argument("-gpu", "--gpu", required=False,
                        nargs='?', default=[0,1],
                        type=lambda s: [int(item) for item in s.split(',')], help="third operand")
    
    parser.add_argument("-use_NCP", "--use_NCP", required=False,
                        nargs='?', default=True, type=bool, help="fourth operand")
    
    parser.add_argument("-num_itr", "--num_itr", required=False,
                        nargs='?', default=1, type=int, help="fifth operand")
    
    parser.add_argument("-reco_type", "--reco_type", required=False,
                        nargs='?', default=2, type=int, help="fifth operand")
    args = parser.parse_args()
    
    if args.reco_type not in (1,2):
        raise ValueError('Wrong reco method used!')
    # %%
    main(args)