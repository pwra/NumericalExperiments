#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 19 14:47:34 2020

@author: Peter Winkel Rasmussen
"""
import os
os.chdir('/home/pwra/simulation/')
import argparse
import numpy as np
import constants as const
from skimage.draw import circle
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.style as style
from matplotlib.widgets import Slider
style.use('seaborn')
style.use('ggplot')
from cycler import cycler
from matplotlib.colors import hsv_to_rgb, to_rgba, LogNorm
# import matplotlib.animation as animation
from mpl_toolkits.axes_grid1 import make_axes_locatable
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
params = {'axes.grid': False, 'savefig.dpi': 200,
          'text.color': 'black',
          'axes.labelcolor': 'black',
          'xtick.color': 'black',
          'ytick.color': 'black',
          'lines.linewidth': 4,
          'patch.linewidth': 3,
          'lines.markersize': 10}
plt.rcParams.update(params)

SMALL_SIZE = 13+6
MEDIUM_SIZE = 15+6
BIGGER_SIZE = 17+6

plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=MEDIUM_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title

file_path_paper = '/home/pwra/simulation/paper_plots/'
file_path_supplementary = '/home/pwra/simulation/supplementary_plots/'

reco_names = {0 : 'FBP',
             1 : 'SIRT',
             2 : 'SIRT-BC',
             3 : 'SIRT-IC',
             4 : 'SIRT-LC',
             5 : 'FBP ideal'}
# RGB = np.array([[228,26,28],
#                 [55,126,184],
#                 [77,175,74],
#                 [152,78,163],
#                 [255,127,0]])/255
RGB = np.array([[228,26,28],
                [55,126,184],
                [77,175,74],
                [152,78,163],
                [255,127,0],
                [166,206,227]])/255
default_cycler = cycler(color=RGB)
plt.rc('axes', prop_cycle=default_cycler)
linestyle_tuple = [
    ('solid', (0, ())),
    ('dotted',                (0, (1, 1))),
    ('dashed',                (0, (5, 5))),
    ('dashdotted',            (0, (3, 5, 1, 5))),
    ('densely dashdotdotted', (0, (3, 1, 1, 1, 1, 1)))]
# %%
def main(args):
    # %%
    reconstruction_error = np.zeros((len(args.noise),
                                     len(args.num_proj_used),
                                     len(args.reco_type),
                                     args.num_steps,
                                     args.num_itr),
                                    dtype=np.float32)
    ncp = np.zeros((len(args.noise),
                    len(args.num_proj_used),
                    len(args.reco_type),
                    args.num_steps,
                    args.num_itr),
                   dtype=np.float32)
    reconstruction_error_n = np.zeros((len(args.noise),
                                     len(args.num_proj_used),
                                     len(args.reco_type),
                                     args.num_steps,
                                     args.num_itr),
                                    dtype=np.float32)
    ncp_n = np.zeros((len(args.noise),
                    len(args.num_proj_used),
                    len(args.reco_type),
                    args.num_steps,
                    args.num_itr),
                   dtype=np.float32)
    iterations_used = np.zeros((len(args.noise),
                    len(args.num_proj_used),
                    len(args.reco_type),
                    args.num_steps),
                   dtype=np.uint16)
    for iNoise, noise in enumerate(args.noise):
        for iProj, num_proj_used in enumerate(args.num_proj_used):
            for iReco, reco_type in enumerate(args.reco_type):
                file_path = '/home/pwra/simulation/output_files/noise{}/num_proj_used{}/reco_type{}/'.format(noise, num_proj_used, reco_type)
                file_name = 'reconstruction_error_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}_NumTimeSteps{}.npy'.format(noise, num_proj_used, reco_type, args.num_itr, args.num_steps)
                reconstruction_error[iNoise,iProj,iReco] = np.load(file_path+file_name)
                reconstruction_error_n[iNoise,iProj,iReco] = reconstruction_error[iNoise,iProj,iReco]/reconstruction_error[iNoise,iProj,iReco].max()
                file_name = 'ncp_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}_NumTimeSteps{}.npy'.format(noise, num_proj_used, reco_type, args.num_itr, args.num_steps)
                ncp[iNoise,iProj,iReco] = np.load(file_path+file_name)
                ncp_n[iNoise,iProj,iReco] = ncp[iNoise,iProj,iReco]/ncp[iNoise,iProj,iReco].max()
                file_name = 'dynamic_reconstruction_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}_iterations_used.npy'.format(noise, num_proj_used, reco_type, 'NCP')
                iterations_used[iNoise,iProj,iReco] = np.load(file_path+file_name)[:args.num_steps]

    # noise, num_proj_used, reco_type = '0.25', '360', '1'
    # file_path = '/home/pwra/simulation/output_files/noise{}/num_proj_used{}/reco_type{}/'.format(noise, num_proj_used, reco_type)
    # file_name = 'reconstruction_error_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}_NumTimeSteps{}.npy'.format(noise, num_proj_used, reco_type, 615, args.num_steps)
    # reconstruction_error_hack = np.load(file_path+file_name)
    # file_name = 'ncp_vec_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}_NumTimeSteps{}'.format(args.noise, args.num_proj_used, args.reco_type, args.num_itr, args.num_steps)
    # c = np.save(file_path+file_name)
    # %%
    plt.close('all')
    for iTime in range(args.num_steps):
        for iReco, reco_type in enumerate(args.reco_type):
            fig, ax = plt.subplots(3,3, squeeze=False, figsize=(25,25))
            for iProj, num_proj_used in enumerate(args.num_proj_used):
                for iNoise, noise in enumerate(args.noise):
                    tmp_ncp = ncp[iNoise,iProj,iReco,iTime]
                    tmp_res = reconstruction_error[iNoise,iProj,iReco,iTime]
                    
                    true_min = np.argmin(tmp_res)
                    local_min = np.argwhere(np.r_[True, tmp_ncp[1:] < tmp_ncp[:-1]] &
                                            np.r_[tmp_ncp[:-1] < tmp_ncp[1:], True])[:,0]
                    local_max = np.argwhere(np.r_[True, tmp_ncp[1:] > tmp_ncp[:-1]] &
                                            np.r_[tmp_ncp[:-1] > tmp_ncp[1:], True])[:,0]
                    ax[iNoise,iProj].plot((tmp_res/tmp_res.max()),
                                          label='$|x^{(k)} - \\bar{x}|_2$')
                    label_name = 'min($|x^{(k)} - \\bar{x}|_2$)'
                    ax[iNoise,iProj].scatter(true_min, tmp_res[true_min]/tmp_res.max(),
                                             label='{} = {}'.format(label_name,true_min))
                    ax[iNoise,iProj].plot((tmp_ncp/tmp_ncp.max()),
                                          label='NCP')
                    
                    label_name = ', '.join(map(str, local_min))
                    ax[iNoise,iProj].scatter(local_min, tmp_ncp[local_min]/tmp_ncp.max(),
                                             label='min(NCP) = {}'.format(label_name))
                    label_name = ', '.join(map(str, local_max))
                    ax[iNoise,iProj].scatter(local_max, tmp_ncp[local_max]/tmp_ncp.max(),
                                             label='max(NCP) = {}'.format(label_name))
                    
                    ax[iNoise,iProj].set_title('$\\rho$ = {}%. {} = {}.'.format(noise, '$N_{\\mathrm{proj}}$', num_proj_used))
                    ax[iNoise,iProj].set_xlabel('Iteration number')
                    ax[iNoise,iProj].set_ylabel('Arbitrary units')
                    ax[iNoise,iProj].grid()
                    ax[iNoise,iProj].legend(loc='best', ncol=1)
            fig.tight_layout(rect=[0, 0, 1, 1], w_pad=2.0, h_pad=2.0)
            file_name = 'stopping_rule_RecoType{}_TimeStep{}.pdf'.format(reco_type,iTime)
            fig.savefig(file_path_supplementary+file_name)
            plt.close()
    # %%
    def update_data(val):
        iStep = int(sStep.val)
        reco_type = int(sReco.val)
        for iNoise, noise in enumerate(args.noise):
            for iProj, num_proj_used in enumerate(args.num_proj_used):
                # for iReco, reco_type in enumerate(args.reco_type[:1]):
                    # iCount = (iReco+
                    #           iNoise*len(args.reco_type[:1])+
                    #           iProj*len(args.reco_type[:1])*len(args.noise))
                iCount = iProj+iNoise*len(args.num_proj_used)
                tmp_ncp = ncp[iNoise,iProj,reco_type-1,iStep]
                tmp_res = reconstruction_error[iNoise,iProj,reco_type-1,iStep]
                true_min = np.argmin(tmp_res)
                local_min = np.argwhere(np.r_[True, tmp_ncp[1:] < tmp_ncp[:-1]] &
                                        np.r_[tmp_ncp[:-1] < tmp_ncp[1:], True])[:,0]
                local_max = np.argwhere(np.r_[True, tmp_ncp[1:] > tmp_ncp[:-1]] &
                                        np.r_[tmp_ncp[:-1] > tmp_ncp[1:], True])[:,0]
    
                a[iCount][0].set_ydata(tmp_res)
                c[iCount].set_offsets(np.vstack((true_min, tmp_res[true_min])).T)
                b[iCount][0].set_ydata(tmp_ncp)
                d[iCount].set_offsets(np.vstack((local_min, tmp_ncp[local_min])).T)
                e[iCount].set_offsets(np.vstack((local_max, tmp_ncp[local_max])).T)
                ax[iNoise,iProj].set_ylim(0.9*tmp_res.min(),
                                            1.1*tmp_res.max())
                ax2[iNoise,iProj].set_ylim(0.9*tmp_ncp.min(),
                                            1.1*tmp_ncp.max())
                # ax[iNoise,iProj].set_ylim(0.9*min(tmp_res.min(), tmp_ncp.min()),
                #                             1.1*max(tmp_res.max(), tmp_ncp.max()))
                
                a[iCount][0].set_color(RGB[reco_type])
                b[iCount][0].set_color(RGB[reco_type])
                
                d[iCount].set_color(RGB[reco_type])
                e[iCount].set_color(RGB[reco_type])
    
    plt.close('all')
    reco_type = 1
    a, b, c, d, e = [], [], [], [], []
    ax2 = []
    fig, ax = plt.subplots(3,3, squeeze=False, figsize=(21,15))
    for iNoise, noise in enumerate(args.noise):
        for iProj, num_proj_used in enumerate(args.num_proj_used):
            tmp_res = reconstruction_error[iNoise,iProj,reco_type-1,0]
            
            true_min = np.argmin(tmp_res)            
            a.append(ax[iNoise,iProj].plot((tmp_res), label='$|x^{(k)} - \\bar{x}|_2$',
                                  linestyle=linestyle_tuple[0][1],
                                  color=RGB[reco_type], zorder=1))
            label_name = 'min($|x^{(k)} - \\bar{x}|_2$)'
            c.append(ax[iNoise,iProj].scatter(true_min, tmp_res[true_min],
                                      label='{} = {}'.format(label_name,true_min),
                                      color='lime', marker='*', zorder=2))
            
            ax2.append(ax[iNoise,iProj].twinx())
    ax2 = np.array(ax2).reshape(ax.shape)
    
    for iNoise, noise in enumerate(args.noise):
        for iProj, num_proj_used in enumerate(args.num_proj_used):
            tmp_ncp = ncp[iNoise,iProj,reco_type-1,0]
            
            local_min = np.argwhere(np.r_[True, tmp_ncp[1:] < tmp_ncp[:-1]] &
                                    np.r_[tmp_ncp[:-1] < tmp_ncp[1:], True])[:,0]
            local_max = np.argwhere(np.r_[True, tmp_ncp[1:] > tmp_ncp[:-1]] &
                                    np.r_[tmp_ncp[:-1] > tmp_ncp[1:], True])[:,0]
            extrema = np.sort(np.concatenate((local_max, local_min)))
            
            b.append(ax2[iNoise,iProj].plot((tmp_ncp), label='NCP',
                                  linestyle=linestyle_tuple[1][1],
                                  color=RGB[reco_type]))
            
            label_name = ', '.join(map(str, extrema))
            d.append(ax2[iNoise,iProj].scatter(local_min, tmp_ncp[local_min],
                                      label='extrema(NCP) = {}'.format(label_name),
                                      color=RGB[reco_type]))
            # label_name = ', '.join(map(str, local_max))
            e.append(ax2[iNoise,iProj].scatter(local_max, tmp_ncp[local_max],
                                      marker='x',
                                      #label='max(NCP) = {}'.format(label_name),
                                      color=RGB[reco_type]))
    
    for iNoise, noise in enumerate(args.noise):
        for iProj, num_proj_used in enumerate(args.num_proj_used):
            ax2[iNoise,iProj].set_yscale('log')
            ax[iNoise,iProj].set_yscale('log')
            # ax[iNoise,iProj].set_ylim(1e1,10e3)
            ax[iNoise,iProj].set_title('$\\rho$ = {}%. {} = {}.'.format(noise, '$N_{\\mathrm{proj}}$', num_proj_used))
            # ax[iNoise,iProj].set_xlabel('Iteration number')
            # ax[iNoise,iProj].set_ylabel('Arbitrary units')
            # ax[iNoise,iProj].legend(loc='upper right', ncol=2, fontsize=12)
            ax[iNoise,iProj].grid()
    fig.tight_layout(rect=[0, 0.04, 1, 1], w_pad=1.5, h_pad=1.5)
    
    axcolor = 'lightgoldenrodyellow'
    ax_step = plt.axes([0.2, 0.035, 0.6, 0.01], facecolor=axcolor)
    ax_reco = plt.axes([0.4, 0.015, 0.20, 0.01], facecolor=axcolor)
    
    sStep = Slider(ax_step, 'Time step', 0, args.num_steps-1, valinit=0, valstep=1)
    sReco = Slider(ax_reco, 'Reco', 1, len(args.reco_type), valinit=1, valstep=1)
    
    sStep.on_changed(update_data)
    sReco.on_changed(update_data)
    # %%
    def update_data(val):
        iItr = int(sItr.val)
        
        for iProj, num_proj_used in enumerate(args.num_proj_used):
            for iNoise, noise in enumerate(args.noise):
                for iReco, reco_type in enumerate(args.reco_type[2:]):
                    iCount = (iReco+
                              iNoise*len(args.reco_type[2:])+
                              iProj*len(args.reco_type[2:])*len(args.noise))
                    
                    tmp_ncp = ncp_n[iNoise,iProj,reco_type-1,iItr]
                    tmp_res = reconstruction_error_n[iNoise,iProj,reco_type-1,iItr]
                    
                    
                    true_min = np.argmin(tmp_res)
                    local_min = np.argwhere(np.r_[True, tmp_ncp[1:] < tmp_ncp[:-1]] &
                                            np.r_[tmp_ncp[:-1] < tmp_ncp[1:], True])[:,0]
                    local_max = np.argwhere(np.r_[True, tmp_ncp[1:] > tmp_ncp[:-1]] &
                                            np.r_[tmp_ncp[:-1] > tmp_ncp[1:], True])[:,0]
        
                    a[iCount][0].set_ydata(tmp_res)
                    c[iCount].set_offsets(np.vstack((true_min, tmp_res[true_min])).T)
                    b[iCount][0].set_ydata(tmp_ncp)
                    d[iCount].set_offsets(np.vstack((local_min, tmp_ncp[local_min])).T)
                    e[iCount].set_offsets(np.vstack((local_max, tmp_ncp[local_max])).T)
                y_min = min(ncp_n[iNoise,iProj,reco_type-1,iItr].min(),
                            ncp_n[iNoise,iProj,reco_type-2,iItr].min(),
                            reconstruction_error_n[iNoise,iProj,reco_type-1,iItr].min(),
                            reconstruction_error_n[iNoise,iProj,reco_type-2,iItr].min())
                y_max = max(ncp_n[iNoise,iProj,reco_type-1,iItr].max(),
                            ncp_n[iNoise,iProj,reco_type-2,iItr].max(),
                            reconstruction_error_n[iNoise,iProj,reco_type-1,iItr].max(),
                            reconstruction_error_n[iNoise,iProj,reco_type-2,iItr].max())
                
                y_min = np.floor(y_min*10000)/10000
                # print(y_min)
                y_max = 1.05
                
                ax[iNoise,iProj].set_ylim(y_min,y_max)
        # fig.canvas.draw_idle()
    
    plt.close('all')
    a, b, c, d, e = [], [], [], [], []
    fig, ax = plt.subplots(3,3, squeeze=False, figsize=(25,25))
    for iProj, num_proj_used in enumerate(args.num_proj_used):
        for iNoise, noise in enumerate(args.noise):
            for iReco, reco_type in enumerate(args.reco_type[2:]):
                tmp_ncp = ncp_n[iNoise,iProj,reco_type-1,0]
                tmp_res = reconstruction_error_n[iNoise,iProj,reco_type-1,0]
                
                true_min = np.argmin(tmp_res)
                local_min = np.argwhere(np.r_[True, tmp_ncp[1:] < tmp_ncp[:-1]] &
                                        np.r_[tmp_ncp[:-1] < tmp_ncp[1:], True])[:,0]
                local_max = np.argwhere(np.r_[True, tmp_ncp[1:] > tmp_ncp[:-1]] &
                                        np.r_[tmp_ncp[:-1] > tmp_ncp[1:], True])[:,0]
                extrema = np.sort(np.concatenate((local_max, local_min)))
                
                a.append(ax[iNoise,iProj].plot((tmp_res), label='$|x^{(k)} - \\bar{x}|_2$',
                                      linestyle=linestyle_tuple[0][1],
                                      color=RGB[reco_type]))
                label_name = 'min($|x^{(k)} - \\bar{x}|_2$)'
                c.append(ax[iNoise,iProj].scatter(true_min, tmp_res[true_min],
                                          label='{} = {}'.format(label_name,true_min),
                                          color=RGB[reco_type]))
                b.append(ax[iNoise,iProj].plot((tmp_ncp), label='NCP',
                                      linestyle=linestyle_tuple[1][1],
                                      color=RGB[reco_type]))
                
                label_name = ', '.join(map(str, extrema))
                d.append(ax[iNoise,iProj].scatter(local_min, tmp_ncp[local_min],
                                          label='extrema(NCP) = {}'.format(label_name),
                                          color=RGB[reco_type]))
                # label_name = ', '.join(map(str, local_max))
                e.append(ax[iNoise,iProj].scatter(local_max, tmp_ncp[local_max],
                                          marker='x',
                                          #label='max(NCP) = {}'.format(label_name),
                                          color=RGB[reco_type]))
            ax[iNoise,iProj].set_yscale('log')
            # ax[iNoise,iProj].set_ylim(1e-2,1e1)
            ax[iNoise,iProj].set_title('$\\rho$ = {}%. {} = {}.'.format(noise, '$N_{\\mathrm{proj}}$', num_proj_used))
            # ax[iNoise,iProj].set_xlabel('Iteration number')
            # ax[iNoise,iProj].set_ylabel('Arbitrary units')
            # ax[iNoise,iProj].legend(loc='upper right', ncol=2, fontsize=12)
            ax[iNoise,iProj].grid()
    fig.tight_layout(rect=[0, 0.04, 1, 1], w_pad=2.0, h_pad=2.0)
    
    axcolor = 'lightgoldenrodyellow'
    ax_iteration = plt.axes([0.2, 0.025, 0.6, 0.02], facecolor=axcolor)
    sItr = Slider(ax_iteration, 'Iteration', 0, args.num_steps-1, valinit=0, valstep=1)
    sItr.on_changed(update_data)
    # %%
    reconstruction_error_min = np.min(reconstruction_error, axis=(-1))
    reconstruction_error_argmin = np.argmin(reconstruction_error, axis=(-1))
    iteration_difference = iterations_used - reconstruction_error_argmin
    # iterations_used_trunc = np.copy(iterations_used)
    # iterations_used_trunc[iterations_used_trunc > 499] = 499
    tmp = np.zeros_like(reconstruction_error, dtype=bool)
    for iReco, reco_type in enumerate(args.reco_type):
        for iProj, num_proj_used in enumerate(args.num_proj_used):
            for iNoise, noise in enumerate(args.noise):
                for iFrame in range(args.num_steps):
                    tmp[iNoise,iProj,iReco,iFrame,iterations_used[iNoise,iProj,iReco,iFrame]] = True
    l2_difference = reconstruction_error[tmp].reshape(reconstruction_error_min.shape) - reconstruction_error_min
    # l2_difference[0,0,0] = np.diag(reconstruction_error_hack[:,iterations_used[0,0,0,:]]) - reconstruction_error_min[0,0,0]
    # %%
    plt.close('all')
    for iReco, reco_type in enumerate(args.reco_type):
        fig, ax = plt.subplots(3,3, squeeze=False, figsize=(16,16))
        for iProj, num_proj_used in enumerate(args.num_proj_used):
            for iNoise, noise in enumerate(args.noise):
                tmp = iteration_difference[iNoise,iProj,iReco,:]
                ax[iNoise,iProj].plot(tmp, color=RGB[reco_type])
                ax[iNoise,iProj].set_title('$\\rho$ = {}%. {} = {}.'.format(noise, '$N_{\\mathrm{proj}}$', num_proj_used))
                ax[iNoise,iProj].grid()
                ax[iNoise,iProj].set_xlim(-0.5,39.5)
                ax[iNoise,iProj].set_xticks([0,5,10,15,20,25,30,35])
                ax[iNoise,iProj].set_xlabel('Time step in simulation')
                ax[iNoise,iProj].set_ylabel('Iteration difference')
                ax[iNoise,iProj].yaxis.get_major_locator().set_params(integer=True)
        fig.tight_layout(rect=[0, 0, 1, 1], w_pad=2.0, h_pad=2.0)
        file_name = 'iteration_difference_RecoType{}.pdf'.format(reco_type)
        fig.savefig(file_path_supplementary+file_name)
    # %%
    plt.close('all')
    for iReco, reco_type in enumerate(args.reco_type):
        fig, ax = plt.subplots(3,3, squeeze=False, figsize=(16,16))
        for iProj, num_proj_used in enumerate(args.num_proj_used):
            for iNoise, noise in enumerate(args.noise):
                tmp = l2_difference[iNoise,iProj,iReco,:]
                ax[iNoise,iProj].plot(tmp, color=RGB[reco_type])
                ax[iNoise,iProj].set_title('$\\rho$ = {}%. {} = {}.'.format(noise, '$N_{\\mathrm{proj}}$', num_proj_used))
                ax[iNoise,iProj].grid()
                ax[iNoise,iProj].set_xlim(-0.5,39.5)
                ax[iNoise,iProj].set_xticks([0,5,10,15,20,25,30,35])
                ax[iNoise,iProj].set_xlabel('Time step in simulation')
                ax[iNoise,iProj].set_ylabel('$\ell_2$-norm difference')
        fig.tight_layout(rect=[0, 0, 1, 1], w_pad=2.0, h_pad=2.0)
        fig.tight_layout(rect=[0, 0, 1, 1], w_pad=2.0, h_pad=2.0)
        file_name = 'l2norm_difference_RecoType{}.pdf'.format(reco_type)
        fig.savefig(file_path_supplementary+file_name)
    plt.close('all')
    # %%
    plot_names = ['a', 'b']
    iNoise, iProj = 1, 1
    # iReco, reco_type = 0, 1
    
    fig, ax = plt.subplots(2,1, squeeze=False, figsize=(7.5,12.33))
    
    for iReco, reco_type in enumerate(args.reco_type):
        ax[0,0].plot(iteration_difference[iNoise,iProj,iReco,:], color=RGB[reco_type], label=reco_names[reco_type])
    # iReco, reco_type = 3, 4
    # ax[0,0].plot(iteration_difference[iNoise,iProj,iReco,:], color=RGB[reco_type], label=reco_names[reco_type])
    ax[0,0].set_title('$\\rho$ = {}%. {} = {}.'.format(args.noise[iNoise], '$N_{\\mathrm{proj}}$', args.num_proj_used[iProj]))
    ax[0,0].grid()
    # ax[0,0].set_xlim(-0.5,19.5)
    # ax[0,0].set_xticks([0,2,4,6,8,10,12,14,16,18])
    ax[0,0].set_xlabel('Time step in simulation')
    ax[0,0].set_ylabel('Iteration difference')
    ax[0,0].set_ylim([-25, 35])
    ax[0,0].yaxis.get_major_locator().set_params(integer=True)
    
    # iReco, reco_type = 0, 1
    for iReco, reco_type in enumerate(args.reco_type):
        ax[1,0].plot(l2_difference[iNoise,iProj,iReco,:], color=RGB[reco_type], label=reco_names[reco_type])
    # iReco, reco_type = 3, 4
    # ax[1,0].plot(l2_difference[iNoise,iProj,iReco,:], color=RGB[reco_type], label=reco_names[reco_type])
    # ax[1,0].set_title('$\\rho$ = {}%. {} = {}.'.format('1.00', '$N_{\\mathrm{proj}}$', 120))
    ax[1,0].grid()
    # ax[1,0].set_xlim(-0.5,19.5)
    # ax[1,0].set_xticks([0,2,4,6,8,10,12,14,16,18])
    ax[1,0].set_xlabel('Time step in simulation')
    ax[1,0].set_ylabel('$\ell_2$-norm difference')
    ax[1,0].set_ylim([-1, 15])
    ax[0,0].legend(loc='best')
    fig.tight_layout(rect=[0, 0, 1, 1], w_pad=2.0, h_pad=2.0)
    
    for iRow in range(ax.shape[0]):
        ax[iRow,0].text(0.975, 0.975, plot_names[iRow],
                        horizontalalignment='right',
                        verticalalignment='top',
                        fontweight='bold',
                        fontsize=BIGGER_SIZE,
                        transform=ax[iRow,0].transAxes)
    
    file_name = 'ncp_evaluation.pdf'
    fig.savefig(file_path_paper+file_name)
    # %%
    plot_names = ['a', 'b']
    iNoise, iProj = 1, 1
    # iReco, reco_type = 0, 1
    
    fig, ax = plt.subplots(1,2, squeeze=False, figsize=(12.33,7))
    for iReco, reco_type in enumerate(args.reco_type):
        ax[0,0].plot(iteration_difference[iNoise,iProj,iReco,:], color=RGB[reco_type], label=reco_names[reco_type])
    # iReco, reco_type = 3, 4
    # ax[0,0].plot(iteration_difference[iNoise,iProj,iReco,:], color=RGB[reco_type], label=reco_names[reco_type])
    ax[0,0].set_title('$\\rho$ = {}%. {} = {}.'.format(args.noise[iNoise], '$N_{\\mathrm{proj}}$', args.num_proj_used[iProj]))
    ax[0,0].grid()
    # ax[0,0].set_xlim(-0.5,19.5)
    # ax[0,0].set_xticks([0,2,4,6,8,10,12,14,16,18])
    ax[0,0].set_xlabel('Time step in simulation')
    ax[0,0].set_ylabel('Iteration difference')
    ax[0,0].set_ylim([-25, 35])
    ax[0,0].yaxis.get_major_locator().set_params(integer=True)
    
    # iReco, reco_type = 0, 1
    for iReco, reco_type in enumerate(args.reco_type):
        ax[0,1].plot(l2_difference[iNoise,iProj,iReco,:], color=RGB[reco_type], label=reco_names[reco_type])
    # iReco, reco_type = 3, 4
    # ax[0,1].plot(l2_difference[iNoise,iProj,iReco,:], color=RGB[reco_type], label=reco_names[reco_type])
    ax[0,1].set_title('$\\rho$ = {}%. {} = {}.'.format('1.00', '$N_{\\mathrm{proj}}$', 120))
    ax[0,1].grid()
    # ax[0,1].set_xlim(-0.5,19.5)
    # ax[0,1].set_xticks([0,2,4,6,8,10,12,14,16,18])
    ax[0,1].set_xlabel('Time step in simulation')
    ax[0,1].set_ylabel('$\ell_2$-norm difference')
    ax[0,1].set_ylim([-1, 15])
    ax[0,0].legend(loc='best')
    fig.tight_layout(rect=[0, 0, 1, 1], w_pad=2.0, h_pad=2.0)
    
    for iCol in range(ax.shape[1]):
        ax[0,iCol].text(0.975, 0.975, plot_names[iCol],
                        horizontalalignment='right',
                        verticalalignment='top',
                        fontweight='bold',
                        fontsize=BIGGER_SIZE,
                        transform=ax[0,iCol].transAxes)
    
    file_name = 'ncp_evaluation_column.pdf'
    fig.savefig(file_path_paper+file_name)
    # %%
if __name__ == '__main__':
    # Construct the argument parser
    parser = argparse.ArgumentParser()
    
    # Add the arguments to the parser
    parser.add_argument("-num_proj_used", "--num_proj_used", required=False,
                        nargs='?', default=[360, 120, 45],
                        type=lambda s: [item for item in s.split(',')], help="first operand")
    
    parser.add_argument("-noise", "--noise", required=False,
                        nargs='?', default=['0.25', '1.00', '5.00'],
                        type=lambda s: [item for item in s.split(',')], help="second operand")
    
    parser.add_argument("-reco_type", "--reco_type", required=False,
                        nargs='?', default=[1, 2, 3, 4],
                        type=lambda s: [item for item in s.split(',')], help="third operand")
    
    parser.add_argument("-num_itr", "--num_itr", required=False,
                        nargs='?', default=700, type=int, help="fifth operand")
    
    parser.add_argument("-num_steps", "--num_steps", required=False,
                        nargs='?', default=40, type=int, help="fifth operand")
    
    args = parser.parse_args()
    
    # if args.reco_type not in (1,2):
        # raise ValueError('Wrong reco method used!')
    # %%
    main(args)