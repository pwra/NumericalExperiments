#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 13 14:16:00 2020

@author: Peter Winkel Rasmussen
"""
import os
os.chdir('/home/pwra/simulation/')
import argparse
import time
import numpy as np
# import cupy as cp
import astra
import auxiliary_functions as af
import constants as const
from skimage.draw import circle
import matplotlib.pyplot as plt
import matplotlib.style as style
from matplotlib.widgets import Slider
style.use('seaborn')
style.use('ggplot')
# import matplotlib.animation as animation
from mpl_toolkits.axes_grid1 import make_axes_locatable
params = {'axes.grid': False, 'savefig.dpi': 200,
          'text.color': 'black',
          'axes.labelcolor': 'black',
          'xtick.color': 'black',
          'ytick.color': 'black',
          'lines.linewidth': 4,
          'patch.linewidth': 3,
          'lines.markersize': 10}
plt.rcParams.update(params)

SMALL_SIZE = 13+6
MEDIUM_SIZE = 15+6
BIGGER_SIZE = 17+6

plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=MEDIUM_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title

# mempool = cp.get_default_memory_pool()
# %%
def main(args):
    # %%
    astra.set_gpu_index(args.gpu)
    num_proj = args.num_proj
    noise = args.noise
    # %%
    file_path = '/home/pwra/simulation/output_files/'
    file_name = 'simulation_projections_Noise{}_NumProj{}.npy'.format(noise, num_proj)
    projections = np.load(file_path+file_name)
    
    angle_shift = int(np.round(const.angle_shift/(np.pi/args.num_proj)))
    projections = np.roll(projections, shift=angle_shift, axis=2)
    projections[:,:,:angle_shift,:] = np.flip(projections[:,:,:angle_shift,:], axis=-1)
    
    num_frame, num_slice, num_projection, num_pixel = projections.shape
    
    file_name = 'simulation_attenuation_rotated.npy'
    ground_truth = np.load(file_path+file_name)    
    
    file_name = 'static_reconstruction_Noise{}_NumProj{}_RecoType{}_NumItr{}.npy'.format('0.25', 720, 2, 'NCP')
    reconstruction_static = np.load(file_path+file_name)
    # %%
    if args.reco_type == 4:
        rr, cc = circle(num_slice//2-1, num_slice//2-1, 124)
        mask_inner = np.zeros((num_slice, num_slice), dtype='bool')
        mask_inner[rr, cc] = True
        mask_inner_rock = np.tile(mask_inner, (256, 1, 1))
        
        static_mask = reconstruction_static[0] >= 2.4
        
        dynamic_mask = np.logical_and(reconstruction_static[0] < 1.83, reconstruction_static[0] > 0.0)
        dynamic_mask = np.logical_and(dynamic_mask, mask_inner_rock)
    # %%
    angles = np.linspace(0,np.pi,args.num_proj_used, endpoint=False)
    proj_geom = astra.create_proj_geom('parallel3d', 1, 1, num_slice, num_pixel, angles);
    vol_geom = astra.create_vol_geom(num_slice, num_slice, num_slice) # (rows, columns, slices)

    reconstruction = np.zeros((args.num_steps, num_slice, num_slice, num_slice), dtype=np.float32)
    # reconstruction_GT = np.zeros((num_frame, num_slice, num_slice, num_slice), dtype=np.float32)
    # reconstruction_NumItr = np.zeros((num_frame, num_slice, num_slice, num_slice), dtype=np.float32)
    residual = np.zeros((args.num_steps, args.num_itr), dtype=np.float32)
    ncp = np.zeros((args.num_steps, args.num_itr), dtype=np.float32)
    # t_est = np.zeros((num_frame, args.num_itr), dtype=np.float32)
    # GCV = np.zeros((num_frame, args.num_itr), dtype=np.float32)
    fit_error = np.zeros((args.num_steps, args.num_itr), dtype=np.float32)
    
    # w_bar = np.random.randn(num_slice, num_projection, num_pixel)
    # z = af.backward_sino(vol_geom, proj_geom, w_bar).ravel()
    
    rec = np.copy(reconstruction_static[0])
    # time_steps = [0,1,3]
    # c = np.zeros((len(time_steps),args.num_itr, projections[0].size//2), dtype=np.float32)
    c = []
    # %
    # for iterator, iFrame in enumerate(time_steps):
    for iFrame in range(args.num_steps):
        print('{}. Reconstructing time step {} out of {} steps'.format(time.asctime(), iFrame, num_frame))
        sinograms = projections[iFrame,:,::num_proj//args.num_proj_used,:]
        sinograms_id = astra.data3d.create('-sino', proj_geom, sinograms)
        # w_id = astra.data3d.create('-sino', proj_geom, w_bar)
    
        alg_id, rec_id = af.setup_reco_alg(args.reco_type, vol_geom, sinograms_id, rec)
        # alg_w_id, rec_w_id = af.setup_reco_alg(2, vol_geom, w_id, 0)
        
        b = sinograms.ravel()
        # mempool.free_all_blocks()
        rec_npc = np.zeros((5, num_slice, num_slice, num_slice), dtype=np.float32)
        ncp_tmp = np.zeros((5), dtype=np.float32)
        iItr = 0
        ncp_idx = 0
        idx_min = 0
        
        first_min = True
        last_min = 1e6

        ## %% Reconstruction
        while iItr < args.num_itr:
            # print('{}. Iteration number {}'.format(time.asctime(), iItr))
            # 257 ms
            astra.algorithm.run(alg_id, 1)
            # print('After run')
            # astra.algorithm.run(alg_w_id, 1)
            
            rec_npc[ncp_idx] = astra.data3d.get(rec_id)
            # xi = astra.data3d.get(rec_w_id)
            # t_est[iterator, iItr] = np.matmul(z.T,xi.ravel())
            fit_error[iFrame, iItr] = astra.algorithm.get_res_norm(alg_id)
            # GCV[iterator, iItr] = (astra.algorithm.get_res_norm(alg_id)**2/
            #                        (b.shape[0]-t_est[iterator, iItr])**2)
            if args.reco_type == 4:
                rec[static_mask] = const.rock_attenuation
                rec[np.logical_and(dynamic_mask, rec > const.water_attenuation)] = const.water_attenuation
                rec[np.logical_and(dynamic_mask, rec < const.oil_attenuation)] = const.oil_attenuation
                rec_npc[ncp_idx] = rec

            residual[iFrame, iItr] = np.linalg.norm(rec_npc[ncp_idx]-ground_truth[iFrame])
            # res_norm_tmp[gt_idx] = residual[iterator, iItr]
            
            Ax = af.forward_reco(vol_geom, proj_geom, rec_npc[ncp_idx]).ravel()
            astra.set_gpu_index(args.gpu)
            
            c_tmp, ncp_tmp[ncp_idx] = af.cuda_ncp(b, Ax, args.gpu[0])
            ncp[iFrame, iItr] = ncp_tmp[ncp_idx]

            if args.save_ncp_vec:
                c.append(c_tmp[::1000])
            # reconstruction_NumItr[iterator] = rec_npc[-1]
            
            if iItr >= 4:
                if np.sum(ncp_tmp[idx_min] < ncp_tmp) == 4:
                    # print('\tNCP increasing!')
                    if first_min:
                        # num_minima += 1
                        first_min = False
                        reconstruction[iFrame] = rec_npc[idx_min]
                        last_min = ncp_tmp[idx_min]
                    # itr_beyond += 1
                    # if max_itr_beyond < itr_beyond:
                        # break
                        
                # if np.sum(ncp_tmp[idx_min] > ncp_tmp) == 4:
                #     print('\tNCP decreasing!')

                if ncp_tmp.min() == ncp_tmp[2] and ncp_tmp[2] < last_min:
                    # print('\tNCP True minima!')
                    last_min = ncp_tmp[2]
                    reconstruction[iFrame] = rec_npc[2]
                    # num_minima += 1
                    # if max_min <= num_minima:
                    #     break
                    # if first_min:
                    #     max_itr_beyond = max(max_itr_beyond, iItr)
                    #     first_min = False
            
            
            iItr += 1
            if ncp_idx < 4:
                ncp_idx += 1
            else:
                ncp_tmp = np.roll(ncp_tmp, -1)
                rec_npc = np.roll(rec_npc, -1, axis=0)
        ## Clean up.
        # astra.algorithm.delete(alg_w_id)
        # astra.data3d.delete(rec_w_id)
        # astra.data3d.delete(w_id)
        astra.algorithm.delete(alg_id)
        astra.data3d.delete(rec_id)
        astra.data3d.delete(sinograms_id)
        rec = np.copy(reconstruction[iFrame])
    print('{}. Time of Completion'.format(time.asctime()))
    # %%
    file_path = '/home/pwra/simulation/output_files/noise{}/num_proj_used{}/reco_type{}/'.format(noise, args.num_proj_used, args.reco_type)
    file_name = 'reconstruction_error_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}_NumTimeSteps{}'.format(noise, args.num_proj_used, args.reco_type, args.num_itr, args.num_steps)
    np.save(file_path+file_name, residual)
    
    file_name = 'ncp_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}_NumTimeSteps{}'.format(noise, args.num_proj_used, args.reco_type, args.num_itr, args.num_steps)
    np.save(file_path+file_name, ncp)
    
    if args.save_ncp_vec:
        c = np.array(c)
        c = c.reshape((args.num_steps, args.num_itr, c_tmp[::1000].size))
        file_name = 'ncp_vec_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}_NumTimeSteps{}'.format(noise, args.num_proj_used, args.reco_type, args.num_itr, args.num_steps)
        np.save(file_path+file_name, c)
    # %%
    # # m = b.shape[0]
    # true_min = np.argmin(residual, axis=1)
    # ncp_min = np.argmin(ncp, axis=1)
    # # gcv_min = np.argmin(GCV, axis=1)
    # # min_val = np.min([residual.min(), ncp.min(), GCV.min()])-10
    # # max_val = np.max([residual.max(), ncp.max(), GCV.max()])+10
    
    # fig, ax = plt.subplots(1,1,squeeze=False,figsize=(10,10))
    
    # ax[0,0].plot(residual[0, :], '-', label='$|x^{(k)} - \\bar{x}|_2$')
    # label_name = 'min($|x^{(k)} - \\bar{x}|_2$)'
    # ax[0,0].scatter(true_min[0], residual[0, true_min[0]], label='{} = {}'.format(label_name,true_min[0]))
    # ax[0,0].grid()
    # ax[0,0].set_xlabel('Iteration number')
    # ax[0,0].set_ylabel('Arbitrary units')
    # # x_pos, y_pos = 0.775, 0.625-0.1
    # # ax[0,0].legend(loc='center', bbox_to_anchor=(x_pos,y_pos), ncol=1)
    # fig.tight_layout(rect=[0, 0, 1, 1])
    # file_path_plots = '/home/pwra/simulation/summer_school_plots/'
    # # fig.savefig(file_path_plots+'GT_Noise{}_NumProj{}_RecoType{}.pdf'.format(noise, num_projection, args.reco_type))
    
    # ax[0,0].plot(ncp[0, :], '-', label='NCP')
    # ax[0,0].scatter(ncp_min[0], ncp[0, ncp_min[0]], label='min(NCP) = {}'.format(ncp_min[0]))
    # # ax[0,0].legend(loc='center', bbox_to_anchor=(x_pos,y_pos), ncol=1)
    # # fig.savefig(file_path_plots+'NCP_Noise{}_NumProj{}_RecoType{}.pdf'.format(noise, num_projection, args.reco_type))
    
    # # ax[0,0].plot(GCV[0, :], '-', label='GCV')
    # # ax[0,0].scatter(gcv_min[0], GCV[0, gcv_min[0]], label='min(GCV) = {}'.format(gcv_min[0]))
    
    # ax[0,0].plot(fit_error[0, :], '-', label='$|Ax-b|_2$')
    # # ax[0,0].plot(np.arange(args.num_itr), 3.48**2*np.ones((args.num_itr, 1)), '-', label='$\\sigma\sqrt{m}$')
    # ax[0,0].set_yscale('log')
    # # ax[0,0].legend(loc='center', bbox_to_anchor=(x_pos,y_pos), ncol=1)
    # ax[0,0].legend(loc='best')
    # fig.savefig(file_path_plots+'GCV_Noise{}_NumProj{}_RecoType{}.pdf'.format(noise, num_projection, args.reco_type))
    # %
    # for i in range(len(time_steps)):
    #     # m = b.shape[0]
    #     true_min = np.argmin(residual[i,None], axis=1)
    #     # ncp_min = np.argmin(ncp[i,None], axis=1)
    #     local_min = np.argwhere(np.r_[True, ncp[i][1:] < ncp[i][:-1]] & np.r_[ncp[i][:-1] < ncp[i][1:], True])[:,0]
    #     local_max = np.argwhere(np.r_[True, ncp[i][1:] > ncp[i][:-1]] & np.r_[ncp[i][:-1] > ncp[i][1:], True])[:,0]
    #     # gcv_min = np.argmin(GCV[i,None], axis=1)
    #     # min_val = np.min([residual.min(), ncp.min(), GCV.min()])-10
    #     # max_val = np.max([residual.max(), ncp.max(), GCV.max()])+10
        
    #     fig, ax = plt.subplots(1,1,squeeze=False,figsize=(10,10))
        
    #     ax[0,0].plot(residual[i, :]/residual[i, :].max(), '-', label='$|x^{(k)} - \\bar{x}|_2$')
    #     label_name = 'min($|x^{(k)} - \\bar{x}|_2$)'
    #     ax[0,0].scatter(true_min[0], residual[i, true_min[0]]/residual[i, :].max(), label='{} = {}'.format(label_name,true_min[0]))
    #     ax[0,0].grid()
    #     ax[0,0].set_xlabel('Iteration number')
    #     ax[0,0].set_ylabel('Arbitrary units')
    #     # x_pos, y_pos = 0.775, 0.625-0.1
    #     # ax[0,0].legend(loc='center', bbox_to_anchor=(x_pos,y_pos), ncol=1)
    #     fig.tight_layout(rect=[0, 0, 1, 1])
    #     # file_path_plots = '/home/pwra/simulation/summer_school_plots/'
    #     # fig.savefig(file_path_plots+'GT_Noise{}_NumProj{}_RecoType{}.pdf'.format(noise, num_projection, args.reco_type))
        
    #     ax[0,0].plot(ncp[i, :]/ncp[i, :].max(), '-', label='NCP')
    #     label_name = ', '.join(map(str, local_min))
    #     ax[0,0].scatter(local_min, ncp[i, local_min]/ncp[i, :].max(), label='min(NCP) = {}'.format(label_name))
    #     # ax[0,0].scatter(ncp_min[0], ncp[i, ncp_min[0]]/ncp[i, :].max(), label='min(NCP) = {}'.format(ncp_min[0]))
    #     # ax[0,0].legend(loc='center', bbox_to_anchor=(x_pos,y_pos), ncol=1)
    #     # fig.savefig(file_path_plots+'NCP_Noise{}_NumProj{}_RecoType{}.pdf'.format(noise, num_projection, args.reco_type))
        
    #     # ax[0,0].plot(GCV[i, :]/GCV[i, :].max(), '-', label='GCV')
    #     # ax[0,0].scatter(gcv_min[0], GCV[i, gcv_min[0]]/GCV[i, :].max(), label='min(GCV) = {}'.format(gcv_min[0]))
        
    #     ax[0,0].plot(fit_error[i, :]/fit_error[0, :].max(), '-', label='$|Ax-b|_2$')
    #     label_name = ', '.join(map(str, local_max))
    #     ax[0,0].scatter(local_max, ncp[i, local_max]/ncp[i, :].max(), color='k', label='max(NCP) = {}'.format(label_name))
    #     # ax[0,0].plot(np.arange(args.num_itr), 3.48**2*np.ones((args.num_itr, 1)), '-', label='$\\sigma\sqrt{m}$')
    #     # ax[0,0].set_yscale('log')
    #     ax[0,0].legend(loc='best', ncol=1)
        # fig.savefig(file_path_plots+'GCV_norm_Noise{}_NumProj{}_RecoType{}.pdf'.format(noise, num_projection, args.reco_type))
    # %%
    # def update_data(val):
    #     iItr = int(sItr.val)
    #     ax[0,0].plot(c[0,iItr])
    
    # fig, ax = plt.subplots(1,1,squeeze=False,figsize=(10,10))
    # ax[0,0].plot(c[0,0])
    # ax[0,0].grid()
    # axcolor = 'lightgoldenrodyellow'
    
    # ax_iteration = plt.axes([0.2, 0.025, 0.6, 0.02], facecolor=axcolor)
    # sItr = Slider(ax_iteration, 'Iteration', 0, args.num_itr-1, valinit=0, valstep=1)
    # sItr.on_changed(update_data)
    
    # fig.tight_layout(rect=[0, 0.04, 1, 1])
    # %%    
if __name__ == '__main__':
    # Construct the argument parser
    parser = argparse.ArgumentParser()
    
    # Add the arguments to the parser
    parser.add_argument("-num_proj", "--num_proj", required=False,
                        nargs='?', default=360, type=int, help="first operand")
    
    parser.add_argument("-num_proj_used", "--num_proj_used", required=False,
                        default=45, type=int,
                        help="Number of projections used for reconstruction.")
    
    parser.add_argument("-noise", "--noise", required=False,
                        nargs='?', default='5.00', type=str, help="second operand")
    
    parser.add_argument("-gpu", "--gpu", required=False,
                        nargs='?', default=[0,1],
                        type=lambda s: [int(item) for item in s.split(',')], help="third operand")
    
    parser.add_argument("-use_NCP", "--use_NCP", required=False,
                        nargs='?', default=True, type=bool, help="fourth operand")
    
    parser.add_argument("-num_itr", "--num_itr", required=False,
                        nargs='?', default=500, type=int, help="fifth operand")
    
    parser.add_argument("-reco_type", "--reco_type", required=False,
                        nargs='?', default=2, type=int, help="fifth operand")
    
    parser.add_argument("-num_steps", "--num_steps", required=False,
                        nargs='?', default=10, type=int, help="fifth operand")
    
    parser.add_argument("-save_ncp_vec", "--save_ncp_vec",
                        default=False, type=bool, help="fifth operand")
    
    args = parser.parse_args()
    
    # if args.reco_type not in (1,2):
        # raise ValueError('Wrong reco method used!')
    # %%
    main(args)
