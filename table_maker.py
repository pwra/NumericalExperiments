#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug 28 15:05:29 2020

@author: Peter Winkel Rasmussen
"""

import numpy as np
import argparse
file_path_plots = '/home/pwra/simulation/paper_plots/'

reco_names = {0 : '\hspace{-3pt}FBP',
             1 : '\hspace{-3pt}\sirtA{}',
             2 : '\hspace{-3pt}\sirtB{}',
             3 : '\hspace{-3pt}\sirtC{}',
             4 : '\hspace{-3pt}\sirtD{}'}
def powers_of_ten(a):
    return int(np.log10(a))
def pretty_number(a):
    num_pow = powers_of_ten(a)
    scientific_number = np.round(a/(10.0**num_pow), decimals=2)
    return str(scientific_number)+' \\cdot 10^{{{}}}'.format(num_pow)
# %%
def main(args):
    # %%
    num_proj_used_lst = args.num_proj_used
    noise_lst = args.noise
    reco_type_lst = args.reco_type
    l2_norms = np.zeros((len(num_proj_used_lst),
                         len(noise_lst),
                         len(reco_type_lst)),
                        dtype=np.float32)
    l1_norms = np.zeros((len(num_proj_used_lst),
                         len(noise_lst),
                         len(reco_type_lst)),
                        dtype=np.float32)
    
    # %%
    for iProj, num_proj_used in enumerate(num_proj_used_lst):
        for iNoise, noise in enumerate(noise_lst):
            for iReco, reco_type in enumerate(reco_type_lst):
                if iReco == 0:
                    num_itr = 'None'
                else:
                    num_itr = 'NCP'
                file_path = '/home/pwra/simulation/output_files/noise{}/num_proj_used{}/reco_type{}/'.format(noise, num_proj_used, reco_type)    
                file_name = 'res_l2_norm_4d_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}.npy'.format(noise, num_proj_used, reco_type, num_itr)
                l2_norms[iProj,iNoise,iReco] = np.load(file_path+file_name)
                file_name = 'res_l1_norm_4d_Noise{}_NumProjUsed{}_RecoType{}_NumItr{}.npy'.format(noise, num_proj_used, reco_type, num_itr)
                l1_norms[iProj,iNoise,iReco] = np.load(file_path+file_name)
    # %%
    min_vals = [np.min(l1_norms[0,0,:]),np.min(l2_norms[0,0,:]),
                np.min(l1_norms[2,2,:]),np.min(l2_norms[2,2,:])]
    # %%
    powers_of_tens = []
    for iReco, reco_type in enumerate(reco_type_lst):
        powers_of_tens.append([powers_of_ten(l1_norms[0,0,reco_type]),
                               powers_of_ten(l2_norms[0,0,reco_type]),
                               powers_of_ten(l1_norms[2,2,reco_type]),
                               powers_of_ten(l2_norms[2,2,reco_type])])
    powers_of_tens = np.array(powers_of_tens, dtype=int)
    # %% Extreme _ new
    f = open(file_path_plots+'performance_table2.tex', 'w+')
    f.write('\\begin{table}'+'\n')
    f.write('\t'+'\\centering'+'\n')
    f.write('\t'+'\\begin{tabular}{B*{4}{D}}'+'\n')
    f.write('\t\t'+'\\toprule'+'\n')
    table_headers = ['\\thead{$N_{\mathrm{proj}}=360$\\\\$\\rho = 0.25\%$}',
                     '\\thead{$N_{\mathrm{proj}}=45$\\\\$\\rho = 5.00\%$}']
    f.write('\t\t'+'& \\multicolumn{2}{l}{'+table_headers[0]+'} & \\multicolumn{2}{l}{'+table_headers[1]+'} \\\\'+'\n')
    f.write('\t\t'+'\\midrule'+'\n')
    tmp_str = ('& $\\ell_1 \\left(\cdot 10^{{{:d}}}\\right)$ & '.format(powers_of_tens[:,0].min()) +
               '$\\ell_2 \\left(\cdot 10^{{{:d}}}\\right)$ & '.format(powers_of_tens[:,1].min()) +
               '$\\ell_1 \\left(\cdot 10^{{{:d}}}\\right)$ & '.format(powers_of_tens[:,2].min()) +
               '$\\ell_2 \\left(\cdot 10^{{{:d}}}\\right)$ \\\\'.format(powers_of_tens[:,3].min()))
    f.write('\t\t'+tmp_str+'\n')
    f.write('\t\t'+'\\midrule'+'\n')
    for iReco, reco_type in enumerate(reco_type_lst):
        tmp_str = '\t\t'+'{} & {}{{{:.2f}}}$ & {}{{{:.2f}}}$ & {}{{{:.2f}}}$ & {}{{{:.2f}}}$ \\\\'.format(
            reco_names[reco_type],
            '$\\bf' if l1_norms[0,0,reco_type]==min_vals[0] else '$', l1_norms[0,0,reco_type]/10**powers_of_tens[:,0].min(),
            '$\\bf' if l2_norms[0,0,reco_type]==min_vals[1] else '$', l2_norms[0,0,reco_type]/10**powers_of_tens[:,1].min(),
            '$\\bf' if l1_norms[2,2,reco_type]==min_vals[2] else '$', l1_norms[2,2,reco_type]/10**powers_of_tens[:,2].min(),
            '$\\bf' if l2_norms[2,2,reco_type]==min_vals[3] else '$', l2_norms[2,2,reco_type]/10**powers_of_tens[:,3].min())+'\n'
        f.write(tmp_str)
    f.write('\t\t'+'\\bottomrule'+'\n')
    f.write('\t\t'+'\\end{tabular}'+'\n')
    f.write('\t'+'\\caption{Table of the $\\ell_1$- and $\\ell_2$-norms for the best and worst data case. Bold numbers indicate the best performing algorithm.}'+'\n')
    f.write('\t'+'\\label{tab:performance_table}'+'\n')
    f.write('\\end{table}')
    f.close()
    # %% Extreme - Old
    f = open(file_path_plots+'performance_table.tex', 'w+')
    f.write('\\begin{table}'+'\n')
    f.write('\t'+'\\centering'+'\n')
    f.write('\t'+'\\begin{tabular}{B*{4}{D}}'+'\n')
    f.write('\t\t'+'\\toprule'+'\n')
    table_headers = ['\\thead{$N_{\mathrm{proj}}=360$\\\\$\\rho = 0.25\%$}',
                     '\\thead{$N_{\mathrm{proj}}=45$\\\\$\\rho = 5.00\%$}']
    f.write('\t\t'+'& \\multicolumn{2}{l}{'+table_headers[0]+'} & \\multicolumn{2}{l}{'+table_headers[1]+'} \\\\'+'\n')
    f.write('\t\t'+'\\midrule'+'\n')
    f.write('\t\t'+'& $\\ell_1$ & $\\ell_2$ & $\\ell_1$ & $\\ell_2$ \\\\'+'\n')
    f.write('\t\t'+'\\midrule'+'\n')
    for iReco, reco_type in enumerate(reco_type_lst):
        pretty_vals = [pretty_number(l1_norms[0,0,reco_type]),
                       pretty_number(l2_norms[0,0,reco_type]),
                       pretty_number(l1_norms[2,2,reco_type]),
                       pretty_number(l2_norms[2,2,reco_type]),]
        tmp_str = '\t\t'+'{} & {}{{{:s}}}$ & {}{{{:s}}}$ & {}{{{:s}}}$ & {}{{{:s}}}$ \\\\'.format(
            reco_names[reco_type],
            '$\\bf' if l1_norms[0,0,reco_type]==min_vals[0] else '$',pretty_vals[0],
            '$\\bf' if l2_norms[0,0,reco_type]==min_vals[1] else '$',pretty_vals[1],
            '$\\bf' if l1_norms[2,2,reco_type]==min_vals[2] else '$',pretty_vals[2],
            '$\\bf' if l2_norms[2,2,reco_type]==min_vals[3] else '$',pretty_vals[3])+'\n'
        f.write(tmp_str)
    f.write('\t\t'+'\\bottomrule'+'\n')
    f.write('\t\t'+'\\end{tabular}'+'\n')
    f.write('\t'+'\\caption{Table of the $\\ell_1$- and $\\ell_2$-norms for the best and worst data case. Bold numbers indicate the best performing algorithm.}'+'\n')
    f.write('\t'+'\\label{tab:performance_table}'+'\n')
    f.write('\\end{table}')
    f.close()
    # %% All cases, data in columns
    f = open(file_path_plots+'mega_table.tex', 'w+')
    f.write('\\begin{table}'+'\n')
    f.write('\t'+'\\centering'+'\n')
    f.write('\t'+'\\begin{tabular}{B*{18}{D}}'+'\n')
    f.write('\t\t'+'\\toprule'+'\n')
    table_line = ''
    for iProj, num_proj_used in enumerate(num_proj_used_lst):
        for iNoise, noise in enumerate(noise_lst):
                tmp_str = '\\thead{{$N_{{\mathrm{{proj}}}}={}$\\\\$\\rho = {}\%$}}'.format(num_proj_used, noise)
                table_line += '& \\multicolumn{2}{l}{'+tmp_str+'}'
    f.write('\t\t'+table_line+' \\\\'+'\n')
    f.write('\t\t'+'\\midrule'+'\n')
    table_line = ''
    for iProj, num_proj_used in enumerate(num_proj_used_lst):
        for iNoise, noise in enumerate(noise_lst):
                table_line += '& $\\ell_1$ & $\\ell_2$ '
    f.write('\t\t'+table_line+'\\\\'+'\n')
    f.write('\t\t'+'\\midrule'+'\n')
    for iReco, reco_type in enumerate(reco_type_lst):
        table_line = ''
        table_line += reco_names[reco_type]+' '
        for iProj, num_proj_used in enumerate(num_proj_used_lst):
            for iNoise, noise in enumerate(noise_lst):
                min_val = np.min(l1_norms[iProj,iNoise,:])
                tmp_str = pretty_number(l1_norms[iProj,iNoise,reco_type])
                table_line += '& {}{{{:s}}}$ '.format('$\\bf' if l1_norms[iProj,iNoise,reco_type]==min_val else '$',tmp_str)
                min_val = np.min(l2_norms[iProj,iNoise,:])
                tmp_str = pretty_number(l2_norms[iProj,iNoise,reco_type])
                table_line += '& {}{{{:s}}}$ '.format('$\\bf' if l2_norms[iProj,iNoise,reco_type]==min_val else '$',tmp_str)
        f.write('\t\t'+table_line+' \\\\'+'\n')
    f.write('\t\t'+'\\bottomrule'+'\n')
    f.write('\t\t'+'\\end{tabular}'+'\n')

    f.write('\\end{table}')
    f.close()
    # %%
    powers_of_tens = np.zeros((2,3,3,5))
    for iProj, num_proj_used in enumerate(num_proj_used_lst):
        for iNoise, noise in enumerate(noise_lst):
            for iReco, reco_type in enumerate(reco_type_lst):
                powers_of_tens[0,iProj,iNoise,iReco] = powers_of_ten(l1_norms[iProj,iNoise,reco_type])
                powers_of_tens[1,iProj,iNoise,iReco] = powers_of_ten(l2_norms[iProj,iNoise,reco_type])
    powers_of_tens = np.min(powers_of_tens, axis=(-1)).astype('int')
    # %% All cases, data in rows - new
    f = open(file_path_plots+'mega_table_long2.tex', 'w+')
    f.write('\\renewcommand\\theadfont{\\footnotesize}'+'\n')
    f.write('\\renewcommand\\theadalign{ll}'+'\n')
    f.write('\\begin{table*}'+'\n')
    f.write('\t'+'\\centering'+'\n')
    f.write('\t'+'\\begin{tabular}{B*{6}{D}}'+'\n')
    f.write('\t\t'+'\\toprule'+'\n')
    tmp_str = ('Data & Norm & {} & {} & {} & {} & {} \\\\').format(reco_names[0],
                                                                   reco_names[1],
                                                                   reco_names[2],
                                                                   reco_names[3],
                                                                   reco_names[4])
    f.write('\t\t'+tmp_str+'\n')
    f.write('\t\t'+'\\midrule'+'\n')
    for iProj, num_proj_used in enumerate(num_proj_used_lst):
        for iNoise, noise in enumerate(noise_lst):
            table_line = ''
            tmp_str = '\\thead{{$N_{{\mathrm{{proj}}}}={}$\\\\$\\rho = {}\%$}}'.format(num_proj_used, noise)
            table_line += '\\multirow{2}{*}{'+tmp_str+'}'+' & $\ell_1$ \\left(\cdot 10^{{{}}}\\right)'.format(powers_of_tens[0,iProj,iNoise])
            for iReco, reco_type in enumerate(reco_type_lst):
                min_val = np.min(l1_norms[iProj,iNoise,:])
                tmp_str = '{:.2f}'.format(l1_norms[iProj,iNoise,iReco]/10**powers_of_tens[0,iProj,iNoise])
                table_line += '& {}{{{:s}}}$ '.format('$\\bf' if l1_norms[iProj,iNoise,iReco]==min_val else '$',tmp_str)
            f.write('\t\t'+table_line+' \\\\'+'\n')
            table_line = ''
            table_line += '& $\ell_2$ \\left(\cdot 10^{{{}}}\\right)'.format(powers_of_tens[1,iProj,iNoise])
            for iReco, reco_type in enumerate(reco_type_lst):
                min_val = np.min(l2_norms[iProj,iNoise,:])
                tmp_str = '{:.2f}'.format(l2_norms[iProj,iNoise,iReco]/10**powers_of_tens[1,iProj,iNoise])
                table_line += '& {}{{{:s}}}$ '.format('$\\bf' if l2_norms[iProj,iNoise,iReco]==min_val else '$',tmp_str)
            f.write('\t\t'+table_line+' \\\\'+'\n')
    f.write('\t\t'+'\\bottomrule'+'\n')
    f.write('\t\t'+'\\end{tabular}'+'\n')
    f.write('\\end{table*}')
    f.close()
    # %% All cases, data in rows - old
    f = open(file_path_plots+'mega_table_long.tex', 'w+')
    f.write('\\renewcommand\\theadfont{\\footnotesize}'+'\n')
    f.write('\\renewcommand\\theadalign{ll}'+'\n')
    f.write('\\begin{table*}'+'\n')
    f.write('\t'+'\\centering'+'\n')
    f.write('\t'+'\\begin{tabular}{B*{6}{D}}'+'\n')
    f.write('\t\t'+'\\toprule'+'\n')
    f.write('\t\t'+'Data & Norm & FBP & SIRT 1 & SIRT 2 & SIRT 3 & SIRT 4 \\\\'+'\n')
    f.write('\t\t'+'\\midrule'+'\n')
    for iProj, num_proj_used in enumerate(num_proj_used_lst):
        for iNoise, noise in enumerate(noise_lst):
            table_line = ''
            tmp_str = '\\thead{{$N_{{\mathrm{{proj}}}}={}$\\\\$\\rho = {}\%$}}'.format(num_proj_used, noise)
            table_line += '\\multirow{2}{*}{'+tmp_str+'} & $\ell_1$ '
            for iReco, reco_type in enumerate(reco_type_lst):
                min_val = np.min(l1_norms[iProj,iNoise,:])
                tmp_str = pretty_number(l1_norms[iProj,iNoise,reco_type])
                table_line += '& {}{{{:s}}}$ '.format('$\\bf' if l1_norms[iProj,iNoise,reco_type]==min_val else '$',tmp_str)
            f.write('\t\t'+table_line+' \\\\'+'\n')
            table_line = ''
            table_line += '& $\ell_2$ '
            for iReco, reco_type in enumerate(reco_type_lst):
                min_val = np.min(l2_norms[iProj,iNoise,:])
                tmp_str = pretty_number(l2_norms[iProj,iNoise,reco_type])
                table_line += '& {}{{{:s}}}$ '.format('$\\bf' if l2_norms[iProj,iNoise,reco_type]==min_val else '$',tmp_str)
            f.write('\t\t'+table_line+' \\\\'+'\n')
    f.write('\t\t'+'\\bottomrule'+'\n')
    f.write('\t\t'+'\\end{tabular}'+'\n')
    f.write('\\end{table*}')
    f.close()
# %%
if __name__ == '__main__':
    # Construct the argument parser
    parser = argparse.ArgumentParser()
    
    # Add the arguments to the parser
    parser.add_argument("-num_proj_used", "--num_proj_used", required=False,
                        nargs='?', default=[360, 120, 45],
                        type=lambda s: [item for item in s.split(',')], help="first operand")
    
    parser.add_argument("-noise", "--noise", required=False,
                        nargs='?', default=['0.25', '1.00', '5.00'],
                        type=lambda s: [item for item in s.split(',')], help="second operand")
    
    parser.add_argument("-reco_type", "--reco_type", required=False,
                        nargs='?', default=[0, 1, 2, 3, 4],
                        type=lambda s: [item for item in s.split(',')], help="third operand")
    
    parser.add_argument("-num_itr", "--num_itr", required=False,
                        nargs='?', default=['NCP'],
                        type=lambda s: [item for item in s.split(',')], help="third operand")
    args = parser.parse_args()
    # %%
    main(args)